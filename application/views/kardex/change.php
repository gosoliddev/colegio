<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>

	<div class="container">
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<!-- Success message -->
				<form class="form-horizontal" action="kardex/change" method="post"  id="kardex">    
					<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
					<?php if(validation_errors()):?>
						<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
						</div>
					<?php endif;?>
					 
					<fieldset>

						<!-- Form Name -->
						<legend><?php echo lang('kardex');?></legend>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('student');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<select name="kardex[id_estudiante]" placeholder="<?php echo lang('student');?>" class="form-control" title="<?php echo lang('student');?>" ></select>										
								</div>
								<?php echo form_error('kardex[id_estudiante]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- radio checks -->
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('active');?></label>
							<div class="col-md-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="kardex[activo]" value="true" /> 										
									</label>
								</div>
								<?php echo form_error('kardex[activo]',
								'<em class="error">','</em>');  ?>
							</div>
						</div>	
						
						<!-- Button -->
						<div class="form-group">                      
							<label class="col-md-4 control-label"></label>            
							<div class="col-md-4">
								<div class='error'></div>
								<button type="submit" class="btn btn-primary" >Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
							</div>
						</div>
						

					</fieldset>               
					<div id='messageBox'>         
					</div>

				</form>
			</div>			
			<!-- <div class="panel-footer"></div>	 -->
		</div>		
		

	</div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/kardex/kardex.js"></script>
<?php $this->load->view("layout/footer");?>