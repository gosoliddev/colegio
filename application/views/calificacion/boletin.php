<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<?php $curricular_year=curricular_year(); ?>
<?php $num_bimestres=4;?>
<?php $unidad=obtener_unidad_educativa(); ?>
<section>
  <div class="container-fluid">   
    <h3 class="text-info" style="text-decoration: underline;"><?php echo lang("report_card") ?></h3>
    <div class="panel panel-primary ">   
      <form id="form-filter" action='calificacion/boletin' method="post">
        <div class="panel-body well fixpanel">                                  
          <div class="col-sm-4">            
            <div class="form-group">            
              <div class="col-md-12">
                <div class="input-group">                 
                  <span class="input-group-btn">
                    <button class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>                  
                  </span>
                  <input name="texto" placeholder="<?php echo lang('student');?>" class="form-control" title="<?php echo lang('student');?>"  type="text" value="<?php echo set_value('texto',isset($texto)?$texto:'');?>">                    

                </div>
                <?php echo form_error('texto',
                '<em class="error">','</em>'); ?>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo lang("curricular_year")?></label>
              <div class="col-sm-8"> 
                <select class="form-control" name='gestion[id_gestion]' >
                  <option value=""><?php echo lang('select')?></option>
                  <?php foreach ($gestion_list as $item): ?>
                    <option value="<?php echo $item->id_gestion; ?>" <?php echo set_select('gestion[id_gestion]',$item->id_gestion, ( !empty($gestion->id_gestion) && $gestion->id_gestion ==$item->id_gestion ? TRUE : FALSE )); ?>><?php echo $item->inicio_gestion.' - '.$item->fin_gestion; ?></option>
                  <?php endforeach ?>                           
                </select>             
                 <!--  <?php if (get_cookie("rid")!="ADM"): ?>
                    <input type="text" class="form-control" value="<?php echo $curricular_year->inicio_gestion.' - '.$curricular_year->inicio_gestion;?>" disabled='true'/>
                    <input type="hidden" name="gestion[id_gestion]" value="<?php echo $curricular_year->id_gestion; ?>" >
                  <?php else: ?>
                    <select class="form-control" name='gestion[id_gestion]' >
                      <option value=""><?php echo lang('select')?></option>
                      <?php foreach ($gestion_list as $item): ?>
                        <option value="<?php echo $item->id_gestion; ?>"><?php echo $item->inicio_gestion.' - '.$item->fin_gestion; ?></option>
                      <?php endforeach ?>                           
                    </select>
                  <?php endif ?> -->
                  <?php echo form_error('gestion[id_gestion]',
                  '<em class="error">','</em>'); ?>   
                </div>           
              </div>
            </div>
            <div class="col-sm-4">            
              <div class="form-group">    
                <label class="col-sm-3 control-label"><?php echo lang("course")?></label>            
                <div class="col-sm-8"> 
                  <select class="form-control" name='curso[id_curso]' >
                    <option value=""><?php echo lang('select');?></option>
                    <?php foreach ($curso_list as $item): ?>                    
                      <option value="<?php echo $item->id_curso; ?>" data-id-curso="<?php echo $item->id_curso;?>"
                        <?php echo set_select('curso[id_curso]',$item->id_curso, ( !empty($curso->id_curso) && $curso->id_curso ==$item->id_curso ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>                        
                      <?php endforeach ?> 
                    </select>
                  </div>                  
                </div>
              </div>
            </div>    
          </form>         
        </div>
        <div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
        <div class="btn-group btn-group-sm" role="group" aria-label="First group">               
          <button  name="btnPrint" title="<?php echo lang('back')?>" class="btn btn-default"> 
            <i class="glyphicon glyphicon-print"></i> <?php echo lang('print')?>
          </button>                  
        </div>     
      </div>
      <br />
        <div class="panel panel-primary">
          <div class="panel-heading ">Calificacion</div>
          <form class="form-horizontal" id="form-calificacion" action="calificacion/create" method="post">
            <div class="table-responsive" id="areaPrint"> 
              <table border="1" width="100%" class="table table-bordered table-condensed" style="font-size: 11.2pt">
              <tr>
                  <td colspan="9" class="text-center" >
                    <h5><?php echo $unidad->descripcion; ?></h5>
                  </td>             
                </tr>
                <tr>
                  <td colspan="9" style="padding: 0px;">
                    <table width="100%" border="0px" cellpadding="0" cellspacing="0" style="margin:0px;border:0px;">
                      <tr>                    
                        <td width="12%">
                          <?php echo lang('lastname_names'); ?>
                        </td>
                        <td width="20%">
                          <?php if (isset($estudiante_object)): ?>
                            <h5><?php echo $estudiante_object->apellido_paterno.' '.$estudiante_object->apellido_materno.' '.$estudiante_object->nombres;?></h5>

                          <?php endif ?>                          
                        </td>
                        <td width="7%" style="white-space: nowrap;">
                          <?php echo lang('year_current_course'); ?>
                        </td>
                        <td width="20%">
                          <?php if (isset($curso_object)): ?>
                            <h5><?php echo $curso_object->descripcion?></h5>
                          <?php endif ?> 
                        </td>
                        <td width="5%">
                          Gestion
                        </td>
                        <td width="5%" class="text-center"><?php
                        if(isset($gestion_object)){
                          echo date('Y', strtotime($gestion_object->inicio_gestion));
                        }
                         ?></td>                        
                      </tr>
                    </table>
                  </td>             
                </tr> 
                <tr>
                  <td colspan="9" class="text-center" style="vertical-align: inherit">
                    <?php echo lang('text_help_card_calification') ?>
                  </td>
                </tr>
                <tr>
                  <td rowspan="3" width="15%" class="text-center" style="vertical-align: inherit">
                    CAMPO
                  </td>
                  <td rowspan="3" width="15%" class="text-center" style="vertical-align: inherit">
                    AREA
                  </td>
                  <td colspan="6" class="text-center" style="vertical-align: inherit">
                    VALORACION CUANTITATIVA
                  </td>
                  <td rowspan="3" class="text-center" style="vertical-align: inherit">
                    VALORACION CUALITATIVA
                  </td>         
                </tr>
                <tr>
                  <td rowspan="2" style="vertical-align: inherit;white-space: nowrap;">
                    Bimestre 1
                  </td>
                  <td rowspan="2" style="vertical-align: inherit;white-space: nowrap;">
                    Bimestre 2
                  </td>
                  <td rowspan="2" style="vertical-align: inherit;white-space: nowrap;">
                    Bimestre 3
                  </td>
                  <td rowspan="2" style="vertical-align: inherit;white-space: nowrap;">
                    Bimestre 4
                  </td>
                  <td colspan="2" style="vertical-align: inherit;white-space: nowrap;">
                    PROMEDIO FINAL
                  </td>         
                </tr>
                <tr>      
                  <td>NUMERAL</td>
                  <td>LITERAL</td>
                </tr> 
                <?php if (isset($curso) && isset($curso->id_curso) && $curso->id_curso!=''): ?>
                 <?php foreach ($campo_list as $value): ?>
                  <?php $areas=obtener_areas($estudiante,$value->id_campo,$curricular_year->id_gestion,$curso->id_curso);?>
                  <?php if (existe_campo($estudiante,$value->id_campo,$curricular_year->id_gestion) && count($areas)>0): ?>
                    <tr>          
                      <td rowspan="<?php echo (count($areas) +1);?>" class="text-center"><?php echo  $value->descripcion.' ';?></td>
                    </tr>                     
                    <?php if (count($areas)>0): ?>    
                      <?php foreach ($areas as $area): ?>
                        <tr>
                          <td class="text-center"><?php echo $area->descripcion;?></td>     
                          <?php $sumPromedio=0;?>         
                          <?php for ($i=1;$i<=$num_bimestres;$i++) {?>                      
                          <?php $nota=obtener_nota($estudiante,$area->id_area,$i,$curricular_year->id_gestion,$curso->id_curso);?>                        
                          <?php if ($nota): ?>                          
                            <td class="text-center" valign="middle"  style="vertical-align: inherit;white-space: nowrap;"><?php echo $nota->promedio;?></td>  
                            <?php $sumPromedio=$sumPromedio+$nota->promedio;?>
                          <?php else: ?>
                            <td></td>
                          <?php endif ?>                        
                          <?php }?>
                          <td class="text-center" valign="middle"  style="vertical-align: inherit;white-space: nowrap;"><?php echo round($sumPromedio/$num_bimestres);?></td>
                          <td class="text-center" valign="middle"  style="vertical-align: inherit;white-space: nowrap;"><?php echo obtener_literal(round($sumPromedio/$num_bimestres));?></td>
                      <!-- <td rowspan="<?php echo sizeof($areas);?>" width='300px'>
                        <?php for ($i=1;$i<=$num_bimestres;$i++) { ?>
                        <?php $observacion=obtener_observacion($estudiante,$i,$curricular_year->id_gestion); ?>
                        <?php if (isset($observacion)): ?>
                            <div class="col-md-12"><?php echo $observacion->observacion; ?></div>                              
                        <?php endif ?>                          
                        <?php }?>
                      </td> -->                      
                      <td></td>
                    </tr>                  
                  <?php endforeach ?>          
                <?php endif ?>                
              <?php endif ?>

            <?php endforeach ?>
            
            
          <?php endif ?>
          
        </table>
      </div>
    </div>  
  </div>
</section>

<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script>
  $(document).ready(function (){
    $("select[name='curso[id_curso]']").change(function (){
      $("#form-filter").submit();  
    });
    $("select[name='gestion[id_gestion]']").change(function (){
      $("#form-filter").submit();  
    });
  });
    $("button[name='btnPrint']").click(function (e){        
        $('#areaPrint').printArea();    
    })
</script>
<?php $this->load->view("layout/footer");?>