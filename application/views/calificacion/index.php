<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<?php 
$verButton=TRUE;
if(isset($kardex_list) && sizeof($kardex_list)>0){
  if(isset($kardex_list[0]->dimension_list) && sizeof($kardex_list[0]->dimension_list)>0){
    $var=$kardex_list[0]->dimension_list[0]['criterio_list']; 
    $verButton=(isset($var[0]['nota']) &&  $var[0]['nota']>0)?TRUE:FALSE; 
  }
}
?>
<section>
  <div class="container-fluid"> 
  <h3 class="text-info" style="text-decoration: underline;"><?php echo lang("qualification_student") ?></h3> 
    <div class="panel panel-primary ">   
      <form id="form-filter" action='calificacion/index' method="post">
        <div class="panel-body well fixpanel">                        
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label"><?php echo lang("course")?></label>
              <select class="form-control" name='usuario_curso[id_usuario_curso]' >
                <option value=""><?php echo lang('select')?></option>
                <?php foreach ($usuairo_curso_list as $item): ?>
                  <option value="<?php echo $item->id_usuario_curso; ?>" <?php echo set_select('curso[id_usuario_curso]',$item->id_usuario_curso, ( !empty($usuario_curso->id_usuario_curso) && $usuario_curso->id_usuario_curso ==$item->id_usuario_curso ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
                <?php endforeach ?>                                    
              </select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label"><?php echo lang("subarea")?></label>
              <select class="form-control" name='subarea[id_subarea]' >
                <option value=""><?php echo lang('select')?></option>  
                <?php if (sizeof($subarea_list) ): ?>                  
                 <?php foreach ($subarea_list as $item): ?>
                  <option value="<?php echo $item->id_subarea; ?>" <?php echo set_select('subarea[id_subarea]',$item->id_subarea, ( !empty($subarea->id_subarea) && $subarea->id_subarea ==$item->id_subarea ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
                <?php endforeach ?>                                  
              <?php endif ?>                   

            </select>
          </div>
        </div>    
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label"><?php echo lang("bi_monthly")?></label>
            <select class="form-control" name='bimestre[codigo]' >
              <option value=""><?php echo lang('select')?></option>  
              <?php if (sizeof($bimestre_list) ): ?>                  
               <?php foreach ($bimestre_list as $item): ?>
                <option value="<?php echo $item->codigo; ?>" <?php echo set_select('bimestre[codigo]',$item->codigo, ( !empty($bimestre->codigo) && $bimestre->codigo ==$item->codigo ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
              <?php endforeach ?>                                  
            <?php endif ?>                   

          </select>
        </div>
      </div> 
    </div>    
  </form>         
</div>
<!-- start toolbar-->
<?php if (!$verButton): ?>
  <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
    <div class="btn-group btn-group-md" role="group" aria-label="First group">           
      <a href="#" onclick='toolbar.save(event);' title="<?php echo lang('save_change')?>" class="btn btn-primary ">
        <i class=' glyphicon glyphicon-floppy-disk'></i> <?php echo lang('save_change')?></a>               
      </div>      
    </div>      
  <?php endif; ?>
  <div  class="clearfix" ></div>
  <br />    
  <!-- end toolbar--> 
  <div class="panel panel-primary">
    <div class="panel-heading ">Calificacion</div>
    <form class="form-horizontal" id="form-calificacion" action="calificacion/create" method="post">
      <input type='hidden' name="id_subarea" value="<?php echo (isset($subarea['id_subarea'])?$subarea['id_subarea']:"") ?>">
      <input type='hidden' name="id_usuario_curso" value="<?php echo (isset($usuario_curso->id_usuario_curso)?$usuario_curso->id_usuario_curso:"") ?>">
      <input type='hidden' name="codigo" value="<?php echo (isset($bimestre['codigo'])?$bimestre['codigo']:"") ?>">
      <div class="table-responsive"> 
        <table class="table table-hover table-bordered table-striped table-custom" width="100%" >
         <tbody>
          <tr>
            <td></td>
            <td></td>
            <?php if (isset($dimension_list) && sizeof($dimension_list)>0): ?>
             <?php foreach ($dimension_list as $value): ?>                
              <td class="text-center"><?php echo $value["descripcion"];?></td>                
            <?php endforeach ?>
          <?php endif ?>        
          <td></td>
        </tr>
        <tr>
          <td class="text-center">Nro</td>  
          <td class="text-center">Estudiante</td>
          <?php if (isset($dimension_list) && sizeof($dimension_list)>0): ?>            
            <?php foreach ($dimension_list as $value3): ?>   
             <td>    
               <table  class="table-vertical" >
                <tr>
                  <?php foreach ($value3["criterio_list"] as $value4): ?>                      
                    <?php if ($value4["codigo_criterio"]=="AUTOEVALUACION"): ?>
                      <td class="tituloTablaVertical btn-success">
                        <div class="textoVertical">
                          <div class="textoVerticalColumna ">&nbsp;AUTOEVALUACION&nbsp;</div>
                        </div>
                      </td>
                    <?php elseif ($value4["codigo_criterio"]=="PROMEDIO"): ?>
                     <td class="tituloTablaVertical btn-danger ">
                      <div class="textoVertical">
                        <div class="textoVerticalColumna ">&nbsp;PROMEDIO&nbsp;</div>
                      </div>
                    </td>
                  <?php else: ?>
                    <td class="tituloTablaVertical btn-info">
                      <div class="textoVertical">
                        <div class="textoVerticalColumna ">&nbsp;<?php echo $value4["descripcion"] ;?> &nbsp;</div>
                      </div>
                    </td>   
                  <?php endif ?>

                <?php endforeach ?>                                
              </tr>
            </table>
          </td>                 
        <?php endforeach ?>
        <td class="btn-primary">    
         <table  class="table-vertical">
          <tr>
            <td class="tituloTablaVertical">
              <div class="textoVertical">
                <div class="textoVerticalColumna ">&nbsp;NOTA BISMESTRAL&nbsp;</div>
              </div>
            </td>
          </tr>
        </table>
      </td>  
      <!-- <td>      
    </td>  --> 
  <?php endif ?>
</tr>
<?php if (isset($kardex_list) && sizeof($kardex_list)>0): ?>
 <?php  $i=0; ?>     
 <?php foreach ($kardex_list as $estudiante): ?>
  <tr class="rowCalificacion">
    <td class="text-center rude" ><?php echo $estudiante->rude;?></td>
    <td  class="text-right" style='white-space: nowrap;'>
      &nbsp;&nbsp;&nbsp;<?php echo $estudiante->apellido_paterno.' '.$estudiante->apellido_materno.' '.$estudiante->nombres;?>&nbsp;&nbsp;&nbsp;
    </td>
    <?php if (isset($estudiante->dimension_list) && sizeof($estudiante->dimension_list)>0): ?>
      <?php foreach ($estudiante->dimension_list as $value1): ?>           
        <td>    
         <table  class="table-vertical">
          <tr>
            <?php foreach ($value1["criterio_list"] as $value2): ?>              
             <td class="tituloTablaVertical2">
              <div class="textoVertical-inputs">
                <div class="form-group form-group-fix">
                  <?php if ($value2["codigo_criterio"]=="AUTOEVALUACION"): ?>
                    <input type="text" name="<?php echo $value1["codigo"].'['.$estudiante->rude.']['.$value2["codigo_criterio"].']';?>" class="form-control vinput autoevaluacion" value="<?php echo $value2["nota"];?>">
                  <?php elseif ($value2["codigo_criterio"]=="PROMEDIO"): ?>
                   <input type="text" name="<?php echo $value1["codigo"].'['.$estudiante->rude.']['.$value2["codigo_criterio"].']';?>" class="form-control vinput promedio <?php echo $value1["codigo"];?>" readonly="readonly" value="<?php echo $value2["nota"];?>">
                 <?php else: ?>
                  <input type="text" name="<?php echo $value1["codigo"].'['.$estudiante->rude.']['.$value2["codigo_criterio"].']';?>" class="form-control vinput nota" value="<?php echo $value2["nota"];?>" data-porcentaje='<?php echo $value1["porcentaje_calificacion"]?>'>                                     
                <?php endif ?>                
              </div>
            </div>
          </td>                     
        <?php endforeach ?>                                           
      </tr>
    </table>
  </td>                         
<?php endforeach ?>
<?php endif ?>

<td>    
 <table  class="table-vertical">  
   <tr>   
    <td class="tituloTablaVertical2">
      <div class="textoVertical-inputs">
        <div class="form-group form-group-fix">
          <input type="text" name="<?php echo "nota_bimestral[".$estudiante->rude."]"; ?>" class="form-control vinput nota-bimestral" readonly="readonly">
        </div>
      </div>
    </td>        
  </tr>
</table>
</td>  
<!-- <td>
  <textarea name="cualitativo[<?php echo  $estudiante->rude ?>]" class="form-control hide" readonly="readonly"></textarea> 
  <button class="btn btn-success glyphicon glyphicon-plus" name="btnValoracion"></button>
</td>  -->   
</tr>  
<?php endforeach ?>
<?php endif ?>
</tbody>
</table>
</div>  
</form>
</div>
<!-- start toolbar-->
<?php if (!$verButton): ?>
  <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
    <div class="btn-group btn-group-md" role="group" aria-label="First group">           
      <a href="#" onclick='toolbar.save(event);' title="<?php echo lang('save_change')?>" class="btn btn-primary ">
        <i class=' glyphicon glyphicon-floppy-disk'></i> <?php echo lang('save_change')?></a>               
      </div>      
    </div>
  <?php endif ?>
  <br />    
  <!-- end toolbar-->     
  <div class="modal fade" id="modal-cualitativo">
    <div class="modal-dialog modal-lg">      
      <div class="modal-content">
        <form action="" method="POST" class="form-horizontal" role="form">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?php echo  lang('title_value_qualitative')?></h4>
          </div>
          <div class="modal-body">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-4">
                  <label class="col-ms-10 control-label"><?php echo lang('bi_monthly_note')?></label>
                  <div class="col-ms-10">
                    <input class="form-control" name="nota-bimestral" disabled />
                  </div>                
                </div>
                <div class="col-sm-4">
                  <label class="control-label"><?php echo lang('bi_monthly')?></label>
                  <input class="form-control" name="bimestre" disabled/>
                </div>            
                <div class="col-sm-4">
                  <label class="control-label"><?php echo lang('student_rude')?></label>
                  <input class="form-control" name="rude_estudiante" disabled/>
                </div>
              </div>
              <br />
              <div class="row">
                <div class="col-sm-12">                
                  <label class="control-label"><?php echo lang('title_value_qualitative')?></label>
                  <select class="form-control" id='obs' multiple="multiple"></select>                
                </div>                   
              </div>
              <br />
              <div class="row">
                <div class="col-sm-12">
                  <label class="control-label"><?php echo lang('aditional_observation')?></label>
                  <textarea rows="5" name="adicionalObs" class="form-control"></textarea>              
                </div>                   
              </div>
            </div>
          </div>          
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('cancel');?></button>
            <button type="button" class="btn btn-primary" name='btnSave'><?php echo lang('save');?></button>
          </div>
        </form> 
      </div>      
    </div>
  </div>    

</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="<?php echo base_url()?>assets/app/calificacion/calificacion.js"></script>
<?php $this->load->view("layout/footer");?>