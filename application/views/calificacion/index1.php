<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>
  <div class="container-fluid">  
  
    <div class="panel panel-primary ">   
      <form id="form-filter" action='calificacion/index' method="post">
        <div class="panel-body well fixpanel">                        
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label"><?php echo lang("course")?></label>
              <select class="form-control" name='curso[id_curso]' >
                <option value=""><?php echo lang('select')?></option>
                <?php foreach ($curso_list as $item): ?>
                  <option value="<?php echo $item->id_curso; ?>" <?php echo set_select('curso[id_curso]',$item->id_curso, ( !empty($curso->id_curso) && $curso->id_curso ==$item->id_curso ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
                <?php endforeach ?>                                    
              </select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label"><?php echo lang("subarea")?></label>
              <select class="form-control" name='subarea[id_subarea]' >
                <option value=""><?php echo lang('select')?></option>  
                <?php if (sizeof($subarea_list) ): ?>                  
                 <?php foreach ($subarea_list as $item): ?>
                  <option value="<?php echo $item->id_subarea; ?>" <?php echo set_select('subarea[id_subarea]',$item->id_subarea, ( !empty($curso->id_subarea) && $curso->id_subarea ==$item->id_subarea ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
                <?php endforeach ?>                                  
              <?php endif ?>                   

            </select>
          </div>
        </div>    

      </div>    
    </form>         
  </div>
  <div class="panel panel-primary">
    <div class="panel-heading ">Calificacion</div>
    <form class="form-horizontal" id="form-calificacion" action="calificacion/create" method="post">
      <div class="table-responsive"> 
        <table class="table table-hover table-bordered table-striped table-custom" width="100%" >         
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <?php if (isset($plan_list) && sizeof($plan_list)>0): ?>
               <?php foreach ($plan_list as $key=>$value): ?>                
                <td class="text-center"><?php echo $key;?></td>                
              <?php endforeach ?>
            <?php endif ?>        
             <td></td>
          </tr>
          <tr>
            <td class="text-center">Nro</td>  
            <td class="text-center">Estudiante</td>
            <?php if (isset($plan_list) && sizeof($plan_list)>0): ?>            
              <?php foreach ($plan_list as $key=>$value3): ?>   
               <td>    
                 <table  class="table-vertical" >
                  <tr>
                    <?php foreach ($value3 as $key => $value4): ?>                      
                     <td class="tituloTablaVertical btn-info">
                      <div class="textoVertical">
                        <div class="textoVerticalColumna ">&nbsp;<?php echo $value4->criterio ;?> &nbsp;</div>
                      </div>
                    </td> 
                  <?php endforeach ?>
                  <td class="tituloTablaVertical btn-success">
                    <div class="textoVertical">
                      <div class="textoVerticalColumna ">&nbsp;AUTOEVALUACION&nbsp;</div>
                    </div>
                  </td>
                  <td class="tituloTablaVertical btn-danger ">
                    <div class="textoVertical">
                      <div class="textoVerticalColumna ">&nbsp;PROMEDIO&nbsp;</div>
                    </div>
                  </td>
                </tr>
              </table>
            </td>                 
          <?php endforeach ?>
          <td class="btn-primary">    
           <table  class="table-vertical">
            <tr>
              <td class="tituloTablaVertical">
                <div class="textoVertical">
                  <div class="textoVerticalColumna ">&nbsp;NOTA BISMESTRAL&nbsp;</div>
                </div>
              </td>
            </tr>
          </table>
        </td>    
      <?php endif ?>
    </tr>
    <?php if (isset($plan_list) && sizeof($kardex_list)>0): ?>
     <?php  $i=0; ?>     
      <?php foreach ($kardex_list as $estudiante): ?>
        <tr class="rowCalificacion">
          <td class="text-center"><?php echo $estudiante->rude;?></td>
          <td  class="text-right" style='white-space: nowrap;'>&nbsp;&nbsp;&nbsp;<?php echo $estudiante->apellido_paterno.' '.$estudiante->apellido_materno.' '.$estudiante->nombres;?>&nbsp;&nbsp;&nbsp;</td>              
          <?php foreach ($plan_list as $key1=>$value1): ?>   
            <?php  $j=0; ?>    
           <td>    
             <table  class="table-vertical">
              <tr>
                <?php foreach ($value1 as $key2 => $value2): ?>
                 <td class="tituloTablaVertical2">
                  <div class="textoVertical-inputs">
                  <div class="form-group form-group-fix">
                      <input type="text" name="<?php echo $key1.'['.$estudiante->rude.']['.$value2->codigo.']';?>" class="form-control vinput nota" value="<?php echo $value2->nota;?>" data-porcentaje='<?php echo $value2->porcentaje;?>'> 
                    </div>
                  </div>
                </td>                     
              <?php endforeach ?>   
                        
              <td class="tituloTablaVertical2">              
                <div class="textoVertical-inputs">
                  <div class="form-group form-group-fix">
                    <input type="text" name="<?php echo $key1.'['.$estudiante->rude.'][AUTOEVALUACION]';?>" class="form-control vinput autoevaluacion" value="<?php echo $value1[0]->autoevaluacion; ?>">
                  </div>
                </div>
              </td>                  
              <td class="tituloTablaVertical2">
                <div class="textoVertical-inputs">
                  <div class="form-group form-group-fix">
                    <input type="text" name="<?php echo $key1.'['.$estudiante->rude.'][PROMEDIO]';?>" class="form-control vinput promedio <?php echo $key1;?>" readonly="readonly" value="<?php echo $value1[0]->promedio; ?>">
                  </div>
                </div>
              </td>              
            </tr>
          </table>
        </td>                         
      <?php endforeach ?>   
      <td>    
       <table  class="table-vertical">  
         <tr>   
          <td class="tituloTablaVertical2">
            <div class="textoVertical-inputs">
              <div class="form-group form-group-fix">
                <input type="text" name="<?php echo $i; ?>" class="form-control vinput nota-bimestral" readonly="readonly">
              </div>
            </div>
          </td>        
        </tr>
      </table>
    </td>      
  </tr>  
<?php endforeach ?>
<?php endif ?>
</tbody>
</table>
</div>  
<button type="submit" >Enviar</button>
</form>
</div>
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="<?php echo base_url()?>assets/app/calificacion/calificacion.js"></script>
<?php $this->load->view("layout/footer");?>