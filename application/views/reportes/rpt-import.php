<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>
    <div class="container"> 

        <h3 class="text-info" style="text-decoration: underline;text-transform: capitalize;"><?php echo lang("verify_students") ?></h3>
        <div class="panel panel-primary ">              
            <div class="panel-body well fixpanel" id="areaPrint">
                <?php echo isset($error)?$error:"";?>

                <?php echo form_open_multipart('reportes/do_upload');?>

                <input type="file" name="userfile" size="20" />

                <br /><br />

                <input type="submit" value="upload" />

            </form>
        </div>
    </div>
</div>
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>        
<?php $this->load->view("layout/footer");?>