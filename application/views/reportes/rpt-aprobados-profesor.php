<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<?php
$curricular_year=curricular_year();
$unidad=obtener_unidad_educativa();
?>	
<section>
	<div class="container">			
		<h3 class="text-info" style="text-decoration: underline;text-transform: capitalize;"><?php echo lang("rpt_aprobados_by_course") ?></h3>
		<!-- <div class="panel-heading">Registro</div>		 -->
		<div class="panel panel-primary ">	
					<!-- <div class="panel-heading">Registro</div>	
				-->
				<div class="panel-body well fixpanel">
					<form id="formRptAprobados" method="post" action="reportes/profesor_aprobados"> 
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label"><?php echo lang("curricular_year")?></label>
								<?php if (get_cookie("rid")=="PRO" || get_cookie("rid")=="ASE"): ?>
									<input type="text" class="form-control" value="<?php echo $curricular_year->inicio_gestion.' - '.$curricular_year->inicio_gestion;?>" disabled='true'/>
									<input type="hidden" name="gestion[id_gestion]"  id="gestion" value="<?php echo $curricular_year->id_gestion; ?>" >
								<?php else: ?>
									<select class="form-control" name='gestion[id_gestion]'  id="gestion">
										<option value=""><?php echo lang('select')?></option>
										<?php foreach ($gestion_list as $item): ?>
											<option value="<?php echo $item->id_gestion; ?>" <?php echo set_select('gestion[id_gestion]',$item->id_gestion, ( !empty($gestion->id_gestion) && $gestion->id_gestion ==$item->id_gestion ? TRUE : FALSE )); ?>><?php echo $item->inicio_gestion.' - '.$item->fin_gestion; ?></option>
										<?php endforeach ?>	            							
									</select>
								<?php endif ?>
								<?php echo form_error('gestion[id_gestion]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>	
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label"><?php echo lang("subject")?></label>
								<select class="form-control" name='subarea[id_subarea]' >
									<option value=""><?php echo lang('select')?></option>
									<?php foreach ($subarea_list as $item): ?>
										<option value="<?php echo $item->id_subarea; ?>" <?php echo set_select('subarea[id_subarea]',$item->id_subarea, ( !empty($subarea->id_subarea) && $subarea->id_subarea ==$item->id_subarea ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
									<?php endforeach ?>       							
								</select>
							</div>
						</div>				
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label"><?php echo lang("course")?></label>
								<select class="form-control" name='curso[id_curso]' >
									<option value=""><?php echo lang('select')?></option>
									<?php foreach ($curso_list as $item): ?>
										<?php if (existe_subarea_curso($subarea['id_subarea'],$curricular_year,$item->id_curso))	: ?>
											<option value="<?php echo $item->id_curso; ?>" <?php echo set_select('curso[id_curso]',$item->id_curso, ( !empty($curso->id_curso) && $curso->id_curso ==$item->id_curso ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
										<?php endif?>
									<?php endforeach ?>	            							
								</select>
								<?php echo form_error('curso[id_curso]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>	
													
					</form>
				</div>				
			</div>	

			<div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
				<div class="btn-group btn-group-sm" role="group" aria-label="First group">               
					<button  name="btnPrint" title="<?php echo lang('back')?>" class="btn btn-default"> 
						<i class="glyphicon glyphicon-print"></i> <?php echo lang('print')?>
					</button>                  
				</div>		 
			</div>
			<br />
			<div class="panel panel-primary ">	
				<div class="panel-heading"><?php echo lang("list")?></div>		
				<div class="panel-body" id="areaPrint">
					<table id="tableCurso" class="table table-condensed table-hover table-striped table-bordered" >        			
						<tr>
							<td colspan="8">
								<table width="100%">
									<tr>										
										<td width="20%">
											<h5><?php echo $unidad->nombre ?></h5>
										</td>
										<td width="65%" class="text-center">
											<h4><?php echo lang('rpt_aprobados') ?></h3>
											</td>
											<td width="15%" class="text-center"><small>Fecha:<?php echo date("d-m-Y") ?></small></td>
										</tr>
									</table>
								</td>							
							</tr> 
							<tr>							
								<td colspan="2" class="bg-info text-center"><?php echo  lang('estudent')?></td>													
								<td style="white-space: nowrap;" class="bg-info text-center" colspan="<?php echo sizeOf($bimestre_list)?>" class="text-center"><?php echo lang("bi_monthly")?></td>
								<td class="text-center bg-info"></td>                    
								<td class="text-center bg-info"></td>                    
							</tr>            
							<tr>							
								<td><?php echo  lang('student_rude')?></td>
								<td><?php echo  lang('complete_name')?></td>  
								<?php foreach ($bimestre_list as  $value): ?>
									<td class="text-center"><?php echo  $value->codigo;?></td> 
								<?php endforeach ?>                  							
								<td class="text-center"><?php echo  lang('anual_note')?></td>                    
								<td class="text-center"><?php echo  lang('status')?></td>                    
							</tr>
							<?php if (isset($estudiantes_list) && sizeOf($estudiantes_list)>0 && isset($subarea) && $subarea['id_subarea']): ?>								
								<?php foreach ($estudiantes_list as $estudiante): ?>
									<?php $promedio=0; ?>
									<tr>							
										<td class="text-center"><?php echo  $estudiante->rude?></td>
										<td class="text-center"><?php echo $estudiante->apellido_paterno.' '.$estudiante->apellido_materno.' '.$estudiante->nombres?></td>  
										<?php foreach ($bimestre_list as  $value): ?>
											<?php $nota=  obtener_promedio_bimestre_rpt1($value->codigo,$curso['id_curso'],$subarea['id_subarea'],$curricular_year->id_gestion,$estudiante->id_estudiante)?>								
											<td class="text-center"><?php echo  $nota["promedio"];?></td> 
											<?php $promedio+=$nota["promedio"] ?>
										<?php endforeach ?>                  							
										<td class="text-center"><?php echo round($promedio/sizeof($bimestre_list));?></td>                    
										<?php if (($promedio/sizeof($bimestre_list))>51): ?>
											<td class="text-center"><?php echo lang('approved') ?></td>	
										<?php else: ?>
											<td class="text-center"><?php echo lang('reprobate') ?></td>	
										<?php endif ?>

									</tr>
								<?php endforeach ?>     							
							<?php endif ?>						   			
						</table>
					</div>
				</div>	 
			</div>	
		</section> 

		<?php $this->load->view("layout/container_footer");?>
		<?php $this->load->view("layout/scripts");?>
		<script type="text/javascript" src="assets/app/reportes/rpt-aprobados.js"></script>
		<?php $this->load->view("layout/footer");?>