<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>
    <div class="container"> 
    
    <h3 class="text-info" style="text-decoration: underline;text-transform: capitalize;"><?php echo lang("students_founded") ?></h3>
        <div class="panel panel-primary ">  
            <div class="panel-heading"><?php echo lang("list").' '.lang('students_founded')?></div>      
            <div class="panel-body" id="areaPrint">
            <table cellpadding="0" cellspacing="0" width="100%" class="table table-hover table-striped">
                    <thead>
                        <th><?php echo lang("student_rude");?></th>
                        <th><?php echo lang("student");?></th>
                        <th><?php echo lang("status");?></th>
                    </thead>

                    <?php foreach($csvData as $field){?>
                    <tr>
                        <td><?php echo $field['RUDE']?></td>
                        <td><?php echo $field['NOMBRE']?></td>  
                           
                        <?php if ($field['estado']==0): ?>
                             <td class="bg-warning"><?php echo lang("notfound") ?> </td>
                         <?php else: ?>
                             <td class="bg-success"><?php echo lang("founded") ?> </td>
                         <?php endif ?> 
                        </td>      
                    </tr>
                    <?php }?>
                </table>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>        
<?php $this->load->view("layout/footer");?>