<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header"); ?>

<section><?php
	if(isset($cargo))
	{
		$id=$cargo->id_cargo;
		$cbxValue=($cargo->activo=="1"?true:false);
	}
	else
	{
		$id="";
		$cbxValue=false;
	}
	?>

	<div class="container">
	<!-- start toolbar-->
        <div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">               
                <a href="#cargo/index" onclick='toolbar.home(event);' title="<?php echo lang('back')?>" class="btn btn-default"> 
                	<i class="glyphicon glyphicon-arrow-left"></i> <?php echo lang('back')?>
                </a>                  
            </div>
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                <div class="btn-group">
                   <!--  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="optAdvanced">
                        <li><a href="#">{{aplicarSolicitud}}</a></li>                        
                    </ul> -->
                </div>
            </div>
        </div>
        <br />
        <!-- end toolbar-->	
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<!-- Success message -->
				<form class="form-horizontal" action="cargo/change" method="post"  id="cargo">    
					<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
					<input type="hidden" name="id" value="<?php echo $id; ?>">
					<?php if(validation_errors()):?>
						<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
						</div>
					<?php endif;?>
					 
					<fieldset>

						<!-- Form Name -->
						<legend><?php echo lang('cargo_registration');?></legend>

						
						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('name');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
									<input name="cargo[descripcion]" placeholder="<?php echo lang('name');?>" class="form-control" title="<?php echo lang('name');?>"  type="text" value="<?php echo set_value('cargo[descripcion]',isset($cargo->descripcion)?$cargo->descripcion:'');?>">										
								</div>
								<?php echo form_error('cargo[descripcion]','<em class="error">','</em>'); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('code');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
									<input name="cargo[descripcion]" placeholder="<?php echo lang('code');?>" class="form-control" title="<?php echo lang('code');?>"  type="text" value="<?php echo set_value('cargo[codigo]',isset($cargo->codigo)?$cargo->codigo:'');?>">										
								</div>
								<?php echo form_error('cargo[descripcion]','<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- radio checks -->
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('active');?></label>
							<div class="col-md-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="cargo[activo]" value="1" <?php echo set_checkbox('unidad_educativa[activo]',true,$cbxValue);?>/>
									</label>
								</div>
								<?php echo form_error('cargo[activo]','<em class="error">','</em>'); ?>
							</div>
						</div>						
						<!-- Button -->
						<div class="form-group">                      
							<label class="col-md-4 control-label"></label>            
							<div class="col-md-4">
								<div class='error'></div>
								<button type="submit" class="btn btn-primary" >Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
							</div>
						</div>
						

					</fieldset>               
					<div id='messageBox'>         
					</div>

				</form>
			</div>			
			<!-- <div class="panel-footer"></div>	 -->
		</div>		

	</div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/cargo/cargo.js"></script>
<?php $this->load->view("layout/footer");?>