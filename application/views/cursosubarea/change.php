<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>

	<div class="container">
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<!-- Success message -->
				<form class="form-horizontal" action="cursosubarea/change" method="post"  id="curso-sub-area">    
					<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
					<?php if(validation_errors()):?>
						<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
						</div>
					<?php endif;?>
					 
					<fieldset>

						<!-- Form Name -->
						<legend><?php echo lang('sub_area_course_registration');?></legend>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('course');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
									<select name="cursosubarea[id_curso]" placeholder="<?php echo lang('course');?>" class="form-control" title="<?php echo lang('course');?>" ></select>										
								</div>
								<?php echo form_error('cursosubarea[id_curso]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>


						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('subarea');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
									<select name="cursosubarea[id_subarea]" placeholder="<?php echo lang('subarea');?>" class="form-control" title="<?php echo lang('subarea');?>" ></select>										
								</div>
								<?php echo form_error('cursosubarea[id_subarea]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						
						<!-- radio checks -->
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('active');?></label>
							<div class="col-md-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="cursosubarea[activo]" value="true" /> 										
									</label>
								</div>
								<?php echo form_error('cursosubarea[activo]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>	
						
						<!-- Button -->
						<div class="form-group">                      
							<label class="col-md-4 control-label"></label>            
							<div class="col-md-4">
								<div class='error'></div>
								<button type="submit" class="btn btn-primary" >Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
							</div>
						</div>
						

					</fieldset>               
					<div id='messageBox'>         
					</div>

				</form>
			</div>			
			<!-- <div class="panel-footer"></div>	 -->
		</div>		
		

	</div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/cursosubarea/cursosubarea.js"></script> 
<?php $this->load->view("layout/footer");?>