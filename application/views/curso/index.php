<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>
 
	<div class="container">
    <h3 class="text-info" style="text-decoration: underline;"><?php echo lang("management_courses") ?></h3>
     
        <div class="panel panel-primary">
            <div class="panel-heading"><?php echo lang("list") ?></div>
            <div class="panel panel-default">
            <div class="panel-heading">

		  <!-- start toolbar-->
        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                <a href="#curso/change" onclick='toolbar.create(event);' title="<?php echo lang('new')?>" class="btn btn-default">
                            <i class=" glyphicon glyphicon-plus"></i> <?php echo lang('new')?></a>         
                      <a href="#curso/change" onclick='toolbar.edit(event);' title="<?php echo lang('edit')?>" class="btn btn-default">
                            <i class=" glyphicon glyphicon-pencil"></i> <?php echo lang('edit')?></a>       
                     <a href="#curso/delete" onclick='toolbar.trash(event);'title="<?php echo lang('trash')?>" class="btn btn-default ">
                            <i class=" glyphicon glyphicon-trash"></i> <?php echo lang('trash')?></a> 
            </div>
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                <div class="btn-group">                  
                </div>
            </div>
        </div>
        <!-- end toolbar-->

		</div>

        </div>

			<table id="tableCurso" class="table table-condensed table-hover table-striped" >
               <thead>             
                <tr>
                    <th data-column-id="id_curso" data-visible='false' data-identifier="true"><?php echo  lang('id')?></th>
                    <th data-column-id="descripcion_curso" ><?php echo  lang('description')?></th>
                     <th data-column-id="paralelo" ><?php echo  lang('parallel')?></th>
                    <th data-column-id="descripcion_nivel" ><?php echo  lang('level')?></th>
                    <th data-column-id="grado"><?php echo  lang('grade')?></th>
                    <th data-column-id="fecha_actualizacion" ><?php echo  lang('update_date')?></th>
                    <th data-column-id="activo" data-formatter='active' ><?php echo  lang('active')?></th>
                </tr>
            </thead>         
            </table>
			<div class="panel-footer"></div>	
		</div>

	</div>	
</section> 
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/curso/curso.js"></script>
<?php $this->load->view("layout/footer");?>