<?php $this->load->view("layout/header"); ?>
<?php $this->load->view("layout/container_header"); ?>
<?php
if (isset($criterio)) {
    $id = $criterio->id_criterio;
    $cbxValue = ($criterio->activo == "1" ? true : false);
} else {
    $id = "";
    $cbxValue = false;
}
?>
    <section>

        <div class="container">
            <!-- start toolbar-->
            <div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
                <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                    <a href="#criterio/" onclick='toolbar.home(event);' title="<?php echo lang('back')?>" class="btn btn-default"> 
                    <i class="glyphicon glyphicon-arrow-left"></i> <?php echo lang('back')?>
                </a> 
                </div>
                <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                    <div class="btn-group">                         
                    </div>
                </div>
            </div>
            <br/>
            <!-- end toolbar-->
            <div class="panel panel-primary ">
                <!-- <div class="panel-heading">Registro</div>		 -->
                <div class="panel-body well fixpanel">
                    <!-- Success message -->
                    <form class="form-horizontal" action="criterio/change" method="post" id="criterio">
                        <!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> -->
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <?php if (validation_errors()): ?>
                            <div class="alert alert-danger text-center" role="alert" id="success_message">Error <i
                                    class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >', '</li>'); ?>
                            </div>
                        <?php endif; ?>

                        <fieldset>

                            <!-- Form Name -->
                            <legend><?php echo lang('criterio_record'); ?></legend>

                            <!-- Text input-->

                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo lang('dimencion'); ?></label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i
                                                class="glyphicon glyphicon-pencil"></i></span>
                                        <select name="criterio[id_dimension]"
                                                placeholder="<?php echo lang('dimencion'); ?>" class="form-control"
                                                title="<?php echo lang('dimencion'); ?>">
                                            <option value=""><?php echo lang('select') ?></option>
                                            <?php foreach ($dimension_list as $item):?>
                                                <option value="<?php echo $item->id_dimension; ?>" <?php echo set_select('gestion[id_unidad_educativa]',$item->id_dimension, ( !empty($criterio->id_dimension) && $criterio->id_dimension ==$item->id_dimension ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
                                            <?php endforeach;  ?>
                                        </select>
                                    </div>
                                    <?php echo form_error('criterio[id_dimension]',
                                        '<em class="error">', '</em>'); ?>
                                </div>
                            </div>

                            <!-- Text input-->

                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo lang('description'); ?></label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i
                                                class="glyphicon glyphicon-th-list"></i></span>
                                        <input name="criterio[descripcion]"
                                               placeholder="<?php echo lang('description'); ?>" class="form-control"
                                               title="<?php echo lang('description'); ?>" type="text"
                                               value="<?php echo set_value('criterio[descripcion]', isset($criterio->descripcion) ? $criterio->descripcion : ''); ?>">
                                    </div>
                                    <?php echo form_error('criterio[descripcion]',
                                        '<em class="error">', '</em>'); ?>
                                </div>
                            </div>

                            <!-- Text input-->

                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo lang('percentage_rating'); ?></label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i
                                                class="glyphicon glyphicon-th-list"></i></span>
                                        <input name="criterio[porcentaje_calificacion]"
                                               placeholder="<?php echo lang('percentage_rating'); ?>"
                                               class="form-control" title="<?php echo lang('percentage_rating'); ?>"
                                               type="text"
                                               value="<?php echo set_value('criterio[porcentaje_calificacion]', isset($criterio->porcentaje_calificacion) ? $criterio->porcentaje_calificacion : ''); ?>">
                                    </div>
                                    <?php echo form_error('criterio[porcentaje_calificacion]', '<em class="error">', '</em>'); ?>
                                </div>
                            </div>

                            <!-- radio checks -->
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo lang('active'); ?></label>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="criterio[activo]"
                                                   value="1" <?php echo set_checkbox('unidad_educativa[activo]', true, $cbxValue); ?>/>
                                        </label>
                                    </div>
                                    <?php echo form_error('criterio[activo]',
                                        '<em class="error">', '</em>'); ?>
                                </div>
                            </div>


                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <div class='error'></div>
                                    <button type="submit" class="btn btn-primary">Send <span
                                            class="glyphicon glyphicon-floppy-disk"></span></button>
                                </div>
                            </div>


                        </fieldset>
                        <div id='messageBox'>
                        </div>

                    </form>
                </div>
                <!-- <div class="panel-footer"></div>	 -->
            </div>


        </div>
    </section>
<?php $this->load->view("layout/container_footer"); ?>
<?php $this->load->view("layout/scripts"); ?>
    <script type="text/javascript" src="assets/app/criterio/criterio.js"></script>
<?php $this->load->view("layout/footer"); ?>