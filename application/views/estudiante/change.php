<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
 <section>

	<div class="container">
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<!-- Success message -->
				<form class="form-horizontal" action="estudiante/change" method="post"  id="estudiante">    
					<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
					<?php if(validation_errors()):?>
						<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
						</div>
					<?php endif;?>
					 
					<fieldset>

						<!-- Form Name -->
						<legend><?php echo lang('student_registration');?></legend>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('student');?></label> 
							<div class="col-md-4">
								<div class="input-group">									
								<span class="input-group-btn">
										<button class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
									</span>
									</span>
									<input name="estudiante[codigo_rude]" placeholder="<?php echo lang('student');?>" class="form-control" title="<?php echo lang('student');?>"  type="text">										
									
								</div>
								<?php echo form_error('estudiante[codigo_rude]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('previous_sie');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
									<input name="estudiante[sie_anterior]" placeholder="<?php echo lang('previous_sie');?>" class="form-control" title="<?php echo lang('previous_sie');?>"  type="text">										
								</div>
								<?php echo form_error('estudiante[sie_anterior]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('names');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<input  name="estudiante[nombres]" placeholder="<?php echo lang('names');?>" class="form-control" title="<?php echo lang('names');?>"  type="text">									
								</div>
								<?php echo form_error('estudiante[nombres]','<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('last_name');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<input name="estudiante[apellido_paterno]" placeholder="<?php echo lang('last_name');?>" class="form-control" title="<?php echo lang('last_name');?>"  type="text">										
								</div>
								<?php echo form_error('estudiante[apellido_paterno]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label for='email' class="col-md-4 control-label"><?php echo lang('second_last_name');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<input name="estudiante[apellido_materno]" placeholder="<?php echo lang('second_last_name');?>" id='email' class="form-control" title="<?php echo lang('second_last_name');?>"  type="text">				
								</div>
								<?php echo form_error('estudiante[apellido_materno]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>


						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('ci');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
									<input name="estudiante[ci]" title="<?php echo lang('ci');?>" placeholder="<?php echo lang('ci');?>" class="form-control" type="text">									
								</div>
								<?php echo form_error('estudiante[ci]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('ci_expedido');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
									<input name="estudiante[ci_expedido]" title="<?php echo lang('ci_expedido');?>" placeholder="<?php echo lang('ci_expedido');?>" class="form-control" type="text">									
								</div>
								<?php echo form_error('estudiante[ci_expedido]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>											 


					<!-- Text input-->
						<div class="form-group">
							<label for='student_birth' class="col-md-4 control-label"><?php echo lang('student_birth');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
									<input name="estudiante[fecha_nacimiento]" placeholder="<?php echo lang('student_birth');?>" id='student_birth' class="form-control" title="<?php echo lang('student_birth');?>"  type="text">				
								</div>
								<?php echo form_error('estudiante[fecha_nacimiento]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>		


						
						
					<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('cell_phone');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
									<input name="estudiante[celular]" title="<?php echo lang('cell_phone');?>" placeholder="<?php echo lang('cell_phone');?>" class="form-control" type="text">									
								</div>
								<?php echo form_error('estudiante[celular]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>		
						
					 <!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('telephone');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
									<input name="estudiante[telefono]" title="<?php echo lang('telephone');?>" placeholder="<?php echo lang('telephone');?>" class="form-control" type="text">									
								</div>
								<?php echo form_error('estudiante[telefono]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>	

					<!-- Text input-->
						<div class="form-group">
							<label for='gender' class="col-md-4 control-label"><?php echo lang('gender');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<input name="estudiante[genero]" placeholder="<?php echo lang('gender');?>" id='gender' class="form-control" title="<?php echo lang('gender');?>"  type="text">				
								</div>
								<?php echo form_error('estudiante[genero]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>
							
					<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('address');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
									<input name="estudiante[direccion]" title="<?php echo lang('name');?>" placeholder="<?php echo lang('address');?>" class="form-control" type="text">									
								</div>
								<?php echo form_error('estudiante[direccion]','<em class="error">','</em>'); ?>
							</div>
						</div>	


 					<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('email');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									 <span class="input-group-addon" id="basic-addon1">@</span>
									<input name="estudiante[email]" title="<?php echo lang('email');?>" placeholder="<?php echo lang('email');?>" class="form-control" type="text">									
								</div>
								<?php echo form_error('estudiante[email]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>	
						

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('tutor');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
									<input name="estudiante[tutor]" title="<?php echo lang('tutor');?>" placeholder="<?php echo lang('tutor');?>" class="form-control" type="text">									
								</div>
								<?php echo form_error('estudiante[tutor]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('ci_tutor');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
									<input name="estudiante[ci_tutor]" title="<?php echo lang('ci_tutor');?>" placeholder="<?php echo lang('ci_tutor');?>" class="form-control" type="text">									
								</div>
								<?php echo form_error('estudiante[ci_tutor]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('status');?></label>  
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<input name="estudiante[estado]" title="<?php echo lang('status');?>" placeholder="<?php echo lang('status');?>" class="form-control" type="text">									
								</div>
								<?php echo form_error('estudiante[estado]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>


						<!-- radio checks -->
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('active');?></label>
							<div class="col-md-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="estudiante[activo]" value="true" /> 										
									</label>
								</div>
								<?php echo form_error('estudiante[activo]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>
						
						<!-- Button -->
						<div class="form-group">                      
							<label class="col-md-4 control-label"></label>            
							<div class="col-md-4">
								<div class='error'></div>
								<button type="submit" class="btn btn-primary" >Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
							</div>
						</div>

					</fieldset>               
					<div id='messageBox'>         
					</div>

				</form>
			</div>			
			<!-- <div class="panel-footer"></div>	 -->
		</div>		
		

	</div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/estudiante/estudiante.js"></script>
<?php $this->load->view("layout/footer");?>