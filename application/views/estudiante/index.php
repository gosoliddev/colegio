<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>
  <div class="container">  
    <div class="panel panel-default ">  
      <!-- <div class="panel-heading">Registro</div>       -->
      <div class="panel-body well fixpanel">
        <form class="form-horizontal" id="formSearch">
          <div class="col-sm-12">
            <div class="form-group">
              <label class="control-label"><?php echo lang("student_rude")?></label>
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <input type="text" class="form-control" name='estudiante[rude]' placeholder="<?php echo lang("student_rude")?>" title="<?php echo lang("student_rude")?>">                                    
                <span class="input-group-btn">
                <button class="btn btn-default"><i class="glyphicon glyphicon-search" name="btnSearch"></i></button>    
                </span>
              </div>
            </div>
          </div>
        </form>                                
      </div>
      <div class="panel-footer">
        <button class="btn btn-primary" name="btnNewStudent">
          <i class="glyphicon glyphicon-plus"></i> <?php echo lang('new_student')?>
        </button>                               
      </div>
    </div>
    <div class="panel panel-info" id="panel-info">
      <input type="hidden" class="form-control" name='estudiante[id_estudiante]'>
      <div class="panel-heading"><?php echo lang("info_estudiante")?></div>              
      <!-- end toolbar-->
      <div class="panel-body">            
        <div class="row">
          <div class="col-sm-6">
            <table class="table table-striped table-condensed">
              <tbody>
                <tr>
                  <td><?php echo lang('ci');?></td>
                  <td><span id="ci"></span></td>
                </tr>
                <tr>
                  <td><?php echo lang('ci_expedido');?></td>
                  <td><span id="ci_expedido"></span></td>
                </tr>
                <tr>
                  <td><?php echo lang('complete_name');?></td>
                  <td><span id="complete_name"></span></td>
                </tr> 
                <tr>
                  <td><?php echo lang('student_rude');?></td>
                  <td><span id="student_rude"></span></td>
                </tr> 
                <tr>
                  <td><?php echo lang('previous_sie');?></td>
                  <td><span id="previous_sie"></span></td>
                </tr>                                          
              </tbody>
            </table>
          </div>
          <div class="col-sm-6">
            <table class="table table-striped">
              <tbody>
               <tr>
                <td><?php echo lang('telephone');?></td>
                <td><span id="telephone"></span></td>
              </tr>
              <tr>
                <td><?php echo lang('cell_phone');?></td>
                <td><span id="cell_phone"></span></td>
              </tr>       
              <tr>
                <td><?php echo lang('address');?></td>
                <td><span id="address"></span></td>
              </tr> 
              <tr>
                <td><?php echo lang('gender');?></td>
                <td><span id="gender"></span></td>
              </tr> 
              <tr>
                <td><?php echo lang('status');?></td>
                <td><span id="status"></span></td>
              </tr>             
            </tbody>
          </table>
        </div>
      </div>            
    </div>
    <div class="panel-footer">
     <button title="<?php echo lang('edit')?>" class="btn btn-default" name="btnEditStudent" >
       <i class=" glyphicon glyphicon-pencil"></i> <?php echo lang('edit')?>
     </button>  
   </div>
 </div>
 <div class="panel panel-primary">
  <div class="panel-heading"><?php echo  lang("list")?></div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <!-- start toolbar-->            
      <div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups" id="toolbar">
        <div class="btn-group btn-group-sm" role="group" aria-label="First group">
          <button  title="<?php echo lang('add_inscription')?>" class="btn btn-success" name="btnNewInscription">
            <i class=" glyphicon glyphicon-plus"></i> <?php echo lang('add_inscription')?>
          </button>                             
        </div>                       
      </div>  
      <!-- end toolbar-->
    </div>
  </div>             
  <table  id="tableKardex" class="table table-condensed table-hover table-striped" >
   <thead>
    <tr>
      <th data-column-id="id_kardex_detalle" data-visible="false" data-identifier="true"><?php echo  lang('id')?></th>
      <th data-column-id="rude" ><?php echo  lang('student_rude')?></th>                      
      <th data-column-id="gestion" ><?php echo  lang('curricular_year')?></th>
      <th data-column-id="curso" ><?php echo  lang('course')?></th>
      <th data-column-id="nivel" ><?php echo  lang('level')?></th>                    
    </tr>
  </thead>         
</table>
</div>
</div>	
</section> 
<!-- nuevo estudiante -->
<div class="modal fade" id="modal-new_estudent">
  <div class="modal-dialog modal-lg">
    <form class="form-horizontal" id="formNewEstudiante">
     <input type="hidden" class="form-control" name='estudiante[id_estudiante]'>
     <div class="modal-content">
      <div class="modal-header">                
        <h4 class="modal-title"><?php echo lang("new_student")?></h4>
      </div>
      <div class="modal-body">                       
       <div class="panel-body">
         <div class="row">
           <div class="col-sm-6">
             <div class="form-group">
              <label class="col-md-4 control-label"><?php echo lang("student_rude")?> *</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                  <input type="text" class="form-control" name='estudiante[rude]'  placeholder="<?php echo lang("student_rude")?>" title="<?php echo lang("student_rude")?>">                                        
                </div>    
              </div>                                            
            </div>
          </div>
          <div class="col-sm-6">
           <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("previous_sie")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <input type="text" class="form-control" name='estudiante[sie_anterior]' placeholder="<?php echo lang("previous_sie")?>" title="<?php echo lang("previous_sie")?>">
              </div>    
            </div>                                            
          </div>
        </div>
      </div>      
      <br/>              
      <div class="row">                          
        <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("ci")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <input type="text" class="form-control" name='estudiante[ci]' placeholder="<?php echo lang("ci")?>" title="<?php echo lang("ci")?>">                                                                                
              </div>    
            </div>                                            
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("ci_expedido")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <select name="estudiante[ci_expedido]" title="<?php echo lang('ci_expedido');?>" placeholder="<?php echo lang('ci_expedido');?>" class="form-control" >
                  <option value=""><?php echo lang('select')?></option>   
                  <option value="CB">Cochabamaba</option>
                  <option value="LP">La paz</option>
                  <option value="SC">Santa cruz</option>
                  <option value="BN">Beni</option>
                  <option value="OR">Oruro</option>
                  <option value="PN">Pando</option>
                  <option value="CH">Chuquisaca</option>
                  <option value="PT">Potosi</option>
                  <option value="TJ">Tarija</option>
                </select>                                                                                
              </div>    
            </div>                                            
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("last_name")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <input type="text" class="form-control" name='estudiante[apellido_paterno]' placeholder="<?php echo lang("last_name")?>" title="<?php echo lang("last_name")?>">                                                                                
              </div>    
            </div>                                            
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("second_last_name")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <input type="text" class="form-control" name='estudiante[apellido_materno]' placeholder="<?php echo lang("second_last_name")?>" title="<?php echo lang("second_last_name")?>">                                                                                
              </div>    
            </div>                                            
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("name")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <input type="text" class="form-control" name='estudiante[nombres]' placeholder="<?php echo lang("name")?>" title="<?php echo lang("name")?>">                                                                                
              </div>    
            </div>                                            
          </div>


        </div>
        <div class="col-sm-6">  
          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("date_birthday")?></label>
            <div class="col-md-8">
              <div class="input-group _fixDate">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>   
                <input type="text" class="form-control _maskDate" name='estudiante[fecha_nacimiento]' placeholder="<?php echo lang("date_birthday")?>" title="<?php echo lang("date_birthday")?>">  

              </div>

            </div>                                            
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("gender")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <select  class="form-control" name='estudiante[genero]' placeholder="<?php echo lang("gender")?>" title="<?php echo lang("gender")?>">
                  <option value=""><?php echo lang('select')?></option>
                  <option value="FEMENINO">FEMENINO</option>
                  <option value="MASCULINO">MASCULINO</option>
                </select>                                                                                
              </div>    
            </div>                                            
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("email")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <input type="text" class="form-control" name='estudiante[email]' placeholder="<?php echo lang("email")?>" title="<?php echo lang("email")?>">                                                                                
              </div>    
            </div>                                            
          </div>       
          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("cell_phone")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <input type="text" class="form-control" name='estudiante[celular]' placeholder="<?php echo lang("cell_phone")?>" title="<?php echo lang("cell_phone")?>">                                                                                
              </div>    
            </div>                                            
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label"><?php echo lang("telephone")?></label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></span>
                <input type="text" class="form-control" name='estudiante[telefono]' placeholder="<?php echo lang("telephone")?>" title="<?php echo lang("telephone")?>">                                                                                
              </div>    
            </div>                                            
          </div>  

        </div>     
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">         
            <label class="col-md-2 control-label"><?php echo lang("address")?></label>                
            <div class="col-md-10">                    
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                <input type="text" class="form-control" name='estudiante[direccion]' placeholder="<?php echo lang("address")?>" title="<?php echo lang("address")?>">                            
              </div>    
            </div>                                            
          </div> 
        </div>
      </div>      
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('cancel')?></button>
    <button type="submit" class="btn btn-primary" name='btnSave'><?php echo lang("save_change");?></button>
  </div>
</div>
</form>
</div>
</div>
<!-- nueva inscripcion -->
<div class="modal fade" id="modal-inscripcion">
  <div class="modal-dialog">
    <form class="form-horizontal" action="#" id="form-add-inscripcion">
      <input type="hidden" name="estudiante[rude]" value='0'/>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"><?php echo lang('add_inscription')?></h4>
        </div>
        <div class="modal-body">
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 control-label"><?php echo lang('course')?></label>
              <div class="col-md-10">
                <select name="curso[id_curso]" class="form-control"> 
                  <option value=""><?php echo lang('select')?></option>
                  <?php foreach ($lista_curso as $item): ?>
                    <option value="<?php echo $item->id_curso?>"><?php echo $item->descripcion?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
          </div>          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('cancel')?></button>
          <button type="submit" class="btn btn-primary"><?php echo lang('save_change')?></button>
        </div>
      </div>
    </form>
  </div>
</div>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="<?php echo base_url()?>assets/app/estudiante/estudiante.js"></script>
<?php $this->load->view("layout/footer");?>

