<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<?php
$curricular_year=curricular_year();
?>
<section>
	<div class="container">			
	<h3 class="text-info" style="text-decoration: underline;"><?php echo lang("assign_subject_to_course") ?></h3>
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label"><?php echo lang("curricular_year")?></label>
						<?php if (get_cookie("rid")!="ADM"): ?>
							<input type="text" class="form-control" value="<?php echo $curricular_year->inicio_gestion.' - '.$curricular_year->inicio_gestion;?>" disabled='true'/>
							<input type="hidden" name="gestion"  id="gestion" value="<?php echo $curricular_year->id_gestion; ?>" >
						<?php else: ?>
							<select class="form-control" name='gestion'  id="gestion">
								<option value=""><?php echo lang('select')?></option>
								<?php foreach ($gestion_list as $item): ?>
									<option value="<?php echo $item->id_gestion; ?>"><?php echo $item->inicio_gestion.' - '.$item->fin_gestion; ?></option>
								<?php endforeach ?>	            							
							</select>
						<?php endif ?>

					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label"><?php echo lang("level")?></label>
						<select class="form-control" name='nivel' >
							<option value=""><?php echo lang('select')?></option>
							<?php foreach ($nivel_list as $item): ?>
								<option value="<?php echo $item->id_nivel; ?>"><?php echo $item->descripcion; ?></option>
							<?php endforeach ?>	            							
						</select>
					</div>
				</div>	
				<div class="col-sm-4">
					<div class="form-group">
						<label class="control-label"><?php echo lang("course")?></label>
						<select class="form-control" name='curso' >
							<option value=""><?php echo lang('select')?></option>
							<?php foreach ($curso_list as $item): ?>
								<option value="<?php echo $item->id_curso; ?>"><?php echo $item->descripcion; ?></option>
							<?php endforeach ?>	            							
						</select>
					</div>
				</div>							
			</div>
		</div>
		<!-- start toolbar-->
		<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
			<div class="btn-group btn-group-sm" role="group" aria-label="First group">				   
				<a href="#" onclick='toolbar.save(event);'title="<?php echo lang('save_change')?>" class="btn btn-default "><i class=' glyphicon glyphicon-floppy-disk'></i> <?php echo lang('save_change')?></a> 				 			
			</div>			
		</div>
		<br />
		<!-- end toolbar-->
		<div class="panel panel-primary">

			<div class="panel-heading"><?php echo lang('setted_subarea')?></div>

			<table id="tableSubareaCurso" class="table table-condensed table-hover table-striped" >
				<thead>
					<tr> 
						<th></th>
						<th><?php echo lang('subarea')?></th>
						<th><?php echo lang('area')?></th>
						<th><?php echo lang('field')?></th>
					</tr>
				</thead> 
				<tbody>	
					<?php foreach ($subarea_list as $item): ?>
						<tr>
							<td>											
								<input type="hidden" name="subarea_curso[][id_curso_subarea]" value=''/>				
								<input type="hidden" name="subarea_curso[][id_subarea]" value='<?php echo $item->id_subarea?>'/>
								<input type="checkbox" name="subarea_curso[][activo]" value='<?php ((isset($item->activo) && $item->activo==true)?"TRUE":"FALSE");?>' <?php isset($item->activo)?"checked":''?>/>
							</td>						
							<td>
								<?php echo $item->descripcion;?>
							</td>
							<td>
								<?php echo $item->descripcion_area;?>
							</td>
							<td>
								<?php echo $item->descripcion_campo;?>
							</td>
						</tr>					
					<?php endforeach ?>	    				

				</tbody>        
			</table>		

			<div class="panel-footer"></div>	
		</div>

	</div>	
</section> 
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/plancurricular/asignar_subarea_curso.js"></script>
<?php $this->load->view("layout/footer");?>