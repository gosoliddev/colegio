<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<?php
$curricular_year=curricular_year();
$current_bimestre=current_bimestre();
$idusuarioSession=$usuario_session->id_usuario;
$sub=$this->input->post('subarea');
$subarea=isset($sub)?$sub:null;
?>
<section>

	<div class="container">
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<!-- Success message -->
				<form class="form-horizontal" action="plancurricular/configurarcriteriosmateria" method="post"  id="plan-curricular">    
					<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
					<?php if(validation_errors()):?>
						<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
						</div>
					<?php endif;?>

					<fieldset>

						<!-- Form Name -->
						<legend><?php echo lang('set_criterios_materia');?></legend>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang("curricular_year")?></label>
							<div class="col-md-4">
								<?php if (get_cookie("rid")!="ADM"): ?>
									<input type="text" class="form-control" value="<?php echo $curricular_year->inicio_gestion.' - '.$curricular_year->inicio_gestion;?>" disabled='true'/>
									<input type="hidden" name="gestion[id_gestion]" value="<?php echo $curricular_year->id_gestion; ?>" id='gestion' >
								<?php else: ?>
									<select class="form-control" name='gestion[id_gestion]' disabled="">
										<option value=""><?php echo lang('select')?></option>
										<?php foreach ($gestion_list as $item): ?>
											<option value="<?php echo $item->id_gestion; ?>" <?php echo set_select('gestion[id_gestion]',$item->id_gestion, ( !empty($gestion->id_gestion) && $gestion->id_gestion ==$item->id_gestion ? TRUE : FALSE )); ?>><?php echo $item->inicio_gestion.' - '.$item->fin_gestion; ?></option>
										<?php endforeach ?>	            							
									</select>
								<?php endif ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang("teacher")?></label>
							<div class="col-md-4">
								<select class="form-control" name='usuario[id_usuario]' >
									<option value=""><?php echo lang('select')?></option>

									<?php foreach ($usuario_list as $item): ?>
										<option value="<?php echo $item->id_usuario; ?>"
											<?php echo set_select('usuario[id_usuario]',$item->id_usuario, ( !empty($usuario->id_usuario) && ($usuario->id_usuario ==$item->id_usuario) ? TRUE : FALSE )); ?>><?php echo $item->apellido_paterno.' '.$item->apellido_materno.' '.$item->nombres; ?></option>
										<?php endforeach ?>	            																		
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"><?php echo lang("subarea")?></label>
								<div class="col-md-4">
									<select class="form-control" name='subarea[id_subarea]' >
										<option value=""><?php echo lang('select')?></option>
										<?php foreach ($subarea_list as $item): ?>
											<option value="<?php echo $item->id_subarea; ?>"
												<?php echo set_select('subarea[id_subarea]',$item->id_subarea, ( !empty($subarea->id_subarea) && $subarea->id_subarea ==$item->id_subarea ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
											<?php endforeach ?>	
										</select>
									</div>
								</div>	
								<div class="form-group">
									<label class="col-md-4 control-label"><?php echo lang("course")?></label>
									<div class="col-md-4">
										<select class="form-control" name='usuario_curso[id_usuario_curso]' >
											<option value=""><?php echo lang('select');?></option>
											<?php foreach ($usuario_curso_list as $item): ?>
												<?php if (existe_subarea_curso($subarea['id_subarea'],$curricular_year,$item->id_curso))	: ?>
												<option value="<?php echo $item->id_usuario_curso; ?>" data-id-curso="<?php echo $item->id_curso;?>"
													<?php echo set_select('usuario_curso[id_usuario_curso]',$item->id_usuario_curso, ( !empty($usuario_curso->id_usuario_curso) && $usuario_curso->id_usuario_curso ==$item->id_usuario_curso ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>	
												<?php else: ?>
													
												<?php endif ?>
												
												<?php endforeach ?>	
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-4 control-label"><?php echo lang("bi_monthly")?></label>
										<div class="col-md-4">
											<select class="form-control" name='bimestre[codigo]' title="<?php echo lang('bi_monthly');?>" >
												<option value=""><?php echo lang('select')?></option>
												<?php foreach ($bimestre_list as $item): ?>
													<option value="<?php echo $item->codigo; ?>"
														<?php echo set_select('bimestre[codigo]',$item->codigo, ( !empty($bimestre->codigo) && $bimestre->codigo ==$item->codigo ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
													<?php endforeach ?>	
												</select>
											</div>
										</div>	



										<div role="tabpanel">
											<!-- Nav tabs -->
											<ul class="nav nav-tabs" role="tablist">
												<?php $i=0; ?>
												<?php foreach ($dimension_list as $item): ?>
													<li role="presentation" class="<?php echo $i==0?"active":""?>">
														<a href="#<?php echo $item->codigo?>" aria-controls="<?php echo $item->codigo?>" role="tab" data-toggle="tab"><?php echo $item->descripcion;?></a>
													</li>
													<?php $i++; ?>
												<?php endforeach ?>	

											</ul>

											<!-- Tab panes -->
											<div class="tab-content">
												<?php $i=0; ?>

												<?php foreach ($dimension_list as $item1): ?>
													<div role="tabpanel" class="tab-pane <?php echo $i==0?"active":""?>" id="<?php echo $item1->codigo?>">
														<div class="container-fluid">
															<br />
															<div class="form-group ">													
																<div class="col-xs-5">
																	<select name="from[<?php echo $item1->codigo?>][]"  class="form-control multiselect-class"  multiple="multiple" data-right="#<?php echo $item1->codigo;?>_to" data-right-all="#multiselect_rightAll<?php echo $i;?>" data-right-selected="#multiselect_rightSelected<?php echo $i;?>" data-left-all="#multiselect_leftAll<?php echo $i;?>" data-left-selected="#multiselect_leftSelected<?php echo $i;?>">
																		<?php foreach ($criterio_list as $item2): ?>
																			<?php if ($item2->id_dimension==$item1->id_dimension && in_array($item2->id_criterio, $planglobal_list_selected)): ?>	
																				<option value="<?php echo $item2->id_criterio;?>" selected><?php echo $item2->descripcion;?></option>
																			<?php elseif ($item2->id_dimension==$item1->id_dimension):?>
																				<option value="<?php echo $item2->id_criterio;?>"><?php echo $item2->descripcion;?></option>
																			<?php endif ?>

																		<?php endforeach ?>

																	</select>
																</div>

																<div class="col-xs-2">
																	<button type="button" id="multiselect_rightAll<?php echo $i;?>" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>
																	<button type="button" id="multiselect_rightSelected<?php echo $i;?>" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
																	<button type="button" id="multiselect_leftSelected<?php echo $i;?>" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
																	<button type="button" id="multiselect_leftAll<?php echo $i;?>" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>
																</div>

																<div class="col-xs-5">
																	<select name="to[<?php echo $item1->codigo?>][]" id="<?php echo $item1->codigo;?>_to" class="form-control"  multiple="multiple">
																		<?php foreach ($criterio_list as $item2): ?>
																			<?php if ($item2->id_dimension==$item1->id_dimension && in_array($item2->id_criterio, $planglobal_list_selected)): ?>	
																				<option value="<?php echo $item2->id_criterio;?>" selected><?php echo $item2->descripcion;?></option>																	
																			<?php endif ?>

																		<?php endforeach ?>
																	</select>
																</div>
															</div>
														</div>
													</div>									 
													<?php $i++; ?>	
												<?php endforeach ?>	

											</div>
										</div>						
										<!-- Button -->
										<div class="form-group">                      
											<label class="col-md-4 control-label"></label>            
											<div class="col-md-4">
												<div class='error'></div>
												<button type="submit" class="btn btn-primary" name="send" value="1">Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
											</div>
										</div>						

									</fieldset>               
									<div id='messageBox'>         
									</div>

								</form>
							</div>			
							<!-- <div class="panel-footer"></div>	 -->
						</div>		


					</div>	
				</section>
				<?php $this->load->view("layout/container_footer");?>
				<?php $this->load->view("layout/scripts");?>
				<script type="text/javascript" src="assets/app/plancurricular/configurar_libro_pedagogico.js"></script>
				<?php $this->load->view("layout/footer");?>