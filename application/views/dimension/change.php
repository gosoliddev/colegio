<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header"); ?>
<?php

if(isset($dimension))
	{
		$id=$dimension->id_dimension;
		$cbxValue=($dimension->activo=="1"?true:false);			 
	}
	else
	{
		$id="";
		$cbxValue=false;
	}
?>
<section>

	<div class="container">
	<!-- start toolbar-->
        <div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">               
                <a href="#dimension/index" onclick='toolbar.home(event);' title="<?php echo lang('back')?>" class="btn btn-default"> 
                	<i class="glyphicon glyphicon-arrow-left"></i> <?php echo lang('back')?>
                </a>                  
            </div>
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                <div class="btn-group">
                   <!--  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="optAdvanced">
                        <li><a href="#">{{aplicarSolicitud}}</a></li>                        
                    </ul> -->
                </div>
            </div>
        </div>
        <br />
        <!-- end toolbar-->	
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<!-- Success message -->
				<form class="form-horizontal" action="dimension/change" method="post"  id="dimension">    
					<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
					<input type="hidden" name="id" value="<?php echo $id; ?>">
					<?php if(validation_errors()):?>
						<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
						</div>
					<?php endif;?>
					 
					<fieldset>

						<!-- Form Name -->
						<legend><?php echo lang('dimension_record');?></legend>

						
						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('description');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
									<input name="dimension[descripcion]" placeholder="<?php echo lang('description');?>" class="form-control" title="<?php echo lang('description');?>"  type="text" value="<?php echo set_value('dimension[descripcion]',isset($dimension->descripcion)?$dimension->descripcion:'');?>">										
								</div>
								<?php echo form_error('dimension[descripcion]',
								'<em class="error">','</em>');  ?>
							</div>
						</div>

						<!-- radio checks -->
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('active');?></label>
							<div class="col-md-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="dimension[activo]" value="1" <?php echo set_checkbox('dimension[activo]',true,$cbxValue);?>/> 										
									</label>
								</div>
								<?php echo form_error('dimension[activo]',
								'<em class="error">','</em>');  ?>
							</div>
						</div>

						
						
						<!-- Button -->
						<div class="form-group">                      
							<label class="col-md-4 control-label"></label>            
							<div class="col-md-4">
								<div class='error'></div>
								<button type="submit" class="btn btn-primary" >Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
							</div>
						</div>
						

					</fieldset>               
					<div id='messageBox'>         
					</div>

				</form>
			</div>			
			<!-- <div class="panel-footer"></div>	 -->
		</div>		
		
	</div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/dimension/dimension.js"></script>
<?php $this->load->view("layout/footer");?>