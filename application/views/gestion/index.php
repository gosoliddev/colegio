<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>

	<div class="container">
        <h3 class="text-info" style="text-decoration: underline;"><?php echo lang("management_curricular_year") ?></h3>
        <div class="panel panel-primary">
         <div class="panel-heading"><?php echo lang("list") ?></div>
         <div class="panel panel-default">
            <div class="panel-heading">
             <!-- start toolbar-->
             <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
                <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                    <a href="#gestion/change"  onclick='toolbar.create(event);' title="<?php echo lang('new')?>" class="btn btn-default btn-toolbar">
                        <i class=" glyphicon glyphicon-plus"></i> <?php echo lang('new')?>
                    </a>         
                    <a href="#gestion/change" data-rol='ADM' onclick='toolbar.edit(event);' title="<?php echo lang('edit')?>" class="btn btn-default btn-toolbar">
                        <i class=" glyphicon glyphicon-pencil"></i> <?php echo lang('edit')?>
                    </a>       
                    <a href="#gestion/delete" onclick='toolbar.trash(event);'title="<?php echo lang('trash')?>" class="btn btn-default btn-toolbar">
                        <i class=" glyphicon glyphicon-trash"></i> <?php echo lang('trash')?>
                    </a> 
                </div>                  
            </div>

            <!-- end toolbar-->
        </div>

    </div>


    <table id="tableGestion" class="table table-condensed table-hover table-striped" >
       <thead>               
        <tr>
            <th data-column-id="id_gestion" data-visible='false' data-identifier="true"><?php echo  lang('id')?></th>                    
            <th data-column-id="descripcion_unidad_educativa" ><?php echo  lang('educational_unit')?></th>
            <th data-column-id="inicio_gestion" ><?php echo  lang('start_management')?></th>
            <th data-column-id="fin_gestion" ><?php echo  lang('end_management')?></th>                    
            <th data-column-id="fecha_actualizacion"><?php echo  lang('update_date')?></th>
            <th data-column-id="activo" data-formatter="active"><?php echo  lang('active')?></th>

        </tr>
    </thead>         
</table>
<div class="panel-footer"></div>	
</div>

</div>	
</section> 
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/gestion/gestion.js"></script>
<?php $this->load->view("layout/footer");?>