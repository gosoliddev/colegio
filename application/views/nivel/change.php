<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<?php
$unidad_educativa=user_unidad_educativa();
if(isset($nivel))
{
	$id=$nivel->id_nivel;
	$cbxValue=($nivel->activo=="1"?true:false);	
}
else
{
	$id="";
	$cbxValue=false;
}
?>
<section>

	<div class="container">
		<!-- start toolbar-->
		<div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
			<div class="btn-group btn-group-sm" role="group" aria-label="First group">               
				<a href="#nivel/index" onclick='toolbar.home(event);' title="<?php echo lang('back')?>" class="btn btn-default"> 
					<i class="glyphicon glyphicon-arrow-left"></i> <?php echo lang('back')?>
				</a>                  
			</div>
			<div class="btn-group btn-group-sm" role="group" aria-label="First group">
				<div class="btn-group">
                   <!--  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="optAdvanced">
                        <li><a href="#">{{aplicarSolicitud}}</a></li>                        
                    </ul> -->
                </div>
            </div>
        </div>
        <br />
        <!-- end toolbar-->	
        <div class="panel panel-primary ">	
        	<!-- <div class="panel-heading">Registro</div>		 -->
        	<div class="panel-body well fixpanel">
        		<!-- Success message -->
        		<form class="form-horizontal" action="<?php echo base_url(uri_string());?>" method="post"  id="nivel">    
        			<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
        			<input type="hidden" name="id" value="<?php echo $id; ?>">
        			<?php if(validation_errors()):?>
        				<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
        				</div>
        			<?php endif;?>

        			<fieldset>

        				<!-- Form Name -->
        				<legend><?php echo lang('record_level');?></legend>

        				<!-- Text input-->

        				<div class="form-group ">
        					<label class="col-md-4 control-label" ><?php echo lang('educational_unit');?></label> 
        					<div class="col-md-4">
        						<div class="input-group">
        							<span class="input-group-addon"><i class="glyphicon glyphicon-education"></i></span>
        							<?php if (get_cookie("rid")!="ADM"): ?>
        								<input type="hidden" name=nivel[id_unidad_educativa] value="<?php echo $unidad_educativa->id_unidad_educativa?>" />
        								<input type="text" disabled="true" class="form-control" value="<?php echo $unidad_educativa->nombre?>" />
        							<?php else: ?>
        								<select name="nivel[id_unidad_educativa]" placeholder="<?php echo lang('educational_unit');?>" class="form-control" title="<?php echo lang('educational_unit');?>" >
        									<option value=""><?php echo lang('select') ?></option>
        									<?php foreach ($unidadeducativa_list as $item):?>
        										<option value="<?php echo $item->id_unidad_educativa; ?>" <?php echo set_select('nivel[id_unidad_educativa]',$item->id_unidad_educativa, ( !empty($nivel->id_unidad_educativa) && $nivel->id_unidad_educativa ==$item->id_unidad_educativa ? TRUE : FALSE )); ?>><?php echo $item->nombre; ?></option>
        									<?php endforeach;  ?>
        								</select>	
        							<?php endif ?>									
        						</div>
        						<?php echo form_error('nivel[id_unidad_educativa]',
        						'<em class="error">','</em>'); ?>
        					</div>
        				</div>					 
        				<!-- Text input-->

        				<div class="form-group">
        					<label class="col-md-4 control-label" ><?php echo lang('name');?></label> 
        					<div class="col-md-4">
        						<div class="input-group">
        							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>		
        							<input name="nivel[descripcion]" placeholder="<?php echo lang('name');?>" class="form-control" title="<?php echo lang('name');?>"  type="text" value="<?php echo set_value('nivel[descripcion]',isset($nivel->descripcion)?$nivel->descripcion:'');?>">		

        						</div>
        						<?php echo form_error('nivel[inicio_nivel]',
        						'<em class="error">','</em>'); ?>
        					</div>
        				</div>



        				<!-- radio checks -->
        				<div class="form-group">
        					<label class="col-md-4 control-label"><?php echo lang('active');?></label>
        					<div class="col-md-4">
        						<div class="checkbox">
        							<label>
        								<input type="checkbox" name="nivel[activo]" value="1" <?php echo set_checkbox('nivel[activo]',true,$cbxValue);?>/> 										
        							</label>
        						</div>
        						<?php echo form_error('nivel[activo]',
        						'<em class="error">','</em>'); ?>
        					</div>
        				</div>

        				<!-- Button -->
        				<div class="form-group">                      
        					<label class="col-md-4 control-label"></label>            
        					<div class="col-md-4">
        						<div class='error'></div>
        						<button type="submit" class="btn btn-primary" >Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
        					</div>
        				</div>

        			</fieldset>               
        			<div id='messageBox'>         
        			</div>

        		</form>
        	</div>			
        	<!-- <div class="panel-footer"></div>	 -->
        </div>		

    </div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/nivel/nivel.js"></script>
<?php $this->load->view("layout/footer");?>