<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>
 
	<div class="container">
    <h3 class="text-info" style="text-decoration: underline;"><?php echo lang("management_user") ?></h3>    
    
        <div class="panel panel-primary">
            <div class="panel-heading"><?php echo lang("list") ?></div>
            <div class="panel panel-default">
            <div class="panel-heading">

		 	<!-- start toolbar-->
        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                <a href="#usuario/change" onclick='toolbar.create(event);' title="<?php echo lang('new')?>" class="btn btn-default">
                            <i class=" glyphicon glyphicon-plus"></i> <?php echo lang('new')?></a>         
                      <a href="#usuario/change" onclick='toolbar.edit(event);' title="<?php echo lang('edit')?>" class="btn btn-default">
                            <i class=" glyphicon glyphicon-pencil"></i> <?php echo lang('edit')?></a>       
                     <!-- <a href="#usuario/delete" onclick='toolbar.trash(event);'title="<?php echo lang('trash')?>" class="btn btn-default ">
                            <i class=" glyphicon glyphicon-trash"></i> <?php echo lang('trash')?></a>  -->
            </div>
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                <div class="btn-group">
                  <!--   <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="optAdvanced">
                        <li><a href="#">{{aplicarSolicitud}}</a></li>                        
                    </ul> -->
                </div>
            </div>
        </div>
       
        <!-- end toolbar-->
		
         </div>

            </div>

			<table id="tableUsuario" class="table table-condensed table-hover table-striped" >
               <thead>
                <tr>
                    <th data-column-id="id_usuario" data-visible='false' data-identifier="true"><?php echo  lang('id')?></th>
                    <th data-column-id="descripcion_unidad_educativa" ><?php echo  lang('educational_unit')?></th>
                    <th data-column-id="apellido_paterno" ><?php echo  lang('last_name')?></th>
                    <th data-column-id="apellido_materno" ><?php echo  lang('second_last_name')?></th>
                    <th data-column-id="nombres"><?php echo  lang('name')?></th>
                    <th data-column-id="ci"><?php echo  lang('ci')?></th>
                    <th data-column-id="genero"><?php echo  lang('gender')?></th>
                    <th data-column-id="ci_expedido"><?php echo  lang('ci_expedido')?></th>                    
                    <th data-column-id="email"><?php echo  lang('email')?></th>                    
                    <th data-column-id="celular"><?php echo  lang('cell_phone')?></th>                                        
                    <th data-column-id="activo" data-formatter="active" data-sortable="false"><?php echo  lang('active')?></th>
                    <th data-column-id="fecha_nacimiento"><?php echo  lang('birth_date')?></th>
                    <th data-column-id="fecha_actualizacion"><?php echo  lang('update_date')?></th>                    
                </tr>
            </thead>         
            </table>	
			<div class="panel-footer"></div>	
		</div>

	</div>	
</section> 
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/usuario/usuario.js"></script>
<?php $this->load->view("layout/footer");?>