<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<?php
	if(isset($area))
	{
		$id=$area->id_area;
		$cbxValue=($area->activo=="1"?true:false);	
	}
	else
	{
		$id="";
		$cbxValue=false;
	}
?>
<section>

	<div class="container">
		<!-- start toolbar-->
        <div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">               
                <a href="#area/index" onclick='toolbar.home(event);' title="<?php echo lang('back')?>" class="btn btn-default"> 
                	<i class="glyphicon glyphicon-arrow-left"></i> <?php echo lang('back')?>
                </a>                  
            </div>
            <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                <div class="btn-group">
                   <!--  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="optAdvanced">
                        <li><a href="#">{{aplicarSolicitud}}</a></li>                        
                    </ul> -->
                </div>
            </div>
        </div>
        <br />
        <!-- end toolbar-->	
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<!-- Success message -->
				<form class="form-horizontal" action="area/change" method="post"  id="area">    
					<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
					<input type="hidden" name="id" value="<?php echo $id; ?>">
					<?php if(validation_errors()):?>
						<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
						</div>
					<?php endif;?>
					 
					<fieldset>

						<!-- Form Name -->
						<legend><?php echo lang('registration_area');?></legend>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('field');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
								 		
									<select name="area[id_campo]" placeholder="<?php echo lang('field');?>" class="form-control" title="<?php echo lang('field');?>" >
										<option value=""><?php echo lang('select') ?></option>
										<?php foreach ($campo_list as $item):?>
											<option value="<?php echo $item->id_campo; ?>" <?php echo set_select('area[id_campo]',$item->id_campo, ( !empty($area->id_campo) && $area->id_campo ==$item->id_campo ? TRUE : FALSE )); ?>><?php echo $item->descripcion; ?></option>
										<?php endforeach;  ?>
									</select>							
								</div>
								<?php echo form_error('area[id_campo]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>
						
						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('description');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
									<input name="area[descripcion]" placeholder="<?php echo lang('description');?>" class="form-control" title="<?php echo lang('description');?>"  type="text" value="<?php echo set_value('area[descripcion]',isset($area->descripcion)?$area->descripcion:'');?>">										
								</div>
								<?php echo form_error('area[descripcion]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- radio checks -->
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo lang('active');?></label>
							<div class="col-md-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="area[activo]" value="1" <?php echo set_checkbox('area[activo]',true,$cbxValue);?>  /> 										
									</label>
								</div>
								<?php echo form_error('area[activo]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						
						<!-- Button -->
						<div class="form-group">                      
							<label class="col-md-4 control-label"></label>            
							<div class="col-md-4">
								<div class='error'></div>
								<button type="submit" class="btn btn-primary" >Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
							</div>
						</div>
						

					</fieldset>               
					<div id='messageBox'>         
					</div>

				</form>
			</div>			
			<!-- <div class="panel-footer"></div>	 -->
		</div>		
		

	</div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/area/area.js"></script>
<?php $this->load->view("layout/footer");?>