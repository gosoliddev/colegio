<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>

	<div class="container">
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<!-- Success message -->
				<form class="form-horizontal" action="kardexcriterio/change" method="post"  id="kardexcriterio">    
					<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
					<?php if(validation_errors()):?>
						<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
						</div>
					<?php endif;?>
					 
					<fieldset>

						<!-- Form Name -->
						<legend><?php echo lang('kardex_criterio_record');?></legend>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('criterio');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
									<select name="kardexcriterio[id_criterio]" placeholder="<?php echo lang('criterio');?>" class="form-control" title="<?php echo lang('criterio');?>" ></select>										
								</div>
								<?php echo form_error('kardexcriterio[id_criterio]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('kardex_detail');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-education"></i></span>
									<select name="kardexcriterio[id_kardex_detalle]" placeholder="<?php echo lang('kardex_detail');?>" class="form-control" title="<?php echo lang('kardex_detail');?>" ></select>										
								</div>
								<?php echo form_error('kardexcriterio[id_kardex_detalle]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('bi_monthly');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
									<input name="kardexcriterio[bimestre]" placeholder="<?php echo lang('bi_monthly');?>" class="form-control" title="<?php echo lang('bi_monthly');?>"  type="text">										
								</div>
								<?php echo form_error('kardexcriterio[bimestre]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						
						<!-- Button -->
						<div class="form-group">                      
							<label class="col-md-4 control-label"></label>            
							<div class="col-md-4">
								<div class='error'></div>
								<button type="submit" class="btn btn-primary" >Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
							</div>
						</div>
						

					</fieldset>               
					<div id='messageBox'>         
					</div>

				</form>
			</div>			
			<!-- <div class="panel-footer"></div>	 -->
		</div>		
		

	</div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/kardexcriterio/kardexcriterio.js"></script>
<?php $this->load->view("layout/footer");?>