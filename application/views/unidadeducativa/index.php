<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>

	<div class="container">  
    <h3 class="text-info" style="text-decoration: underline;"><?php echo lang("management_educational_unit") ?></h3>            
        <!-- end toolbar-->
        <div class="panel panel-primary">
         <div class="panel-heading"><?php echo lang("list"); ?></div>
         <div class="panel panel-default">
            <div class="panel-heading">
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
                    <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                        <a href="#unidadeducativa/change" onclick='toolbar.create(event);' title="<?php echo lang('new')?>" class="btn btn-default">
                            <i class=" glyphicon glyphicon-plus"></i> <?php echo lang('new')?>
                        </a>         
                        <a href="#unidadeducativa/change" onclick='toolbar.edit(event);' title="<?php echo lang('edit')?>" class="btn btn-default">
                            <i class=" glyphicon glyphicon-pencil"></i> <?php echo lang('edit')?>
                        </a>       
                        <a href="#unidadeducativa/delete" onclick='toolbar.trash(event);'title="<?php echo lang('trash')?>" class="btn btn-default ">
                            <i class=" glyphicon glyphicon-trash"></i> <?php echo lang('trash')?>
                        </a> 
                    </div>
                    <div class="btn-group btn-group-sm" role="group" aria-label="First group">
                        <div class="btn-group">                 
                        </div>
                    </div>
                </div>
            </div>                
        </div>
        <table id="tableUnidadEducativa" class="table table-condensed table-hover table-striped" >
         <thead>
            <tr>
                <th data-column-id="id_unidad_educativa" data-visible="false" data-identifier="true"><?php echo lang('id')?></th>                                
                <th data-column-id="nombre" ><?php echo  lang('name')?></th>
                <th data-column-id="descripcion" ><?php echo  lang('description')?></th>
                <th data-column-id="codigo_sie" ><?php echo  lang('number_sie')?></th>
                <th data-column-id="dependencia"><?php echo  lang('dependency')?></th>
                <th data-column-id="activo" data-formatter="active"><?php echo  lang('active')?></th>

            </tr>
        </thead>         
    </table>	
    <div class="panel-footer"></div>	
</div>

</div>	
</section> 
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="<?php echo base_url()?>assets/app/unidadeducativa/unidadeducativa.js"></script>
<?php $this->load->view("layout/footer");?>