<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<?php
if(isset($unidadeducativa))
{
	$id=$unidadeducativa->id_unidad_educativa;
	$cbxValue=($unidadeducativa->activo=="1"?true:false);	
}
else
{
	$id="";
	$cbxValue=false;
}
?>
<section>

	<div class="container">
		<!-- start toolbar-->
		<div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups" id="solPnlToolBar">
			<div class="btn-group btn-group-sm" role="group" aria-label="First group">               
				<a href="#unidadeducativa/index" onclick='toolbar.home(event);' title="<?php echo lang('back')?>" class="btn btn-default"> 
					<i class="glyphicon glyphicon-arrow-left"></i> <?php echo lang('back')?>
				</a>                 
			</div>
			<div class="btn-group btn-group-sm" role="group" aria-label="First group">
				<div class="btn-group">
                   <!--  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="optAdvanced">
                        <li><a href="#">{{aplicarSolicitud}}</a></li>                        
                    </ul> -->
                </div>
            </div>
        </div>
        <br />
        <!-- end toolbar-->	
        <div class="panel panel-primary ">	
        	<!-- <div class="panel-heading">Registro</div>		 -->
        	<div class="panel-body well fixpanel">
        		<!-- Success message -->
        		<form class="form-horizontal" action="<?php echo base_url(uri_string());?>" method="post"  id="unidad-educativa">    
        			<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
        			<input type="hidden" name="id" value="<?php echo $id; ?>">
        			<?php if(validation_errors()):?>
        				<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
        				</div>
        			<?php endif;?>

        			<fieldset>

        				<!-- Form Name -->
        				<legend><?php echo lang('registro_unidad_educativa');?></legend>

        				<!-- Text input-->

        				<div class="form-group">
        					<label class="col-md-4 control-label"><?php echo lang('name');?></label>  
        					<div class="col-md-4">
        						<div class="input-group">
        							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        							<input  name="unidad_educativa[nombre]" placeholder="<?php echo lang('name');?>" class="form-control" title="<?php echo lang('name');?>"  type="text" value="<?php echo set_value('unidad_educativa[nombre]',isset($unidadeducativa->nombre)?$unidadeducativa->nombre:'');?>">									
        						</div>
        						<?php echo form_error('unidad_educativa[nombre]','<em class="error">','</em>'); ?>
        					</div>
        				</div>

        				<!-- Text input-->

        				<div class="form-group">
        					<label class="col-md-4 control-label" ><?php echo lang('description');?></label> 
        					<div class="col-md-4">
        						<div class="input-group">
        							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        							<input name="unidad_educativa[descripcion]" placeholder="<?php echo lang('description');?>" class="form-control" title="<?php echo lang('description');?>"  type="text" value="<?php echo set_value('unidad_educativa[descripcion]',isset($unidadeducativa->descripcion)?$unidadeducativa->descripcion:'');?>">										
        						</div>
        						<?php echo form_error('unidad_educativa[descripcion]','<em class="error">','</em>'); ?>
        					</div>
        				</div>

        				<!-- Text input-->
        				<div class="form-group">
        					<label for='email' class="col-md-4 control-label"><?php echo lang('number_sie');?></label>  
        					<div class="col-md-4">
        						<div class="input-group">
        							<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
        							<input name="unidad_educativa[codigo_sie]" placeholder="<?php echo lang('number_sie');?>" id='email' class="form-control" title="<?php echo lang('number_sie');?>"  type="text" value="<?php echo set_value('unidad_educativa[codigo_sie]',isset($unidadeducativa->codigo_sie)?$unidadeducativa->codigo_sie:'');?>">				
        						</div>
        						<?php echo form_error('unidad_educativa[codigo_sie]','<em class="error">','</em>'); ?>
        					</div>
        				</div>


        				<!-- Text input-->

        				<div class="form-group">
        					<label class="col-md-4 control-label"><?php echo lang('dependency');?></label>  
        					<div class="col-md-4">
        						<div class="input-group">
        							<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
        							<select name="unidad_educativa[dependencia]" title="<?php echo lang('name');?>" placeholder="<?php echo lang('dependency');?>" class="form-control" type="text" >	
        								<option value=""><?php echo lang('select') ?></option>
        								<option value="PÚBLICA"  <?php echo set_select('unidad_educativa[dependencia]','PÚBLICA', ( !empty($unidadeducativa->dependencia) && $unidadeducativa->dependencia =='PÚBLICA' ? TRUE : FALSE )); ?>>PÚBLICA</option>
        								<option value="COMUNITARIA"  <?php echo set_select('unidad_educativa[dependencia]','COMUNITARIA', ( !empty($unidadeducativa->dependencia) && $unidadeducativa->dependencia =='COMUNITARIA' ? TRUE : FALSE )); ?>>COMUNITARIA</option>
        								<option value="CONVENIO"  <?php echo set_select('unidad_educativa[dependencia]','CONVENIO', ( !empty($unidadeducativa->dependencia) && $unidadeducativa->dependencia =='CONVENIO' ? TRUE : FALSE )); ?>>CONVENIO</option>
        								<option value="PRIVADA"  <?php echo set_select('unidad_educativa[dependencia]','PRIVADA', ( !empty($unidadeducativa->dependencia) && $unidadeducativa->dependencia =='PRIVADA' ? TRUE : FALSE )); ?>>PRIVADA</option>
        							</select>								
        						</div>
        						<?php echo form_error('unidad_educativa[dependencia]','<em class="error">','</em>'); ?>
        					</div>
        				</div>											 

        				<!-- radio checks -->
        				<div class="form-group">
        					<label class="col-md-4 control-label"><?php echo lang('active');?></label>
        					<div class="col-md-4">
        						<div class="checkbox">
        							<label>
        								<input type="checkbox" name="unidad_educativa[activo]" value="1" <?php echo set_checkbox('unidad_educativa[activo]',true,$cbxValue);?>/> 										
        							</label>
        						</div>
        						<?php echo form_error('unidad_educativa[activo]','<em class="error">','</em>'); ?>
        					</div>
        				</div>



        				<!-- Button -->
        				<div class="form-group">                      
        					<label class="col-md-4 control-label"></label>            
        					<div class="col-md-4">
        						<div class='error'></div>
        						<button type="submit" class="btn btn-primary" ><?php echo  lang("confirm")?> <span class="glyphicon glyphicon-floppy-disk"></span></button>
        						<?php if($this->uri->segment(3)!==NULL){?>
        						<button type="submit" class="btn btn-primary" ><?php echo  lang("delete")?> <span class="glyphicon glyphicon-trash"></span></button>
        						<?php }?>
        					</div>
        				</div>

        			</fieldset>               
        			<div id='messageBox'>         
        			</div>

        		</form>
        	</div>			
        	<!-- <div class="panel-footer"></div>	 -->
        </div>				 
    </div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="<?php echo base_url()?>assets/app/unidadeducativa/unidadeducativa.js"></script>
<?php $this->load->view("layout/footer");?>