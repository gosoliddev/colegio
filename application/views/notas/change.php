<?php $this->load->view("layout/header");?>
<?php $this->load->view("layout/container_header");?>
<section>

	<div class="container">
		<div class="panel panel-primary ">	
			<!-- <div class="panel-heading">Registro</div>		 -->
			<div class="panel-body well fixpanel">
				<!-- Success message -->
				<form class="form-horizontal" action="notas/change" method="post"  id="notas">    
					<!-- <div class="alert alert-success text-center" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div> --> 
					<?php if(validation_errors()):?>
						<div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
						</div>
					<?php endif;?>
					 
					<fieldset>

						<!-- Form Name -->
						<legend><?php echo lang('record_notes');?></legend>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('kardex_criterio');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-education"></i></span>
									<select name="notas[id_kardex_criterio]" placeholder="<?php echo lang('kardex_criterio');?>" class="form-control" title="<?php echo lang('kardex_criterio');?>" ></select>										
								</div>
								<?php echo form_error('notas[id_kardex_criterio]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('bi_monthly_note');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
									<input name="notas[nota_bimestral]" placeholder="<?php echo lang('bi_monthly_note');?>" class="form-control" title="<?php echo lang('bi_monthly_note');?>"  type="text">										
								</div>
								<?php echo form_error('notas[nota_bimestral]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>


						<!-- Text input-->

						<div class="form-group">
							<label class="col-md-4 control-label" ><?php echo lang('note');?></label> 
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
									<input name="notas[nota]" placeholder="<?php echo lang('note');?>" class="form-control" title="<?php echo lang('note');?>"  type="text">										
								</div>
								<?php echo form_error('notas[nota]',
								'<em class="error">','</em>'); ?>
							</div>
						</div>

						
						<!-- Button -->
						<div class="form-group">                      
							<label class="col-md-4 control-label"></label>            
							<div class="col-md-4">
								<div class='error'></div>
								<button type="submit" class="btn btn-primary" >Send <span class="glyphicon glyphicon-floppy-disk"></span></button>
							</div>
						</div>
						

					</fieldset>               
					<div id='messageBox'>         
					</div>

				</form>
			</div>			
			<!-- <div class="panel-footer"></div>	 -->
		</div>		
		

	</div>	
</section>
<?php $this->load->view("layout/container_footer");?>
<?php $this->load->view("layout/scripts");?>
<script type="text/javascript" src="assets/app/notas/notas.js"></script>
<?php $this->load->view("layout/footer");?>