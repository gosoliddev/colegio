<!DOCTYPE html>
<html lang="en">
<head>	
	<base href="<?php echo  base_url();?>">
	<meta charset="utf-8">
	<title></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes"> 

	<link href="<?php echo base_url();?>assets/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/jquery.bootgrid-1.3.1/jquery.bootgrid.min.css" rel="stylesheet" type="text/css" />		
	<link href="<?php echo base_url();?>assets/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/bootstrap-duallistbox/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" /> 
	<link href="<?php echo base_url();?>assets/printArea/css/PrintArea.css" rel="stylesheet" type="text/css" /> 
	<link href="<?php echo base_url();?>assets/theme/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" language="javascript">
		var lang = new Array();
		<?php foreach($this->lang->language as $key => $val){ ?>
			lang['<?php echo $key; ?>']='<?php echo $val; ?>';
			<?php } ?>

		</script>
	</head>
	<body>
		