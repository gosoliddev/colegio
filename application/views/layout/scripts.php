<script src="<?php echo base_url();?>assets/jquery/jquery-1.12.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap-3.3.7/js/bootstrap.js"></script>      
<script src="<?php echo base_url();?>assets/jquery-validate/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/jquery-validate/localization/messages_es.min.js"></script>      
<script src="<?php echo base_url();?>assets/jquery-validate/localization/messages_es.min.js"></script> 
<script src="<?php echo base_url();?>assets/jquery.bootgrid-1.3.1/jquery.bootgrid.min.js"></script>      
<script src="<?php echo base_url();?>assets/moment/moment.js"></script>
<script src="<?php echo base_url();?>assets/moment/es.js"></script>
<script src="<?php echo base_url();?>assets/maskinput/jquery.maskedinput.min.js"></script> 
<script src="<?php echo base_url();?>assets/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>    
<script src="<?php echo base_url();?>assets/dateformat/js/jquery-dateFormat.js"></script>       
<script src="<?php echo base_url();?>assets/bootbox/bootbox.min.js"></script>    
<script src="<?php echo base_url();?>assets/select2/js/select2.full.min.js"></script>    
<script src="<?php echo base_url();?>assets/toaster/jquery.toaster.js"></script>  
<script src="<?php echo base_url();?>assets/multiselect/multiselect.min.js"></script>
<script src="<?php echo base_url();?>assets/printArea/js/jquery.PrintArea.js"></script>  
<script src="<?php echo base_url();?>assets/app/common/colegio.common.fn.js"></script>  
<script type="text/javascript" language="javascript">
	<?php if ($this->session->flashdata('message')): ?>
	<?php echo $this->session->flashdata('message');?>
<?php endif ?>
</script>