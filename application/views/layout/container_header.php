<header>
	<nav class="navbar navbar-default " role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Sis. calificaci&oacute;n</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">					
				<ul class="nav navbar-nav navbar-left">	
					<li class="dropdown">
						<a href="main"><i class='glyphicon glyphicon-home'></i></a>
					</li>					
					<li class="dropdown" data-rol='ADM,DIR'>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class='glyphicon glyphicon-education'></i>&nbsp;<?php echo lang('school');?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li data-rol='ADM'><a href="<?php echo  base_url('unidadeducativa');?>"><?php echo lang('management_educational_unit');?></a></li>
							<li data-rol='ADM,DIR'><a href="<?php echo  base_url('gestion');?>"><?php echo lang('management_curricular_year');?></a></li>					
							<li data-rol='ADM'><a href="<?php echo  base_url('cargo');?>"><?php echo lang('management_job_positions');?></a></li>
							<li data-rol='ADM'><a href="<?php echo  base_url('usuario');?>"><?php echo lang('management_user');?></a></li>							
							<li data-rol='ADM,DIR'><a href="<?php echo  base_url('nivel');?>"><?php echo lang('management_levels');?></a></li>
							<li data-rol='ADM,DIR'><a href="<?php echo  base_url('curso');?>"><?php echo lang('management_courses');?></a></li>	
						</ul>
					</li>				 
					<li class="dropdown" data-rol='ADM,DIR,SEC'>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class='glyphicon glyphicon-envelope'></i>&nbsp;<?php echo lang('curriculum');?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo  base_url('campo');?>"><?php echo lang('management_fields');?></a></li>							 
							<li><a href="<?php echo  base_url('area');?>"><?php echo lang('management_areas');?></a></li>							 
							<li><a href="<?php echo  base_url('subarea');?>"><?php echo lang('management_subjects');?></a></li>							
						</ul>
					</li>
					<li class="dropdown" data-rol='ADM,DIR,SEC'>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class='glyphicon glyphicon-envelope'></i>&nbsp;<?php echo lang('evaluation');?> <b class="caret"></b></a>
						<ul class="dropdown-menu">							
							<li><a href="<?php echo  base_url('dimension');?>"><?php echo lang('management_dimensions');?></a></li>
							<li><a href="<?php echo  base_url('criterio');?>"><?php echo lang('management_criteria');?></a></li>							
						</ul>
					</li>
					<li class="dropdown" data-rol='ADM,DIR,SEC,PRO'>
						<a href="#" class="dropdown-toggle " data-toggle="dropdown"><i class='glyphicon glyphicon-user'></i>&nbsp;<?php echo lang('curricular_plan');?> <b class="caret"></b></a>
						<ul class="dropdown-menu">													
							<li data-rol='ADM,DIR,SEC'><a href="<?php echo  base_url('plancurricular/materiacurso');?>"><?php echo lang('assign_subject_to_course');?></a></li>							
							<li data-rol='ADM,DIR,SEC'><a href="<?php echo  base_url('plancurricular/materiaprofesor');?>"><?php echo lang('assign_subject_to_teacher');?></a></li>
							<li data-rol='ADM,DIR,SEC'><a href="<?php echo  base_url('plancurricular/asesorcurso');?>"><?php echo lang('assign_teacher_to_course');?></a></li>							
							<li data-rol='PRO'><a href="<?php echo  base_url('plancurricular/configurarcriteriosmateria');?>"><?php echo lang('setup_pedagogic_book');?></a></li>	
						</ul>
					</li>
					<li class="dropdown" data-rol='DIR,SEC,PRO'>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class='glyphicon glyphicon-envelope'></i>&nbsp;<?php echo lang('inscription');?> <b class="caret"></b></a>
						<ul class="dropdown-menu">							
							<li><a href="estudiante" ><?php echo lang('student_inscription');?></a></li>	
						</ul>
					</li>
					<li class="dropdown" data-rol='DIR,SEC,PRO'>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class='glyphicon glyphicon-list'></i>&nbsp;<?php echo lang('qualification');?> <b class="caret"></b></a>
						<ul class="dropdown-menu">							
							<li data-rol='PRO'><a href="<?php echo  base_url('calificacion');?>" ><?php echo lang('qualification_student');?></a></li>
							<li data-rol='DIR,SEC'><a href="<?php echo  base_url('calificacion/boletin');?>" ><?php echo lang('report_card');?></a></li>
						</ul>
					</li>
					<li class="dropdown" data-rol='DIR,SEC,ASE,PRO'>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class='glyphicon glyphicon-list-alt'></i>&nbsp;<?php echo lang('reports');?> <b class="caret"></b></a>
						<ul class="dropdown-menu">					
						<!-- menu general -->		
							<li data-rol='DIR,SEC'><a href="<?php echo  base_url('reportes/aprobados');?>" ><?php echo lang('rpt_aprobados_by_course');?></a></li>
							<li data-rol='DIR,SEC'><a href="<?php echo  base_url('reportes/reprobados');?>" ><?php echo lang('rpt_reprobados_by_course');?></a></li>							
							<li data-rol='DIR,SEC'><a href="<?php echo  base_url('reportes/materias');?>" ><?php echo lang('rpt_by_subject');?></a></li>
							<!-- menu profesor -->		
							<li data-rol='PRO'><a href="<?php echo  base_url('reportes/profesor_aprobados');?>" ><?php echo lang('rpt_aprobados_by_course');?></a></li>
							<li data-rol='PRO'><a href="<?php echo  base_url('reportes/profesor_reprobados');?>" ><?php echo lang('rpt_reprobados_by_course');?></a></li>							
							<li data-rol='PRO'><a href="<?php echo  base_url('reportes/materias_profesor');?>" ><?php echo lang('rpt_by_subject');?></a></li>
							
							<!-- menu asesor -->		
							<li data-rol='ASE'><a href="<?php echo  base_url('reportes/asesor_aprobados');?>" ><?php echo lang('rpt_aprobados_by_course');?></a></li>
							<li data-rol='ASE'><a href="<?php echo  base_url('reportes/asesor_reprobados');?>" ><?php echo lang('rpt_reprobados_by_course');?></a></li>							
							<li data-rol='ASE'><a href="<?php echo  base_url('reportes/materias_asesor');?>" ><?php echo lang('rpt_by_subject');?></a></li>

							<li data-rol='DIR,SEC'><a href="<?php echo  base_url('reportes/upload');?>" ><?php echo lang('verify_students');?></a></li>
						</ul>
					</li>
				</ul>
				<ul class='nav navbar-nav navbar-right' style='font-size:9pt;'>
					<li class='dropdown hide'>
						<a href="javascript:(void)"  class="text-danger" id='time'> 
							<?php echo date("Y-m-d H:i");?> </a>					
						</li>			
						<li class='dropdown'>
							<a id='ss_user' href='#' class='dropdown-toggle'  data-toggle='dropdown'><span class='glyphicon glyphicon-user'><span class='caret'></span></span></a>
							<ul class='dropdown-menu' role='menu' id='selectedRol'>								
								<li>
									<a href='usuarios/info'>
										<span class='glyphicon glyphicon-info-sign'></span> <?php echo lang('info_account')?>
									</a>
								</li>
								<li>
									<a href='javascript:(void)'>
										<span class='glyphicon glyphicon-user'></span><span id='drprol'>&nbsp;<?php echo user_name();?></span>
									</a>
								</li>
								<li class='divider'></li>
								<li>
									<a href='logon/logout'>
										<span class='glyphicon glyphicon-remove'></span> <?php echo lang('logout')?>
									</a>
								</li>
							</ul>
						</li>
					</ul>				

				</div><!-- /.navbar-collapse -->
			</div>
		</nav>
	</header>