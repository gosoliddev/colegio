<?php $this->load->view("layout/header");?>

<section >
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
        <br />
        <br />
        <br />
           <!--  <h1 class="text-center login-title">Identificate</h1> -->
            <div class="account-wall">

                <img class="profile-img" src="assets/img/photo.png" alt="">
                <form class="form-signin" method="POST" action="logon/main">
                <input type="hidden" name="login" value="1" >
                <div class="form-group">
                    <input type="text" name='username' class="form-control" placeholder="Email"  autofocus value="<?php echo set_value('username');?>">
                </div>
                <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name='password' value="<?php echo set_value('password');?>">
                </div>
                <div class="form-group">
                    <button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo lang('singin')?></button>   
                </div>
                <div class="form-group">             
               <!--  <label class="checkbox-inline">
                    <input type="checkbox" value="remember-me"><?php echo lang('remeber_me')?></button>
                </label>    -->             
                </div>
                 <?php if(validation_errors()):?>
                        <div class="alert alert-danger text-center" role="alert" id="success_message">Error <i class="glyphicon glyphicon-thumbs-down"></i><?php echo validation_errors('<li >','</li>'); ?>
                        </div>
                    <?php endif;?>
                </form>
            </div>
            <!-- <a href="#" class="text-center new-account"><?php echo lang('forgot_password')?></a> -->
        </div>
    </div>
</div>
</section>
<?php $this->load->view("layout/scripts");?>
<?php $this->load->view("layout/footer");?>