<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curso extends MY_Controller {
	var $model='mcurso';
	
	public function index()
	{
		$this->load->view("curso/index");
	}
	public function load(){		
		$this->load->model($this->model);
		$data = $this->input->post();
		$pageSize = $this->input->post("rowCount");
		$offset = $this->input->post("current");
		$sort = array();
		$search=null;
		unset($data["rowCount"]);
		unset($data["current"]);
		if (array_key_exists('sort', $data)) {
			$sort = $data['sort'];
			unset($data["sort"]);
		}
		if (isset($data['searchPhrase'])) {
			$search = $data['searchPhrase'];
			unset($data["searchPhrase"]);
		}
		$mcurso = new Mcurso();
		$result = $mcurso->load($search, $pageSize, $offset, $sort);
		if (sizeof($result) > 0) {
			$object = (object)['rows' => $result, 'total' => $result[0]->total, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
		} else {
			$object = (object)['rows' => $result, 'total' => 0, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
		}
		echo json_encode($object);
	}
	public function change(){		 		
		$now=date("Y-m-d H:i:s");	
		$usr=$this->session->userdata('usr');	
		$this->load->model($this->model);	
		$this->load->model('mnivel');		
		$mnivel=new mnivel();
		$gestion=new mnivel();	
		$curso=new mcurso();
		$id=$this->uri->segment(3);
		$data['curso']=NULL;
		$data['nivel']=$mnivel->_list();
		$data['parallel_list']=$this->getParallel();
		$data['grade_list']=$this->getGrade();
		if(isset($id)){
			$data['curso']=$curso->get($id);				
		}
		$this->form_validation->set_rules($this->validation->config[$this->model]);		
		if ($this->form_validation->run() == FALSE) {			 
			$this->load->view("curso/change",$data);			 
		} else {	
			$id=$this->input->post("id");
			$row=$this->input->post("curso");
			if(!isset($row['activo']))
				$row['activo']=0;
			else
				$row['activo']=1;
			if(isset($id) && $id!='')
			{
				$row["usuario_actualizacion"]=$usr->id_usuario;                        
				$row["fecha_actualizacion"]=$now;                   
			}
			else
			{
				$row["fecha_registro"]=$now;
				$row["fecha_actualizacion"]=$now;   
				$row["usuario_registro"]=$usr->id_usuario;  
			}		
			$curso->save($row,$id);			
			success();	
			redirect('curso/index', 'refresh');			
		}		
	}
	function getParallel(){
		$parallel=array('A','B','C','D','E');
		return $parallel;
	}
	function getGrade(){
		$grade=array('1','2','3','4','5','6');
		return $grade;
	}	
	public function checkduplicate(){	
		$this->load->model($this->model);
		$data=$this->input->post('curso');		
		$mcurso=new mcurso();
		$response=null;
		$id=$this->uri->segment(3);		
		$response=$mcurso->checkDuplicate($data['id_nivel'],$data['paralelo'],$data['grado']);					
		echo $response;			 
	}
	public function delete(){	
		$this->load->model($this->model);
		$mcurso=new mcurso();
		$id=$this->uri->segment(3);
		$mcurso->delete($id);
		success();
		redirect('curso/index', 'refresh');
	}
	public function courseByIdGestion(){
		$this->load->model($this->model);	
		$idGestion=$this->input->post('id_gestion');		
		$mcurso=new mcurso();
		$object=$mcurso->courseByIdGestion($idGestion);
		echo json_encode($object);
	}	
	public function findByIdNivel(){
		$this->load->model($this->model);	
		$idNivel=$this->input->post('id_nivel');				
		$mcurso=new mcurso();
		if(!empty($idNivel))
			$data['curso_list']=$mcurso->findByIdNivel($idNivel);
		else
			$data['curso_list']=array();
		echo json_encode($data);
	}	
}

/* End of file Curso.php */
/* Location: ./application/controllers/Curso.php */