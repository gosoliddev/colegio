<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kardexdetalle extends MY_Controller {
	var $model='mkardexdetalle';
	public function index()
	{
		$this->load->view("kardexdetalle/index");
	}
	public function change(){		 		
		$now=date("Y-m-d H:i:s");
		$this->load->model($this->model);		
		$this->form_validation->set_rules($this->validation->config[$this->model]);	
		if ($this->form_validation->run() == FALSE) {
			$this->load->view("kardexdetalle/change");
		} else {			
			$this->load->view("kardexdetalle/change");
		}		
	}
	
	public function load(){		
		$this->load->model($this->model);
		$data=$this->input->post();
		$pageSize=$this->input->post("rowCount");		
		$offset=$this->input->post("current");	

		//$fromDate=$data["fecha_actualizacon"]["fromDate"];
		//$toDate=$data["fecha_actualizacon"]["toDate"];
		
		unset($data["rowCount"]);
		unset($data["current"]);	
		unset($data["sort"]);	
		
		//unset($data["fecha_actualizacon"]);	
		//unset($data["fecha_registro"]);		
		//$data['fecha_actualizacon >=']=$fromDate;
		//$data['fecha_actualizacon <=']=$toDate;
		$mkardexdetalle=new MKardexdetalle();
		$result=$mkardexdetalle->load($data,$pageSize,$offset);		
		$object = (object) ['rows' => $result,'total'=>$result[0]->total,'current'=>(int)$this->input->post("current"),'rowCount'=>(int)$this->input->post("rowCount")];				
		echo json_encode($object);
	}
	public function agregarInscripcion(){
		$date=date('Y-m-d h:m');
		$usr=$this->session->userdata('usr');
		$this->load->model($this->model);
		$this->load->model('mkardex');
		$this->load->model('mgestion');
		$mkardex=new mkardex();
		$mgestion=new mgestion();		
		$estudiante=$this->input->post("estudiante");
		$curso=$this->input->post("curso");
		$gestion=$mgestion->findByCurrent($date);		
		$kardex=$mkardex->encontrarPorRudeEstudiante($estudiante['rude']);	
		$mkardexdetalle=new mkardexdetalle();
		$dataIn=array(
			'id_kardex'=>$kardex->id_kardex,
			'id_gestion'=>$gestion->id_gestion,
			'id_curso'=>$curso['id_curso'],
			'activo'=>1,
			'fecha_registro'=>$date,
			'fecha_actualizacion'=>$date,
			'usuario_registro'=>$usr->id_usuario,
			'usuario_actualizacion'=>$usr->id_usuario);
		$idKrdexDetalle=$mkardexdetalle->save($dataIn,NULL);
		if($idKrdexDetalle){
			$object = (object) ['status' =>1,'message'=>lang('success_message')];
			echo json_encode($object);
			exit(0);
		}
		$object = (object) ['status' =>0,'message'=>lang('error_message')];
		echo json_encode($object);		

	}
}

/* End of file CursoSubarea.php */
/* Location: ./application/controllers/CursoSubarea.php */