<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kardexcriterio extends MY_Controller {
	var $model='mkardexcriterio';
	public function index()
	{
		$this->load->view("kardexcriterio/index");
	}
	public function change(){		 		
		$now=date("Y-m-d H:i:s");
		$this->load->model($this->model);		
		$this->form_validation->set_rules($this->validation->config[$this->model]);	
		if ($this->form_validation->run() == FALSE) {
			$this->load->view("kardexcriterio/change");
		} else {			
			$this->load->view("kardexcriterio/change");
		}		
	}
	
	public function load(){		
		$this->load->model($this->model);
		$data=$this->input->post();
		$pageSize=$this->input->post("rowCount");		
		$offset=$this->input->post("current");	

		//$fromDate=$data["fecha_actualizacon"]["fromDate"];
		//$toDate=$data["fecha_actualizacon"]["toDate"];
		
		unset($data["rowCount"]);
		unset($data["current"]);	
		unset($data["sort"]);	
		
		//unset($data["fecha_actualizacon"]);	
		//unset($data["fecha_registro"]);		
		//$data['fecha_actualizacon >=']=$fromDate;
		//$data['fecha_actualizacon <=']=$toDate;
		$mkardexcriterio=new MKardexcriterio();
		$result=$mcurso->load($data,$pageSize,$offset);		
		$object = (object) ['rows' => $result,'total'=>$result[0]->total,'current'=>(int)$this->input->post("current"),'rowCount'=>(int)$this->input->post("rowCount")];				
		echo json_encode($object);
	}

}

/* End of file Kardexcriterio.php */
/* Location: ./application/controllers/Kardexcriterio.php */