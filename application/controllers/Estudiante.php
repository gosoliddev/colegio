<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudiante extends MY_Controller {
	var $model='mestudiante';
	public function index()
	{
		$this->load->model('mcurso');
		$mcurso=new MCurso();
		$data['lista_curso']=$mcurso->_list();
		$this->load->view("estudiante/index",$data);
	}
	public function change(){		 		
		$now=date("Y-m-d H:i:s");
		$this->load->model($this->model);		
		$this->form_validation->set_rules($this->validation->config[$this->model]);	
		if ($this->form_validation->run() == FALSE) {
			$this->load->view("estudiante/change");
		} else {			
			$this->load->view("estudiante/change");
		}		
	}
	
	public function load(){		
		$this->load->model($this->model);
		$data=$this->input->post();
		$pageSize=$this->input->post("rowCount");		
		$offset=$this->input->post("current");	
		unset($data["rowCount"]);
		unset($data["current"]);	
		unset($data["sort"]);	
		$munidadEducativa=new MUnidadEducativa();
		$result=$munidadEducativa->load($data,$pageSize,$offset);		
		$object = (object) ['rows' => $result,'total'=>$result[0]->total,'current'=>(int)$this->input->post("current"),'rowCount'=>(int)$this->input->post("rowCount")];				
		echo json_encode($object);
	}
	public function findByRude(){		 				 
		$this->load->model($this->model);	
		$rude=$this->input->post('rude');
		$mestudiante=new mestudiante();
		$data['estudiante']=$mestudiante->findByRude($rude);		
		echo  json_encode($data);
	}
	public function encontrarPorid(){		 				 
		$this->load->model($this->model);	
		$id_estudiante=$this->input->post('id_estudiante');
		$mestudiante=new mestudiante();
		$data['estudiante']=$mestudiante->encontrarPorId($id_estudiante);		
		echo  json_encode($data);
	}
	public function new_estudiante(){
		$now=date("Y-m-d H:i:s");
		$usr=$this->session->userdata('usr');
		$this->load->model($this->model);
		$this->load->model('mkardex');
		$this->load->model('mgestion');		
		$data=$this->input->post("estudiante");
		$mestudiante=new mestudiante();
		$mkardex=new mkardex();
		$mgestion=new mgestion();	
		$rude=$data['rude'];
		$ci=$data['ci'];
		$rudeFound=$mestudiante->findByRude($rude);			
		$ciFound=$mestudiante->findByCi($ci);
		$id_estudiante=NULL;
		if(!isset($data['id_estudiante']) || $data['id_estudiante']==''){
			if($rudeFound ){
				$object = (object) ['status' =>3,'message'=>lang('existing_rude')];
				echo json_encode($object);
				exit(0);
			}
			if($ciFound ){
				$object = (object) ['status' =>3,'message'=>lang('existing_ci')];
				echo json_encode($object);
				exit(0);
			}
		}
		if(isset($data['id_estudiante']) &&  $data['id_estudiante']==''){
			unset($data['id_estudiante']);
			$data["fecha_registro"]=$now;
			$data["fecha_actualizacion"]=$now;
			$data["estado"]=1;
			$data["usuario_registro"]=$usr->id_usuario;
			$data["usuario_actualizacion"]=$usr->id_usuario;
		}
		else{
			$id_estudiante=$data['id_estudiante'];			
			$data["fecha_actualizacion"]=$now;			
			$data["usuario_actualizacion"]=$usr->id_usuario;
		}		
		
		$isSaveEstudiante=$mestudiante->save($data,$id_estudiante);
		if($isSaveEstudiante>0){
			$gestion=$mgestion->findByCurrent(date('Y-m-d h:m'));			
			$kardex=array(
				"id_gestion"=>$gestion->id_gestion,
				"id_estudiante"=>$isSaveEstudiante,
				'activo'=>1,
				"fecha_registro"=>$now,
				"fecha_actualizacion"=>$now,
				"usuario_registro"=>$usr->id_usuario,
				"usuario_actualizacion"=>$usr->id_usuario
				);
			$isSaveKardex=$mkardex->save($kardex,NULL);
		}		
		$object = (object) ['status' =>1,'message'=>lang('success_message')];
		echo json_encode($object);
	}		
}

/* End of file Estudiante.php */
/* Location: ./application/controllers/Estudiante.php */