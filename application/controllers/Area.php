<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends MY_Controller {
	var $model='marea';
	public function index()
	{
		$this->load->view("area/index");
	}
	 
	
	public function load(){		
		$this->load->model($this->model);
        $data = $this->input->post();
        $pageSize = $this->input->post("rowCount");
        $offset = $this->input->post("current");
        $sort = array();
        $search=null;
        unset($data["rowCount"]);
        unset($data["current"]);
        if (array_key_exists('sort', $data)) {
            $sort = $data['sort'];
            unset($data["sort"]);
        }
        if (isset($data['searchPhrase'])) {
            $search = $data['searchPhrase'];
            unset($data["searchPhrase"]);
        }
        $marea = new Marea();
        $result = $marea->load($search, $pageSize, $offset, $sort);
        if (sizeof($result) > 0) {
            $object = (object)['rows' => $result, 'total' => $result[0]->total, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
        } else {
            $object = (object)['rows' => $result, 'total' => 0, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
        }
        echo json_encode($object);
	}
    public function change()
    {
        $now = date("Y-m-d H:i:s");
        $usr=$this->session->userdata('usr');
        $this->load->model($this->model);
        $this->load->model('mcampo');   
        $area = new Marea();    
        $mcampo=new mcampo();
        $id = $this->uri->segment(3);
        $data['campo_list']=$mcampo->_list();
        $data['area'] = NULL;
        if (isset($id)) {
            $data['area'] = $area->get($id);
        }
        $this->form_validation->set_rules($this->validation->config[$this->model]);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view("area/change", $data);
        } else {
            $id = $this->input->post("id");
            $row = $this->input->post("area");
            if(!isset($row['activo']))
                $row['activo']=0;
            else
                $row['activo']=1;
            if(isset($id) && $id!='')
             {
                $row["usuario_actualizacion"]=$usr->id_usuario;                        
                $row["fecha_actualizacion"]=$now;                   
                }
            else
            {
                $row["fecha_registro"]=$now;
                $row["fecha_actualizacion"]=$now;   
                $row["usuario_registro"]=$usr->id_usuario;  
            }
            $area->save($row, $id);
            redirect('area/index', 'refresh');
        }
    }

    public function delete()
    {
        $this->load->model($this->model);
        $marea = new Marea();
        $id = $this->uri->segment(3);
        $marea->delete($id);
        redirect('area/index', 'refresh');
    }
    public function findByIdCampo(){
        $this->load->model('marea');
        $marea=new Marea();
        $data['area_list']=$marea->findByIdCampo($this->input->post("id_campo"));
        echo  json_encode($data);
    }

}

/* End of file Area.php */
/* Location: ./application/controllers/Area.php */