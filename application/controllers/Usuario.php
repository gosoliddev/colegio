<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends MY_Controller {
	var $model='musuario';	 
	public function index(){		 				
		$this->load->view("usuario/index");	 
	}
	public function load(){		
		$this->load->model($this->model);
		$data = $this->input->post();
		$pageSize = $this->input->post("rowCount");
		$offset = $this->input->post("current");
		$sort = array();
		$search=null;
		unset($data["rowCount"]);
		unset($data["current"]);
		if (array_key_exists('sort', $data)) {
			$sort = $data['sort'];
			unset($data["sort"]);
		}
		if (isset($data['searchPhrase'])) {
			$search = $data['searchPhrase'];
			unset($data["searchPhrase"]);
		}
		$musuario = new Musuario();
		$result = $musuario->load($search, $pageSize, $offset, $sort);
		if (sizeof($result) > 0) {
			$object = (object)['rows' => $result, 'total' => $result[0]->total, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
		} else {
			$object = (object)['rows' => $result, 'total' => 0, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
		}
		echo json_encode($object);
	}
	public function change(){		 		
		$now=date("Y-m-d H:i:s");		
		$usr=$this->session->userdata('usr');
		$this->load->model($this->model);	
		$this->load->model('munidadeducativa');	
		$this->load->model('mcargo');	
		$usuario=new musuario();
		$munidadeducativa=new munidadeducativa();
		$mcargo=new mcargo();
		$id=$this->uri->segment(3);
		$data['usuario']=NULL;
		if(isset($id)){
			$data['usuario']=$usuario->get($id);				
			$data["cargo"]=$mcargo->findByIdUsuario($id);
		}
		$data['cargo_list']=$mcargo->_list();
		$data["expedicion_list"]=$this->getExpedicion();
		$data['unidadeducativa_list']=$munidadeducativa->_list();
		$this->form_validation->set_rules($this->validation->config[$this->model]);					
		if ($this->form_validation->run() == FALSE) {			 
			$this->load->view("usuario/change",$data);			 
		} else {	
			$id=$this->input->post("id");
			$row=$this->input->post("usuario");
			$cargos=$this->input->post('cargo');
			if(!isset($row['activo']))
				$row['activo']=0;
			else
				$row['activo']=1;
			if(isset($id) && $id!='')
			{
				$row["usuario_actualizacion"]=$usr->id_usuario;                        
				$row["fecha_actualizacion"]=$now;                   
			}
			else
			{
				$row["fecha_registro"]=$now;
				$row["fecha_actualizacion"]=$now;   
				$row["usuario_registro"]=$usr->id_usuario;  
				$row["usuario_actualizacion"]=$usr->id_usuario;  
			}	
			if(isset($id) && $id!='')
				unset($row['clave']);		
			else{
				$row['clave']=md5($row['clave']);		
			}

			$idusuario=$usuario->save($row,$id);	
			if(isset($id) && $id!=''){			
				$mcargo->deleteByIdUsuario($id);		
				foreach ($cargos as $value) {
					$usuarioCargo=array(
						'id_cargo'=>$value,
						'id_usuario'=>$id
						);				
					$mcargo->saveCargos($usuarioCargo);
				}		
			}else{				
				foreach ($cargos as $value) {
					$usuarioCargo=array(
						'id_cargo'=>$value,
						'id_usuario'=>$idusuario
						);				
					$mcargo->saveCargos($usuarioCargo);
				}	
			}			
			success();
			redirect('/usuario/index');
		}		
	}
	function getExpedicion(){
		$expedicion=array('BN'=>'BENI',
			'CH'=>'CHIQUISACA',
			'CB'=>'COCHABAMBA',
			'LP'=>'LA PAZ ',
			'OR'=>'ORURO',
			'PD'=>'PANDO',
			'PT'=>'POTOSI',
			'SZ'=>'SANTA CRUZ',
			'TR'=>'TARIJA',
			);
		return $expedicion;
	}
	public function delete(){	
		$this->load->model($this->model);
		$musuario=new musuario();
		$id=$this->uri->segment(3);
		$musuario->delete($id);
		redirect('/usuario/index');
	}
}

/* End of file usuario.php */
/* Location: ./application/controllers/UnidadEducativa.php */