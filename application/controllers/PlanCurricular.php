<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PlanCurricular extends MY_Controller
{
  var $model = 'munidadeducativa';
  var $modelasesorcurso='musuariocurso';
  var $codigo_cargo_profesor='PRO';
  var $codigo_cargo_asesor="ASE";
  var $_mgestion="mgestion";
  var $_msubarea="msubarea";
  var $_musuario="musuario";
  var $_mnivel="mnivel";
  var $_usuariosubarea='musuariosubarea';
  var $_mdimension="mdimension";
  var $_mcriterio="mcriterio";
  var $_mplanglobal="mplanglobal";
  var $_muplanglobal_rules="mplanglobal";
  var $_mbimestre="mbimestre";
  public function index()
  {
    $this->load->view("plancurricular/index");
  }

  public function materiaprofesor()
  {
   $this->load->model($this->_msubarea);        
   $this->load->model($this->_mgestion);
   $this->load->model($this->_musuario);
   $mgestion=new mgestion();                 
   $msubarea=new msubarea();       
   $musuario=new musuario();
   $data['subarea_list']=$msubarea->_list();       
   $data['gestion_list']=$mgestion->_list();
   $data['usuario_list']=$musuario->findByCodigoCargo($this->codigo_cargo_profesor);
   $this->form_validation->set_rules($this->validation->config[$this->model]);        
   $this->load->view("plancurricular/asignar_materia_profesor",$data);

 } 
 public function materiacurso()
 {       
   $this->load->model($this->_msubarea);        
   $this->load->model($this->_mgestion);   
   $this->load->model($this->_mnivel);
   $mgestion=new mgestion();                 
   $msubarea=new msubarea();       
   $mnivel=new mnivel();
   $data['subarea_list']=$msubarea->_list();       
   $data['gestion_list']=$mgestion->_list();
   $data['nivel_list']=$mnivel->_list();
   $this->load->view('plancurricular/asignar_materia_curso',$data);      
 }   
 public function asesorcurso()
 {                 
  $now=date("Y-m-d H:i:s"); 
  $this->load->model($this->_musuario);        
  $this->load->model($this->_mgestion);   
  $this->load->model($this->_mnivel);
  $this->load->model($this->modelasesorcurso);
  $mgestion=new mgestion();              
  $musuario=new musuario();        
  $mnivel=new mnivel();
  $mcursousuario=new MUsuarioCurso();
  $usr=$this->session->userdata('usr');
  $listProfesores=$musuario->findByCodigoCargo($this->codigo_cargo_profesor);
  $listAsesores=$musuario->findByCodigoCargo($this->codigo_cargo_asesor);
  $data['usuario_list']=array();
  foreach ($listProfesores as $value) {
    array_push($data['usuario_list'], $value);
  }
   foreach ($listAsesores as $value) {
    array_push($data['usuario_list'], $value);
  }
  $data['gestion_list']=$mgestion->_list();
  $data['nivel_list']=$mnivel->_list();
  $this->form_validation->set_rules($this->validation->config[$this->modelasesorcurso]);
  if ($this->form_validation->run() == FALSE) {
    $this->load->view('plancurricular/asignar_asesor_curso',$data);      
  }
  else{
    $usuarioCurso=$this->input->post("usuario_curso");
    $usuarioCurso["fecha_actualizacion"]=$now;
    $usuarioCurso["fecha_registro"]=$now;
    $usuarioCurso["usuario_registro"]=$usr->id_usuario;
    $usuarioCurso["usuario_actualizacion"]=$usr->id_usuario;    
    $mcursousuario->save($usuarioCurso,null);        
    success();
    redirect('plancurricular/asesorcurso','refresh');
  }
}
public function configurarcriteriosmateria(){
  $this->load->model($this->_mgestion);   
  $this->load->model($this->_musuario);
  $this->load->model($this->_mdimension);  
  $this->load->model($this->_mcriterio);    
  $this->load->model($this->_usuariosubarea);
  $this->load->model($this->_mplanglobal);
  $this->load->model($this->_mbimestre);  
  $this->load->model($this->modelasesorcurso);
  $profesor=$this->input->post("usuario");
  $gestion=$this->input->post("gestion");
  $subarea=$this->input->post("subarea");
  $bimestre=$this->input->post("bimestre");  
  $btnSend=$this->input->post("send");
  $criterios_list=$this->input->post("to");
  $usrcurso=$this->input->post("usuario_curso");
  $mgestion=new mgestion();              
  $musuario=new musuario(); 
  $mdimension=new mdimension();
  $mcriterio=new mcriterio();
  $musuariosubarea=new musuariosubarea();
  $mplanglobal=new mplanglobal();
  $mbimestre=new mbimestre();
  $musuariocurso=new musuariocurso();
  $usr=$this->session->userdata('usr');
  $data['bimestre_list']=$mbimestre->_list();
  $data['gestion_list']=$mgestion->_list();
  $data['usuario_list']=array();//$musuario->findByCodigoCargo($this->codigo_cargo);
  $data['dimension_list']=$mdimension->_list();
  $data['criterio_list']=$mcriterio->_list();  
  $data['usuario_session']=$usr;
  $data['usuario_curso']=(object)$usrcurso; 
  array_push($data['usuario_list'], $usr);
  $data['usuario']=$usr; 
  $data['subarea']=$subarea;
  $curricular_year=curricular_year(); 
  $gestion['id_gestion']=$curricular_year->id_gestion;
  /*if(isset($data['usuario']) && $data['usuario']->id_usuario!='' && isset($gestion) && $gestion['id_gestion']!='')
  { */   
    $data['subarea_list']=$musuariosubarea->findbyIdUsuario($usr->id_usuario,$gestion["id_gestion"],1);    
    $data['usuario_curso_list']=$musuariocurso->encontrarPorIdUsuarioIdgestion($usr->id_usuario,$gestion["id_gestion"]);       
  /*}  */
  if(isset($subarea) && $subarea['id_subarea']!='' && isset($profesor) && $profesor['id_usuario']!='' && isset($gestion) && $gestion['id_gestion']!='' && $bimestre['codigo']!='' && isset($usrcurso) && $usrcurso["id_usuario_curso"]!="")
  {
    $data['planglobal_list']=$mplanglobal->planCalificacionFix($subarea["id_subarea"],$profesor['id_usuario'],$bimestre["codigo"],$usrcurso["id_usuario_curso"]);    
  }  
  $data["planglobal_list_selected"]=array();
  if(isset($data['subarea_list']) && isset($data['planglobal_list']))
  {
    foreach ($data['planglobal_list'] as $plan) {        
      foreach ($data['criterio_list'] as  $criterio) {
        if($plan->id_dimension==$criterio->id_dimension && $plan->id_criterio==$criterio->id_criterio){
          array_push($data["planglobal_list_selected"], $criterio->id_criterio);
        }
      }
    }
  }  
  if(isset($btnSend) && $btnSend!='')  
  {
    $this->form_validation->set_rules($this->validation->config[$this->_muplanglobal_rules]);
    if($this->form_validation->run() == FALSE) {     
      $this->load->view('plancurricular/configurar_libro_pedagogico',$data);
    } else {
      $idUsuarioSubarea=NULL;
      foreach ($data['subarea_list'] as $subarea_item) {
        if($subarea['id_subarea']==$subarea_item->id_subarea){
         $idUsuarioSubarea= $subarea_item->id_usuario_subarea;         
         break;
       }
     }
     if(isset($idUsuarioSubarea) && $idUsuarioSubarea!=NULL)
     {
      $count=$mplanglobal->borrarPlanCalificacion($idUsuarioSubarea,$gestion["id_gestion"],$bimestre["codigo"],$usrcurso["id_usuario_curso"]);            
      foreach ($criterios_list as $dominios) {
        foreach ($dominios as $criterio_id) {                        
          $planglobal_item=array('id_criterio' => $criterio_id,
            'id_usuario_subarea'=>$idUsuarioSubarea,
            'bimestre'=>$bimestre["codigo"],
            'id_gestion'=>$gestion["id_gestion"],
            'id_usuario_curso'=>$usrcurso["id_usuario_curso"]);
          $mplanglobal->save($planglobal_item,NULL);
        }                    
      }            
    }    
    
    success();
    $this->load->view('plancurricular/configurar_libro_pedagogico',$data);
  }
}
else{
  $this->load->view('plancurricular/configurar_libro_pedagogico',$data);    
}
}
public function createcriterios(){
  $this->load->model($this->_usuariosubarea);  

}
public function obtenerCursoProfesor(){
  $this->load->model($this->modelasesorcurso);
  $modelasesorcurso=new musuariocurso();
  $data=$this->input->post("usuario_curso");
  $id_gestion=$data["id_gestion"];
  $id_curso=$data["id_curso"];
  $id_usuario=$data["id_usuario"];
  $result=null;  
  $result=$modelasesorcurso->encontrarPorIdUsuario($id_usuario,$id_gestion);
  echo  json_encode($result);
}
public function validarAsignacionCursoProfesor(){
  $this->load->model($this->modelasesorcurso);
  $modelasesorcurso=new musuariocurso();
  $data=$this->input->post("usuario_curso");
  $id_gestion=$data["id_gestion"];
  $id_curso=$data["id_curso"];
  $id_usuario=$data["id_usuario"];
  $result=null;  
  if(isset($id_usuario) && isset($id_gestion) && $id_usuario!='' && $id_gestion!='')
    $result=$modelasesorcurso->encontrarPorIdCursoIdgestion($id_usuario,$id_curso,$id_gestion);
  echo  json_encode($result);
}
}

/* End of file UnidadEducativa.php */
/* Location: ./application/controllers/UnidadEducativa.php */