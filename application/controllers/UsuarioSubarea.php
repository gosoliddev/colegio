<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuarioSubarea extends MY_Controller {
	var $model='musuariosubarea';
	public function index()
	{
		$this->load->view("cursosubarea/index");
	}	
	public function findbyIdUsuario(){
		$this->load->model('musuariosubarea');
		$musuariosubarea=new musuariosubarea();
		$idUsuario=$this->input->post('id_usuario');
		$idGestion=$this->input->post('id_gestion');
		$data['subareausuario_list']=$musuariosubarea->findbyIdUsuario($idUsuario,$idGestion,'');
		echo json_encode($data);
	}	
	public function setSubareaUsuario(){
		$now=date("Y-m-d H:i:s");
		$usr=$this->session->userdata('usr');
		$this->load->model('musuariosubarea');		
		$musuariosubarea=new musuariosubarea();				
		$data=$this->input->post('subarea_usuario');			
		if(!is_null($data) && sizeof($data)>0){	
			/*$id_usuario=$data[0]['id_usuario'];
			$id_gestion=$data[0]['id_gestion'];
			$count=$musuariosubarea->deletePorIdGestionIdUsuario($id_gestion,$id_usuario);*/
			/*if(isset($count) && $count>0){*/
				foreach ($data as $item) {
					$id1=$item['id_usuario_subarea'];						
					$item['usuario_registro']=$usr->id_usuario;
					$item['usuario_actualizacion']=$usr->id_usuario;
					$item['fecha_actualizacion']=$now;
					$item['fecha_registro']=$now;				
					if($id1==""){
						unset($item['id_usuario_subarea']);
						unset($item['usuario_registro']);
						unset($item['fecha_registro']);
					}		
					else if(is_null($item['id_usuario_subarea']) || $item['id_usuario_subarea']==''){
						unset($item['id_usuario_subarea']);
					}		
					$musuariosubarea->save($item,$id1);
				}
				$object = (object)['status' => true, 'message'=>lang('success_message')];
			/*}
			else{
				$object = (object)['status' => false, 'message'=>lang('error_message')];
			}*/
		}
		else
			$object = (object)['status' => false, 'message'=>lang('error_message')];

		echo json_encode($object);				
	}

}

/* End of file CursoSubarea.php */
/* Location: ./application/controllers/CursoSubarea.php */