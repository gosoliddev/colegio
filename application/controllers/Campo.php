<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campo extends MY_Controller {
	var $model='mcampo';
	public function index(){		 				
		$this->load->view("campo/index");	 
	}
	public function load(){		
		$this->load->model($this->model);
        $data = $this->input->post();
        $pageSize = $this->input->post("rowCount");
        $offset = $this->input->post("current");
        $sort = array();
        $search=null;
        unset($data["rowCount"]);
        unset($data["current"]);
        if (array_key_exists('sort', $data)) {
            $sort = $data['sort'];
            unset($data["sort"]);
        }
        if (isset($data['searchPhrase'])) {
            $search = $data['searchPhrase'];
            unset($data["searchPhrase"]);
        }
        $mcampo = new Mcampo();
        $result = $mcampo->load($search, $pageSize, $offset, $sort);
        if (sizeof($result) > 0) {
            $object = (object)['rows' => $result, 'total' => $result[0]->total, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
        } else {
            $object = (object)['rows' => $result, 'total' => 0, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
        }
        echo json_encode($object);
	}
	public function change(){		 		
		$now=date("Y-m-d H:i:s");	
		$usr=$this->session->userdata('usr');	
		$this->load->model($this->model);		
		$campo=new mcampo();
		$id=$this->uri->segment(3);
		$data['campo']=NULL;
		if(isset($id)){
			$data['campo']=$campo->get($id);				
		}
		$this->form_validation->set_rules($this->validation->config[$this->model]);	
		if ($this->form_validation->run() == FALSE) {			 
			$this->load->view("campo/change",$data);			 
		} else {	
			$id=$this->input->post("id");
			$row=$this->input->post("campo");
			if(!isset($row['activo']))
				$row['activo']=0;
			else
				$row['activo']=1;
			if(isset($id) && $id!='')
			 {
                $row["usuario_actualizacion"]=$usr->id_usuario;                        
                $row["fecha_actualizacion"]=$now;	                
                }
            else
            {
            	$row["fecha_registro"]=$now;
            	$row["fecha_actualizacion"]=$now;	
                $row["usuario_registro"]=$usr->id_usuario;	
            }
			$campo->save($row,$id);				
			redirect('/campo/index');
		}		
	}
	public function delete(){	
		$this->load->model($this->model);
		$mcampo=new mcampo();
		$id=$this->uri->segment(3);
		$mcampo->delete($id);
		redirect('/campo/index');
	}
	

}

/* End of file Campo.php */
/* Location: ./application/controllers/Campo.php */