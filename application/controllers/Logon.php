<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class logon extends MY_Controller {

	public function index()
	{    
		$this->load->view('login');
	}	 
  public function main()
  {    
     redirect('main','refresh');
  } 
  public function logout(){
    $this->session->unset_userdata('usr');
    redirect('logon','refresh');
  }
}
