<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calificacion extends MY_Controller {
	var $_mestudiante='mestudiante';
	var $_mgestion="mgestion";
	var $_mkardex='mkardex';
	var $_mdimension='mdimension';
	var $_mcriterio='mcriterio';
	var $_mkardexdetalle='mkardexdetalle';	
	var $_musuariosubarea='musuarioSubArea';
	var $_musuariocurso='musuariocurso';
	var $_mcursosubarea='mcursosubarea';
	var $_mkardexcriterio='mkardexcriterio';
	var $_mplanglobal='mplanglobal';
	var $_mnotas='mnotas';
	var $_mcampo='mcampo';
	var $_mvaloracion='mvaloracion';
	var $_mcurso='mcurso';	
	var $_mbimestre='mbimestre';	
	var $_msubarea='msubarea';	
	var $_mkardexbimestreobservacion="mkardexbimestreobservacion";
	public function index(){
		$date=date('Y-m-d h:m');
		$this->load->model($this->_musuariosubarea);
		$this->load->model($this->_mgestion);
		$this->load->model($this->_musuariocurso);	
		$this->load->model($this->_mcursosubarea);		
		$this->load->model($this->_mkardex);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mplanglobal);	
		$this->load->model($this->_mdimension);	
		$this->load->model($this->_mbimestre);	
		$data=array();
		$mgestion=new mgestion();
		$musuariocurso=new musuariocurso();	
		$mcursosubarea=new mcursosubarea();
		$mkardex=new mkardex();
		$mplanglobal=new mplanglobal();
		$mdimension=new mdimension();
		$mkardexdetalle=new mkardexdetalle();
		$mbimestre=new mbimestre();
		$usr=$this->session->userdata('usr');	
		$curso=null;					
		$usuario_curso=$this->input->post("usuario_curso");
		if(isset($usuario_curso) && $usuario_curso['id_usuario_curso']!='')
		{
			$data['usuario_curso']=$usuario_curso;
			$curso=$musuariocurso->findById($usuario_curso['id_usuario_curso']);
		}
		$subarea=$this->input->post("subarea");
		$bimestre=$this->input->post("bimestre");
		$dimension_list=array();
		$gestion=$mgestion->findByCurrent($date);
		$data["usuario_curso"]=(object)$this->input->post("usuario_curso");
		if($gestion!=null){
			$data["bimestre"]=$bimestre;
			$data["bimestre_list"]=$mbimestre->findByGestion($gestion->id_gestion);	
			$data['usuairo_curso_list']=$musuariocurso->encontrarPorIdUsuarioIdgestion($usr->id_usuario,$gestion->id_gestion);
			$data["dimension_list"]=array();
			$data["kardex_list"]=array();
			if(isset($usuario_curso) && $usuario_curso['id_usuario_curso']!=''){						
				$data['subarea_list']=$mcursosubarea->obtenerSubAreasUsuarioFix($usr->id_usuario,$gestion->id_gestion,$curso->id_curso);			
			}
			if(isset($subarea) && $subarea['id_subarea']!=''){
				$idSubarea=$subarea['id_subarea'];
				$data['subarea']=$subarea;						
				$data["kardex_list"]=$mkardex->encontrarEstudiantes($idSubarea,$curso->id_curso);						
			}

			$i=0;			
			if(isset($usuario_curso) && isset($subarea) && isset($bimestre) && $usuario_curso['id_usuario_curso']!='' && $subarea['id_subarea']!='' &&  $bimestre['codigo']!="" ){			
				$dimension_list=$mplanglobal->planCalificacionDominio($idSubarea,$usr->id_usuario,$bimestre['codigo']);			
				foreach ($dimension_list as $value) {							
					$value["criterio_list"]=$mplanglobal->planCalificacionCriterioFix($usuario_curso["id_usuario_curso"],$idSubarea,$usr->id_usuario,$value["codigo"],$bimestre['codigo']);												
					array_push($value["criterio_list"],array("codigo"=>$value["codigo"],"codigo_criterio"=>"AUTOEVALUACION" ,"descripcion"=>"AUTOEVALUACION","nota"=>0));
					array_push($value["criterio_list"], array("codigo"=>$value["codigo"],"codigo_criterio"=>"PROMEDIO" ,"descripcion"=>"PROMEDIO","nota"=>0));					
					array_push($data["dimension_list"], $value);		
					$i++;	
				}

				foreach ($data["kardex_list"] as $key => $value1) {
					$value1->dimension_list=array();
					foreach ($data["dimension_list"] as $key => $value2) {
						$d=array();
						foreach ($value2["criterio_list"] as $value3){																						
							if($value3["codigo_criterio"]==="AUTOEVALUACION")
							{								
								$nota=$mkardexdetalle->obtenerPromedioAutoEvalucaionByRude($value1->rude,$idSubarea,$value2["codigo"],$bimestre['codigo']);	
								if($nota)
									$value3["nota"]=floor($nota->autoevaluacion);
								else
									$value3["nota"]=0;

								array_push($d, $value3);
							}
							else if($value3["codigo_criterio"]==="PROMEDIO"){
								$nota=$mkardexdetalle->obtenerPromedioAutoEvalucaionByRude($value1->rude,$idSubarea,$value2["codigo"],$bimestre['codigo']);	
								if($nota)
									$value3["nota"]=floor($nota->promedio);
								else
									$value3["nota"]=0;

								array_push($d, $value3);
							}
							else{
								$nota=$mkardexdetalle->obtenerNotasByRude($value1->rude,$idSubarea,$value2["codigo"],$value3["codigo_criterio"],$bimestre['codigo']);	
								if($nota)
									$value3["nota"]=floor($nota->nota);
								else
									$value3["nota"]=0;
								array_push($d, $value3);	
							}
						}
						$value2["criterio_list"]=$d;
						array_push($value1->dimension_list, $value2);			
					}						
				}

			}				
		}				
		$this->load->view('Calificacion/index',$data);	
	}	 
	public function create(){		
		$date=date('Y-m-d h:m');
		$this->load->model($this->_mdimension);
		$this->load->model($this->_mcriterio);
		$this->load->model($this->_mestudiante);
		$this->load->model($this->_mkardex);
		$this->load->model($this->_mkardexcriterio);
		$this->load->model($this->_mnotas);
		$this->load->model($this->_mgestion);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mkardexbimestreobservacion);
		$this->load->model($this->_musuariocurso);
		$this->load->model($this->_msubarea);
		$this->load->model($this->_mcurso);	
		$mgestion=new mgestion();
		$mdimension=new mdimension();
		$mcriterio=new mcriterio();
		$mestudiante=new mestudiante();
		$mkardex=new mkardex();		
		$mkardexcriterio=new mkardexcriterio();
		$mnotas=new mnotas();
		$mkardexdetalle=new mkardexdetalle();
		$mkardexbimestreobservacion=new mkardexbimestreobservacion();
		$musuariocurso=new musuariocurso();	
		$msubarea=new msubarea();
		$mcurso=new mcurso();
		$usr=$this->session->userdata('usr');
		$gestion=$mgestion->findByCurrent($date);
		$idSubarea=$this->input->post("id_subarea");
		$id_usuario_curso=$this->input->post("id_usuario_curso");			
		$curso=null;
		$data=$this->input->post();
		if(isset($id_usuario_curso) && $id_usuario_curso!='')
		{
			$data['id_usuario_curso']=$id_usuario_curso;
			$curso=$musuariocurso->findById($id_usuario_curso);
		}
		//$idCurso=$this->input->post("id_curso");
		$bimestre=$this->input->post("codigo");//bimestre a calificar desde la vista 	
		$observacion=$this->input->post("cualitativo") ;//obtiene las observaciones por estudiante de libro pedagogico registrado	
		$list=$this->input->post();					
		$kardeCriterios=array();
		$id_kardex_detalle=NULL;			
		foreach ($list as $key =>$dimension) {					
			if(is_string($key)){
				$tmpDimension=$mdimension->findByCodigo($key);						
				if(is_array($dimension)){								
					foreach ($dimension as $key2 => $row) {		
						if(gettype($row)!="string"){					
						$tmpEstudiante=$mestudiante->findByRude($key2.'');	//obtener estudiante por rude						 		
						$tmpKardex=$mkardexdetalle->obtnerByIdCursoIdGestion($tmpEstudiante->rude,$gestion->id_gestion,$curso->id_curso);//kardex detalle actual
						$id_kardex_detalle=$tmpKardex->id_kardex_detalle;
						$promedio=$row["PROMEDIO"];									
						$autoevaluacion=$row["AUTOEVALUACION"];//obtener la autoevaluacion informada en la lista
						unset($row['PROMEDIO']);//Borrar el promedio para realizar la operacion final		
						$sizeNotas=count($row);//obtener cantidad sumada para el promedio
						unset($row['AUTOEVALUACION']);//Borrar el promedio para realizar la operacion final		
						$kardexCriterio=array(
							'id_kardex_detalle'=>$tmpKardex->id_kardex_detalle,
							'bimestre'=>$bimestre,
							'id_dimension'=>$tmpDimension->id_dimension,
							'autoevaluacion'=>$autoevaluacion,
							'promedio'=>0,
							"usuario_registro"=>$usr->id_usuario,
							"usuario_actualizacion"=>$usr->id_usuario,
							"fecha_registro"=>$date,
							"fecha_actualizacion"=>$date,
							"id_subarea"=>$idSubarea,
							"notas"=>array());			
						$notas=array();				
						foreach ($row as $key3 => $value) {
							$tmpCriterio=$mcriterio->findByCodigo($key3);						
							$nota=array(
								"id_kardex_criterio"=>null,
								"id_criterio"=>$tmpCriterio->id_criterio,
								"nota"=>$value,
								"fecha_registro"=>$date,
								"fecha_actualizacion"=>$date,
								"usuario_registro"=>$usr->id_usuario,
								"usuario_actualizacion"=>$usr->id_usuario);
							array_push($notas, $nota);													
						}						
						$kardexCriterio['notas']=$notas;
						$kardexCriterio['promedio']=$promedio;	
						array_push($kardeCriterios, $kardexCriterio);
					}						
				}
			}
		}			
	}

	foreach ($kardeCriterios as $value1) {
		$tmpnotas=$value1["notas"];
		unset($value1["notas"]);
		$idkardexcriterio=$mkardexcriterio->save($value1,null);
		foreach ($tmpnotas as $nota) {
			$nota['id_kardex_criterio']=$idkardexcriterio;
			$mnotas->save($nota,null);							
		}
	}	
	$tmpcurso=NULL;
	$reprobados=array();
	$promedios=$this->input->post('nota_bimestral');		
	foreach ($promedios as $key => $value) {
		$tmpEstudiante=$mestudiante->findByRude($key.'');		
		if($value<51) 			
		{			
    		//rango e aprobacion     		
			array_push($reprobados, $tmpEstudiante);
		}
	}    
	$asesor=obtener_usuario_asesor($curso->id_curso,$gestion->id_gestion);
	if(isset($asesor) && $asesor->email!=''){
		$subarea=$msubarea->get($idSubarea);
		if(isset($curso) && $curso->id_curso!=''){
			$tmpcurso=$mcurso->get($curso->id_curso);
		}
		$from_email = "ligiavillarroel1012@gmail.com"; 
		$to_email = $asesor->email; 

		$text_subject="Lista de reprobados del curso ".$tmpcurso->descripcion.' (materia:'.$subarea->descripcion.')';
		$text_message="Los alumnos reporbados son:<br />";		
		foreach ($reprobados as $value) {
			$text_message=$text_message.'Alumno:'.$value->apellido_paterno.' '.$value->apellido_materno.' '.$value->nombres.'&nbsp;&nbsp;rude:'.$value->rude.'<br />';			
		}		
         //Load email library 
		$this->load->library('email'); 

		$config['protocol']    = 'smtp';
		$config['smtp_host']    = 'ssl://smtp.gmail.com';
		$config['smtp_port']    = '465';
		$config['smtp_timeout'] = '7';
		$config['smtp_user']    = 'ligiavillarroel1012@gmail.com';
		$config['smtp_pass']    = 'Diciembre1984';
		$config['charset']    = 'utf-8';
		$config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);
        $this->email->from($from_email, 'Colegios.com'); 
        $this->email->to($to_email);
        $this->email->subject($text_subject); 
        $this->email->message($text_message); 

         //Send mail         
        if($this->email->send()){        	
        	success(lang("sent_asesor_notification"));
        }
        else {
        	error(lang("error_sent_asesor_notification"));
        }
    }
    success();
    redirect('calificacion','refresh');	
}
public function boletin(){
	$this->load->model($this->_mcampo);
	$this->load->model($this->_mgestion);
	$this->load->model($this->_mkardex);
	$this->load->model($this->_mestudiante);
	$this->load->model($this->_mcurso);
	$this->load->helper('calificacion_helper');
	$texto=$this->input->post("texto");
	$gestion=$this->input->post("gestion");	
	$curso=$this->input->post("curso");	
	$mcampo=new mcampo();
	$mgestion=new mgestion();
	$mkardex=new mkardex();
	$mestudiante=new mestudiante();
	$mcurso=new mcurso();
	$data["campo_list"]=$mcampo->_list();
	$data['gestion_list']=$mgestion->_list();
	$data["estudiante"]=0;
	$data['curso_list']=array();
	$data['curso']=(object)$this->input->post('curso');
	$data['gestion']=(object)$this->input->post('gestion');		
	if(isset($texto) && $texto!=''){
		$estudiante=$mkardex->searchByRude($texto,1,1,null);	
		if(isset($estudiante) && sizeof($estudiante)>0){
			$data["estudiante"]=$estudiante[0]->id_estudiante;	
			$data["estudiante_object"]=$mestudiante->encontrarPorId($estudiante[0]->id_estudiante);
		}				
		if(isset($curso) && $curso['id_curso']!=''){
			$tmp=$data['curso'];
			$curso=$mcurso->get($tmp->id_curso);
			$data['curso_object']=$curso;
		}		
		if(isset($gestion) && $gestion['id_gestion']!=''){
			$data["gestion_object"]=$mgestion->get($gestion['id_gestion']);
		}
		if(isset($gestion) && $gestion['id_gestion']!='' && isset($estudiante) && sizeof($estudiante)){		
			$data['curso_list']=$this->obtenerCursosEstudiante($estudiante[0]->rude,$gestion['id_gestion']);
		}
	}
	$this->load->view('calificacion/boletin',$data);
}
//Obtiene valoreaciones cualitativas
public function obtenerCursosEstudiante($rude,$id_gestion){
	$this->load->model($this->_mkardexdetalle);		
	$mkardexdetalle=new mkardexdetalle();	
	$curso_list=$mkardexdetalle->obtnerCursosByIdGestion($rude,$id_gestion);
	return $curso_list;
}
//Obtiene valoreaciones cualitativas
public function obtenerValoraciones(){
	$this->load->model($this->_mvaloracion);
	$this->load->model($this->_mcurso);
	$mvaloracion=new mvaloracion();
	$mcurso=new mcurso();
	$curso=$this->input->post("curso");
	$promedio=$this->input->post("promedio");
	$activo=1;
	$curso_tmp=$mcurso->get($curso["id_curso"]);
	$data["valoracion_list"]=$mvaloracion->obtenerObservaciones($curso_tmp->id_nivel,$activo,$promedio);
	echo json_encode($data);
}
}

/* End of file Calificacion.php */
/* Location: ./application/controllers/Calificacion.php */