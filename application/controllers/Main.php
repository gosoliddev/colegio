<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {
 
	public function index(){		
		$this->load->view('index');
	}	
}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */