<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CursoSubarea extends MY_Controller {
	var $model='mcursosubarea';
	public function index()
	{
		$this->load->view("cursosubarea/index");
	}
	public function change(){		 		
		$now=date("Y-m-d H:i:s");
		$this->load->model($this->model);		
		$this->form_validation->set_rules($this->validation->config[$this->model]);	
		if ($this->form_validation->run() == FALSE) {
			$this->load->view("cursosubarea/change");
		} else {			
			$this->load->view("cursosubarea/change");
		}		
	}
	
	
	public function findByIdCurso(){
		$this->load->model('mcursosubarea');
		$mcursosubarea=new mcursosubarea();
		$data=array();
		$id_curso=$this->input->post("id_curso");
		$id_gestion=$this->input->post("id_gestion");	
		if($id_curso!='' && $id_gestion!=''){
			$data['subareacurso_list']=$mcursosubarea->findByIdCurso($this->input->post("id_curso"),$this->input->post("id_gestion"));
		}
		echo  json_encode($data);
	}
	public function cursosPorIdSubareaIdUsuaro(){
		$this->load->model($this->model);
		$mcursosubarea=new mcursosubarea();
		$usr=$this->session->userdata('usr');
		$idSubarea=$this->input->post("id_subarea");		
		$data["curso_list"]=$mcursosubarea->encontrarPorIdSubareaIdUsuario($idSubarea,$usr->id_usuario);
		echo json_encode($data);
	}
	public function setSubareaCurso(){
		$now=date("Y-m-d H:i:s");
		$usr=$this->session->userdata('usr');
		$this->load->model('mcursosubarea');		
		$mcursosubarea=new mcursosubarea();				
		$data=$this->input->post('subarea_curso');
		$id_curso=$this->input->post('id_curso');
		$id_gestion=$this->input->post('id_gestion');
		if(!is_null($data) && sizeof($data)>0){	
			foreach ($data as $item) {
				$id=$item['id_curso_subarea'];				
				if(isset($id)){
					$item['usuario_registro']=$usr->id_usuario;
					$item['usuario_actualizacion']=$usr->id_usuario;
					$item['fecha_actualizacion']=$now;
					$item['fecha_registro']=$now;
				}
				else{					
					$item['usuario_actualizacion']=$usr->id_usuario;
					$item['fecha_actualizacion']=$now;					
				}
				$item['id_curso']=$id_curso;
				$item['id_gestion']=$id_gestion;				
				if($id!=""){
					unset($item['id_curso_subarea']);
					unset($item['usuario_registro']);
					unset($item['fecha_registro']);
				}		
				else if(is_null($item['id_curso_subarea']) || $item['id_curso_subarea']==''){
					unset($item['id_curso_subarea']);
				}		
				$mcursosubarea->save($item,$id);
			}
			success();
			$object = (object)['status' => true, 'message'=>lang('success_message')];
		}
		else
		{
			error();
			$object = (object)['status' => false, 'message'=>lang('error_message')];
		}

		echo json_encode($object);				
	}

}

/* End of file CursoSubarea.php */
/* Location: ./application/controllers/CursoSubarea.php */