<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gestion extends MY_Controller {
	var $model='mgestion';
	public function index()
	{
		$this->load->view("gestion/index");
	}
	 
	public function load(){		
		$this->load->model($this->model);
        $data = $this->input->post();
        $pageSize = $this->input->post("rowCount");
        $offset = $this->input->post("current");
        $sort = array();
        $search=null;
        unset($data["rowCount"]);
        unset($data["current"]);
        if (array_key_exists('sort', $data)) {
            $sort = $data['sort'];
            unset($data["sort"]);
        }
        if (isset($data['searchPhrase'])) {
            $search = $data['searchPhrase'];
            unset($data["searchPhrase"]);
        }
        $mgestion = new Mgestion();
        $result = $mgestion->load($search, $pageSize, $offset, $sort);
        if (sizeof($result) > 0) {
            $object = (object)['rows' => $result, 'total' => $result[0]->total, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
        } else {
            $object = (object)['rows' => $result, 'total' => 0, 'current' => (int)$this->input->post("current"), 'rowCount' => (int)$this->input->post("rowCount")];
        }
        echo json_encode($object);
	}
	public function change(){		 		
		$now=date("Y-m-d H:i:s");		
		$usr=$this->session->userdata('usr');		
		$this->load->model($this->model);
		$this->load->model('mgestion');		
		$this->load->model('munidadeducativa');		
		$unidadeducativa=new munidadeducativa();
		$gestion=new mgestion();		
		$id=$this->uri->segment(3);		
		$data['gestion']=NULL;
		$data['unidadeducativa_list']=$unidadeducativa->_list();
		if(isset($id)){
			$data['gestion']=$gestion->get($id);				
		}
		$this->form_validation->set_rules($this->validation->config[$this->model]);	
		if ($this->form_validation->run() == FALSE) {			 
			$this->load->view("gestion/change",$data);			 
		} else {	
			$id=$this->input->post("id");
			$row=$this->input->post("gestion");			
			if(!isset($row['activo']))
				$row['activo']=0;
			else
				$row['activo']=1;
			if(isset($id) && $id!='')
             {
                $row["usuario_actualizacion"]=$usr->id_usuario;                        
                $row["fecha_actualizacion"]=$now;                   
                }
            else
            {
                $row["fecha_registro"]=$now;
                $row["fecha_actualizacion"]=$now;   
                $row["usuario_registro"]=$usr->id_usuario;  
            }		
			$gestion->save($row,$id);
			redirect('/gestion/index');
		}		
	}
	public function delete(){	
		$this->load->model($this->model);
		$mgestion=new mgestion();
		$id=$this->uri->segment(3);
		$mgestion->delete($id);
		redirect('/gestion/index');
	}
	public function validagestion(){
		$this->load->model($this->model);
		$data=$this->input->post('mgestion');		
		$mgestion=new mgestion();
		$response=null;
		$id=$this->uri->segment(3);		
		$response=$mgestion->validaGestion($data["inicio_gestion"],$data["fin_gestion"]);					
		echo $response;			
	}

}

/* End of file Gestion.php */
/* Location: ./application/controllers/Gestion.php */