<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends MY_Controller {
	var $_mestudiante='mestudiante';
	var $_mgestion="mgestion";
	var $_msubarea="msubarea";
	var $_musuario="musuario";
	var $_mnivel="mnivel";
	var $_mcurso="mcurso";
	var $_mkardexdetalle="mkardexdetalle";
	var $_mbimestre='mbimestre';
	var $_mcurosubarea="mcursosubarea";
	var $_musuariocurso="musuariocurso";
	var $_musuariosubarea="musuariosubarea";
	var $model1="rptaprobados";
	var $model2="rptmaterias";
	var $model3="rptaprobadosprofesor";
	var $model4="rptmateriasprofesor";

	var $model5="rptaprobadosasesor";
	var $model6="rptmateriasasesor";
	
	public function aprobados()
	{
		$this->load->model($this->_msubarea);        
		$this->load->model($this->_mgestion);   
		$this->load->model($this->_mnivel);
		$this->load->model($this->_mcurso);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mbimestre);
		$this->load->model($this->_mcurosubarea);		
		$mbimestre=new mbimestre();
		$mgestion=new mgestion();                 
		$msubarea=new msubarea();       
		$mnivel=new mnivel();
		$mcurso=new mcurso();
		$mcursosubarea=new mcursosubarea();
		$mkardexdetalle=new mkardexdetalle();
		$data['subarea_list']=$msubarea->_list();       
		$data['gestion_list']=$mgestion->_list();
		$data['nivel_list']=$mnivel->_list();
		$data['bimestre_list']=$mbimestre->_list();
		$nivel=$this->input->post("nivel");
		$gestion=$this->input->post("gestion");
		$curso=$this->input->post("curso");	
		$subarea=$this->input->post("subarea");				
		if(isset($nivel) && $nivel['id_nivel']!=""){
			$data['curso_list']=$mcurso->findByIdNivel($nivel["id_nivel"]);			
		}
		if(isset($curso) && $curso['id_curso']!=""){
			$data['subarea_list']=$mcursosubarea->findByIdCurso($curso["id_curso"],$gestion['id_gestion']);			
		}
		$this->form_validation->set_rules($this->validation->config[$this->model1]);
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('reportes/rpt-aprobados', $data);
		} else {
			$estudiante_list=array();
			$list=$mkardexdetalle->obtenerEstudiantesPorIdCursoIdGestion($gestion['id_gestion'],$curso['id_curso']);			
			foreach($list as $estudiante){
				$nota=obtener_aprobado_rpt1($curso['id_curso'],$subarea['id_subarea'],$gestion['id_gestion'],$estudiante->id_estudiante);				
				if($nota['promedio']>51){
					array_push($estudiante_list, $estudiante);
				}
			}
			$data["estudiantes_list"]=$estudiante_list;
			$data['curso']=$curso;
			$data['subarea']=$subarea;
			$this->load->view('reportes/rpt-aprobados', $data);
		}		
	}
	public function reprobados()
	{
		$this->load->model($this->_msubarea);        
		$this->load->model($this->_mgestion);   
		$this->load->model($this->_mnivel);
		$this->load->model($this->_mcurso);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mbimestre);
		$this->load->model($this->_mcurosubarea);
		$mbimestre=new mbimestre();
		$mgestion=new mgestion();                 
		$msubarea=new msubarea();       
		$mnivel=new mnivel();
		$mcurso=new mcurso();
		$mcursosubarea=new mcursosubarea();
		$mkardexdetalle=new mkardexdetalle();
		$data['subarea_list']=$msubarea->_list();       
		$data['gestion_list']=$mgestion->_list();
		$data['nivel_list']=$mnivel->_list();
		$data['bimestre_list']=$mbimestre->_list();
		$nivel=$this->input->post("nivel");
		$gestion=$this->input->post("gestion");
		$curso=$this->input->post("curso");	
		$subarea=$this->input->post("subarea");				
		if(isset($nivel) && $nivel['id_nivel']!=""){
			$data['curso_list']=$mcurso->findByIdNivel($nivel["id_nivel"]);			
		}
		if(isset($curso) && $curso['id_curso']!=""){
			$data['subarea_list']=$mcursosubarea->findByIdCurso($curso["id_curso"],$gestion['id_gestion']);			
		}
		$this->form_validation->set_rules($this->validation->config[$this->model1]);
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('reportes/rpt-reprobados', $data);
		} else {
			$estudiante_list=array();
			$list=$mkardexdetalle->obtenerEstudiantesPorIdCursoIdGestion($gestion['id_gestion'],$curso['id_curso']);			
			foreach($list as $estudiante){
				$nota=obtener_aprobado_rpt1($curso['id_curso'],$subarea['id_subarea'],$gestion['id_gestion'],$estudiante->id_estudiante);				
				if($nota['promedio']<51){
					array_push($estudiante_list, $estudiante);
				}
			}
			$data["estudiantes_list"]=$estudiante_list;
			$data['curso']=$curso;
			$data['subarea']=$subarea;
			$this->load->view('reportes/rpt-reprobados', $data);
		}		
	}
	public function materias()
	{
		$this->load->model($this->_msubarea);        
		$this->load->model($this->_mgestion);   
		$this->load->model($this->_mnivel);
		$this->load->model($this->_mcurso);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mbimestre);
		$this->load->model($this->_mcurosubarea);
		$mbimestre=new mbimestre();
		$mgestion=new mgestion();                 
		$msubarea=new msubarea();       
		$mnivel=new mnivel();
		$mcurso=new mcurso();
		$mcursosubarea=new mcursosubarea();
		$mkardexdetalle=new mkardexdetalle();
		$data['subarea_list']=$msubarea->_list();       
		$data['gestion_list']=$mgestion->_list();
		$data['nivel_list']=$mnivel->_list();
		$data['bimestre_list']=$mbimestre->_list();
		$nivel=$this->input->post("nivel");
		$gestion=$this->input->post("gestion");
		$curso=$this->input->post("curso");	
		$subarea=$this->input->post("subarea");
		$bimestre=$this->input->post("bimestre");
		if(isset($nivel) && $nivel['id_nivel']!=""){
			$data['curso_list']=$mcurso->findByIdNivel($nivel["id_nivel"]);			
		}
		if(isset($curso) && $curso['id_curso']!=""){
			$data['subarea_list']=$mcursosubarea->findByIdCurso($curso["id_curso"],$gestion['id_gestion']);			
		}
		$this->form_validation->set_rules($this->validation->config[$this->model2]);
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('reportes/rpt-materias', $data);
		} else {
			$data["estudiantes_list"]=$mkardexdetalle->obtenerEstudiantesPorIdCursoIdGestion($gestion['id_gestion'],$curso['id_curso']);			
			$data['curso']=$curso;
			$data['subarea']=$subarea;
			$data['bimestre']=$bimestre;
			$this->load->view('reportes/rpt-materias', $data);
		}		
	}
	public function verfiicar(){
		$this->load->model($this->_mestudiante);		
		$this->load->library('csvreader');
		$mestudiante=new mestudiante();
		$result =   $this->csvreader->parse_file('./files/test.csv');        
		$data['csvData'] =  array();
		foreach($result as $field){
			$estudiante=$mestudiante->findByRude($field["RUDE"]);
			if(isset($estudiante) && $estudiante->rude!=''){
				$field["estado"]=1;
			}
			else{
				$field["estado"]=0;
			}
			array_push( $data['csvData'], $field);
		}               
		$this->load->view('reportes/rpt-verificar', $data);  
	}
	public function upload()
	{
		$this->load->view('reportes/rpt-import', array('error' => ' ' ));
	}
	public function do_upload()
	{
		$this->load->model($this->_mestudiante);		
		$this->load->library('csvreader');
		$mestudiante=new mestudiante();
		$config['upload_path']= './files/';
		$config['allowed_types']= 'xlsx|xls|csv';
		$config['max_size']= 100;
		$config['max_width']= 1024;
		$config['max_height']= 768;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('reportes/rpt-import', $error);
		}
		else
		{
			
			$data = array('upload_data' => $this->upload->data());	
			$data=$data['upload_data'];			
			$result =   $this->csvreader->parse_file($data['full_path']);        
			$data['csvData'] =  array();
			foreach($result as $field){
				$estudiante=$mestudiante->findByRude($field["RUDE"]);
				if(isset($estudiante) && $estudiante->rude!=''){
					$field["estado"]=1;
				}
				else{
					$field["estado"]=0;
				}
				array_push( $data['csvData'], $field);
			}               
			$this->load->view('reportes/rpt-verificar', $data); 						
		}
	}

	/*methos para reportes de profesores*/
	public function profesor_aprobados()
	{
		$this->load->model($this->_msubarea);        
		$this->load->model($this->_mgestion);   
		$this->load->model($this->_mnivel);
		$this->load->model($this->_mcurso);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mbimestre);
		$this->load->model($this->_mcurosubarea);
		$this->load->model($this->_musuariocurso);	
		$this->load->model($this->_musuariosubarea);			
		$mbimestre=new mbimestre();
		$mgestion=new mgestion();                 
		$msubarea=new msubarea();       
		$mnivel=new mnivel();
		$mcurso=new mcurso();
		$mcursosubarea=new mcursosubarea();
		$mkardexdetalle=new mkardexdetalle();
		$musuariocurso=new musuariocurso();
		$musuariosubarea=new musuariosubarea();
		$curricular_year=curricular_year();
		$usr=$this->session->userdata('usr');
		$data['subarea_list']=$msubarea->_list();       
		$data['gestion_list']=$mgestion->_list();
		$data['nivel_list']=$mnivel->_list();
		$data['bimestre_list']=$mbimestre->_list();
		$nivel=$this->input->post("nivel");
		$gestion=$this->input->post("gestion");
		$curso=$this->input->post("curso");	
		$subarea=$this->input->post("subarea");						
		$data['curso_list']=$musuariocurso->encontrarPorIdUsuarioIdgestion($usr->id_usuario,$curricular_year->id_gestion);				 
		$data['subarea_list']=$musuariosubarea->findbyIdUsuario($usr->id_usuario,$curricular_year->id_gestion,1);					
		$this->form_validation->set_rules($this->validation->config[$this->model3]);
		$data['curso']=$curso;
		$data['subarea']=$subarea;
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('reportes/rpt-aprobados-profesor', $data);
		} else {
			$estudiante_list=array();
			$list=$mkardexdetalle->obtenerEstudiantesPorIdCursoIdGestion($gestion['id_gestion'],$curso['id_curso']);			
			foreach($list as $estudiante){
				$nota=obtener_aprobado_rpt1($curso['id_curso'],$subarea['id_subarea'],$gestion['id_gestion'],$estudiante->id_estudiante);
				if($nota['promedio']>51){
					array_push($estudiante_list, $estudiante);
				}
			}
			$data["estudiantes_list"]=$estudiante_list;
			
			$this->load->view('reportes/rpt-aprobados-profesor', $data);
		}		
	}
	public function profesor_reprobados()
	{
		$this->load->model($this->_msubarea);        
		$this->load->model($this->_mgestion);   
		$this->load->model($this->_mnivel);
		$this->load->model($this->_mcurso);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mbimestre);
		$this->load->model($this->_mcurosubarea);
		$this->load->model($this->_musuariocurso);	
		$this->load->model($this->_musuariosubarea);			
		$mbimestre=new mbimestre();
		$mgestion=new mgestion();                 
		$msubarea=new msubarea();       
		$mnivel=new mnivel();
		$mcurso=new mcurso();
		$mcursosubarea=new mcursosubarea();
		$mkardexdetalle=new mkardexdetalle();
		$musuariocurso=new musuariocurso();
		$musuariosubarea=new musuariosubarea();
		$curricular_year=curricular_year();
		$usr=$this->session->userdata('usr');
		$data['subarea_list']=$msubarea->_list();       
		$data['gestion_list']=$mgestion->_list();
		$data['nivel_list']=$mnivel->_list();
		$data['bimestre_list']=$mbimestre->_list();
		$nivel=$this->input->post("nivel");
		$gestion=$this->input->post("gestion");
		$curso=$this->input->post("curso");	
		$subarea=$this->input->post("subarea");						
		$data['curso_list']=$musuariocurso->encontrarPorIdUsuarioIdgestion($usr->id_usuario,$curricular_year->id_gestion);				 
		$data['subarea_list']=$musuariosubarea->findbyIdUsuario($usr->id_usuario,$curricular_year->id_gestion,1);					
		$this->form_validation->set_rules($this->validation->config[$this->model3]);
		$data['curso']=$curso;
		$data['subarea']=$subarea;
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('reportes/rpt-reprobados-profesor', $data);
		} else {
			$estudiante_list=array();
			$list=$mkardexdetalle->obtenerEstudiantesPorIdCursoIdGestion($gestion['id_gestion'],$curso['id_curso']);			
			foreach($list as $estudiante){
				$nota=obtener_aprobado_rpt1($curso['id_curso'],$subarea['id_subarea'],$gestion['id_gestion'],$estudiante->id_estudiante);
				if($nota['promedio']<51){
					array_push($estudiante_list, $estudiante);
				}
			}
			$data["estudiantes_list"]=$estudiante_list;
			
			$this->load->view('reportes/rpt-reprobados-profesor', $data);
		}		
	}
	public function materias_profesor()
	{
		$this->load->model($this->_msubarea);        
		$this->load->model($this->_mgestion);   
		$this->load->model($this->_mnivel);
		$this->load->model($this->_mcurso);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mbimestre);
		$this->load->model($this->_mcurosubarea);		
		$this->load->model($this->_musuariocurso);	
		$this->load->model($this->_musuariosubarea);
		$mbimestre=new mbimestre();
		$mgestion=new mgestion();                 
		$msubarea=new msubarea();       
		$mnivel=new mnivel();
		$mcurso=new mcurso();
		$mcursosubarea=new mcursosubarea();
		$musuariocurso=new musuariocurso();
		$musuariosubarea=new musuariosubarea();
		$mkardexdetalle=new mkardexdetalle();
		$curricular_year=curricular_year();
		$usr=$this->session->userdata('usr');
		$data['subarea_list']=$msubarea->_list();       
		$data['gestion_list']=$mgestion->_list();
		$data['nivel_list']=$mnivel->_list();
		$data['bimestre_list']=$mbimestre->_list();
		$nivel=$this->input->post("nivel");
		$gestion=$this->input->post("gestion");
		$curso=$this->input->post("curso");	
		$subarea=$this->input->post("subarea");
		$bimestre=$this->input->post("bimestre");
		$data['curso_list']=$musuariocurso->encontrarPorIdUsuarioIdgestion($usr->id_usuario,$curricular_year->id_gestion);				
		$data['curso']=$curso;
		$data['subarea']=$subarea;
		$data['bimestre']=$bimestre; 
		$data['subarea_list']=$musuariosubarea->findbyIdUsuario($usr->id_usuario,$curricular_year->id_gestion,1);
		$this->form_validation->set_rules($this->validation->config[$this->model4]);
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('reportes/rpt-materias-profesor', $data);
		} else {
			$data["estudiantes_list"]=$mkardexdetalle->obtenerEstudiantesPorIdCursoIdGestion($gestion['id_gestion'],$curso['id_curso']);			
			$this->load->view('reportes/rpt-materias-profesor', $data);
		}		
	}
	/*methos para reportes de asesor*/
	public function asesor_aprobados()
	{
		$this->load->model($this->_msubarea);        
		$this->load->model($this->_mgestion);   
		$this->load->model($this->_mnivel);
		$this->load->model($this->_mcurso);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mbimestre);
		$this->load->model($this->_mcurosubarea);
		$this->load->model($this->_musuariocurso);	
		$this->load->model($this->_musuariosubarea);			
		$mbimestre=new mbimestre();
		$mgestion=new mgestion();                 
		$msubarea=new msubarea();       
		$mnivel=new mnivel();
		$mcurso=new mcurso();
		$mcursosubarea=new mcursosubarea();
		$mkardexdetalle=new mkardexdetalle();
		$musuariocurso=new musuariocurso();
		$musuariosubarea=new musuariosubarea();
		$curricular_year=curricular_year();
		$usr=$this->session->userdata('usr');
		$data['subarea_list']=$msubarea->_list();       
		$data['gestion_list']=$mgestion->_list();
		$data['nivel_list']=$mnivel->_list();
		$data['bimestre_list']=$mbimestre->_list();
		$nivel=$this->input->post("nivel");
		$gestion=$this->input->post("gestion");
		$curso=$this->input->post("curso");	
		$subarea=$this->input->post("subarea");						
		$data['curso_list']=$musuariocurso->encontrarPorIdUsuario($usr->id_usuario,$curricular_year->id_gestion);				 
		if(isset($curso) && $curso['id_curso']!=""){
			$data['subarea_list']=$mcursosubarea->findByIdCurso($curso["id_curso"],$gestion['id_gestion']);			
		}			
		$this->form_validation->set_rules($this->validation->config[$this->model5]);
		$data['curso']=$curso;
		$data['subarea']=$subarea;
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('reportes/rpt-aprobados-asesor', $data);
		} else {
			$estudiante_list=array();
			$list=$mkardexdetalle->obtenerEstudiantesPorIdCursoIdGestion($gestion['id_gestion'],$curso['id_curso']);			
			foreach($list as $estudiante){
				$nota=obtener_aprobado_rpt1($curso['id_curso'],$subarea['id_subarea'],$gestion['id_gestion'],$estudiante->id_estudiante);
				if($nota['promedio']>51){
					array_push($estudiante_list, $estudiante);
				}
			}
			$data["estudiantes_list"]=$estudiante_list;			
			$this->load->view('reportes/rpt-aprobados-asesor', $data);
		}		
	}
	public function asesor_reprobados()
	{
		$this->load->model($this->_msubarea);        
		$this->load->model($this->_mgestion);   
		$this->load->model($this->_mnivel);
		$this->load->model($this->_mcurso);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mbimestre);
		$this->load->model($this->_mcurosubarea);
		$this->load->model($this->_musuariocurso);	
		$this->load->model($this->_musuariosubarea);			
		$mbimestre=new mbimestre();
		$mgestion=new mgestion();                 
		$msubarea=new msubarea();       
		$mnivel=new mnivel();
		$mcurso=new mcurso();
		$mcursosubarea=new mcursosubarea();
		$mkardexdetalle=new mkardexdetalle();
		$musuariocurso=new musuariocurso();
		$musuariosubarea=new musuariosubarea();
		$curricular_year=curricular_year();
		$usr=$this->session->userdata('usr');
		$data['subarea_list']=$msubarea->_list();       
		$data['gestion_list']=$mgestion->_list();
		$data['nivel_list']=$mnivel->_list();
		$data['bimestre_list']=$mbimestre->_list();
		$nivel=$this->input->post("nivel");
		$gestion=$this->input->post("gestion");
		$curso=$this->input->post("curso");	
		$subarea=$this->input->post("subarea");						
		$data['curso_list']=$musuariocurso->encontrarPorIdUsuario($usr->id_usuario,$curricular_year->id_gestion);				 
		if(isset($curso) && $curso['id_curso']!=""){
			$data['subarea_list']=$mcursosubarea->findByIdCurso($curso["id_curso"],$gestion['id_gestion']);			
		}
		$this->form_validation->set_rules($this->validation->config[$this->model3]);
		$data['curso']=$curso;
		$data['subarea']=$subarea;
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('reportes/rpt-reprobados-asesor', $data);
		} else {
			$estudiante_list=array();
			$list=$mkardexdetalle->obtenerEstudiantesPorIdCursoIdGestion($gestion['id_gestion'],$curso['id_curso']);			
			foreach($list as $estudiante){
				$nota=obtener_aprobado_rpt1($curso['id_curso'],$subarea['id_subarea'],$gestion['id_gestion'],$estudiante->id_estudiante);
				if($nota['promedio']<51){
					array_push($estudiante_list, $estudiante);
				}
			}
			$data["estudiantes_list"]=$estudiante_list;
			
			$this->load->view('reportes/rpt-reprobados-asesor', $data);
		}		
	}
	public function materias_asesor()
	{
		$this->load->model($this->_msubarea);        
		$this->load->model($this->_mgestion);   
		$this->load->model($this->_mnivel);
		$this->load->model($this->_mcurso);
		$this->load->model($this->_mkardexdetalle);
		$this->load->model($this->_mbimestre);
		$this->load->model($this->_mcurosubarea);		
		$this->load->model($this->_musuariocurso);	
		$this->load->model($this->_musuariosubarea);
		$mbimestre=new mbimestre();
		$mgestion=new mgestion();                 
		$msubarea=new msubarea();       
		$mnivel=new mnivel();
		$mcurso=new mcurso();
		$mcursosubarea=new mcursosubarea();
		$musuariocurso=new musuariocurso();
		$musuariosubarea=new musuariosubarea();
		$mkardexdetalle=new mkardexdetalle();
		$curricular_year=curricular_year();
		$usr=$this->session->userdata('usr');
		$data['subarea_list']=$msubarea->_list();       
		$data['gestion_list']=$mgestion->_list();
		$data['nivel_list']=$mnivel->_list();
		$data['bimestre_list']=$mbimestre->_list();
		$nivel=$this->input->post("nivel");
		$gestion=$this->input->post("gestion");
		$curso=$this->input->post("curso");	
		$subarea=$this->input->post("subarea");
		$bimestre=$this->input->post("bimestre");
		$data['curso_list']=$musuariocurso->encontrarPorIdUsuarioIdgestion($usr->id_usuario,$curricular_year->id_gestion);				
		$data['curso']=$curso;
		$data['subarea']=$subarea;
		$data['bimestre']=$bimestre; 		
		if(isset($curso) && $curso['id_curso']!=""){
			$data['subarea_list']=$mcursosubarea->findByIdCurso($curso["id_curso"],$gestion['id_gestion']);			
		}				
		$this->form_validation->set_rules($this->validation->config[$this->model6]);
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('reportes/rpt-materias-asesor', $data);
		} else {
			$data["estudiantes_list"]=$mkardexdetalle->obtenerEstudiantesPorIdCursoIdGestion($gestion['id_gestion'],$curso['id_curso']);
			$this->load->view('reportes/rpt-materias-asesor', $data);
		}		
	}
}

/* End of file Reportes.php */
/* Location: ./application/controllers/Reportes.php */