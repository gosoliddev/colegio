<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkardexbimestreobservacion extends CI_Model {
	var $table="kardex_bimestre_observacion";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function save($data,$id)
	{
		if (!$id) {
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			return $id;
		}
		else {
			$this->db->update($this->table, $data, array("id_usuario" => $id));
			$afftectedRows = $this->db->affected_rows();return $afftectedRows;
		}
	}
}

/* End of file mkardexbimestreobservacion.php */
/* Location: ./application/models/mkardexbimestreobservacion.php */