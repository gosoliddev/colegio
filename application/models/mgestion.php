<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MGestion extends CI_Model {

	var $table="gestion";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}	
	public function load($search,$pageSize,$offset,$sort){

                $query=$this->db->select(" * ,gestion.activo, unidad_educativa.nombre as descripcion_unidad_educativa, count(*) OVER() AS  total ");
                $query=$query->from($this->table);
                $query=$query->join("unidad_educativa",'unidad_educativa.id_unidad_educativa = '.$this->table.'.id_unidad_educativa');
                if(sizeof($sort)) {
                        $query = $query->order_by($this->sort_map($sort));
                }
                if(isset($search)){
                        $query=$query->like("Concat(gestion.fecha_actualizacion)",$search);
                }
                if($pageSize>-1){

                        $query=$query->limit($pageSize);
                        $query=$query->offset(($pageSize*($offset-1)));
                }
                $query=$query->get();
                return $query->result();
        }
        public function save($data,$id){
                if (!$id) {
                        $this->db->insert($this->table, $data);
                        $id = $this->db->insert_id();
                        return $id;
                } 
                else {
                        $this->db->update($this->table, $data, array("id_gestion" => $id));
                        $afftectedRows = $this->db->affected_rows();
                        return $afftectedRows;
                }    
        }
        function remove_empty($array) {
                return array_diff($array, array(''));
        }
        function sort_map($array)
        {
                $sort="";
                foreach ($array as $key => $value){
                        $sort.=$key." ".$value;
                }
                return $sort;
        }
        public function get($id){               
                $query=$this->db->select("*");          
                $query=$this->db->from($this->table);           
                $query=$query->where($this->table.'.id_gestion',$id)->get();                   
                return $query->row();
        }
        public function delete($id){                                            
                $this->db->where('id_gestion', $id);
                $this->db->delete($this->table);
                if ($this->db->affected_rows() > 0)
                {
                        return TRUE;
                }
                else
                {
                        return FALSE;
                }
        }
        public function _list(){                
                $query=$this->db->select("*");          
                $query=$query->from($this->table);              
                $query=$query->get();                   
                return $query->result();
        }
        public function findByCurrent($date){
                $query=$this->db->select("*");
                $query=$query->from($this->table);
                $query=$query->where('DATE(inicio_gestion) <= \''. date('Y-m-d', strtotime($date)). '\'');
                $query=$query->where('DATE(fin_gestion) >= \''. date('Y-m-d', strtotime($date)). '\'');
                $query=$query->where('activo',1);
                $query=$query->get();
                return $query->row();
        }
        public function validaGestion($dateStart,$dateEnd){
               $query=$this->db->select("*");
                $query=$query->from($this->table);
                $query=$query->where('DATE(inicio_gestion) <= DATE(\''. date('Y-m-d', strtotime($dateStart)). '\')');
                $query=$query->where('DATE(fin_gestion) >= DATE(\''. date('Y-m-d', strtotime($dateEnd)). '\')');
                $query=$query->where('activo',1);
                $query=$query->get();
                if ($query->num_rows() > 0)
                {
                        return "\"false\"";
                }
                else
                {
                        return "\"true\"";
                }
        }

}

/* End of file curso.php */
/* Location: ./application/models/curso.php */