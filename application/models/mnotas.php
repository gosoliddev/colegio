<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MNotas extends CI_Model {

	var $table="notas";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}	
	public function save($data,$id)
	{
		if (!$id) {
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			return $id;
		}
		else {
			$this->db->update($this->table, $data, array("id_nota" => $id));
			$afftectedRows = $this->db->affected_rows();return $afftectedRows;


		}
	}
	public function load($data,$pageSize,$offset){

		$query=$this->db->select(" * , count(*) OVER() AS  itemsCount ");
		$query=$query->from($this->table);
		$data=$this->remove_empty($data);
		if($data){
			$query=$query->like($data);
		}
		$query=$query->limit($pageSize);
		$query=$query->offset($offset)->get();
		return $query->result();
	}
	function remove_empty($array) {
		return array_diff($array, array(''));
	}

}

/* End of file cargo.php */
/* Location: ./application/models/cargo.php */