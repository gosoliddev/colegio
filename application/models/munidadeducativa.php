<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MUnidadEducativa extends CI_Model {

	var $table="unidad_educativa";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}	
	public function load($search,$pageSize,$offset,$sort){
		
		$query=$this->db->select(" *, count(*) OVER() AS  total ");
		$query=$query->from($this->table);
		if(sizeof($sort)) {
			$query = $query->order_by($this->sort_map($sort));
		}
		if(isset($search)){
			$query=$query->like("Concat(nombre, '', descripcion, '', codigo_sie)",$search);
		}
		if($pageSize>-1){

			$query=$query->limit($pageSize);
			$query=$query->offset(($pageSize*($offset-1)));
		}
		$query=$query->get();
		return $query->result();
	}
	public function save($data,$id){
		if (!$id) {
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			return $id;
		} 
		else {
			$this->db->update($this->table, $data, array("id_unidad_educativa" => $id));
			$afftectedRows = $this->db->affected_rows();
			return $afftectedRows;
		}    
	}
	function remove_empty($array) {
		return array_diff($array, array(''));
	}
	function sort_map($array)
	{
		$sort="";
		foreach ($array as $key => $value){
			$sort.=$key." ".$value;
		}
		return $sort;
	}
	public function get($id){		
		$query=$this->db->select("*");		
		$query=$this->db->from($this->table);		
		$query=$query->where($this->table.'.id_unidad_educativa',$id)->get();			
		return $query->row();
	}
	public function delete($id){						
		$this->db->where('id_unidad_educativa', $id);
		$this->db->delete($this->table);
		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function _list(){		
		$query=$this->db->select("*");		
		$query=$query->from($this->table);		
		$query=$query->get();			
		return $query->result();
	}
}

/* End of file unidadEducativa.php */
/* Location: ./application/models/unidadEducativa.php */