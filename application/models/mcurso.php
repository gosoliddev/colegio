<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MCurso extends CI_Model {

	var $table="curso";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}	
	public function load($search,$pageSize,$offset,$sort){

		$query=$this->db->select(" curso.* , curso.descripcion as descripcion_curso,nivel.descripcion as descripcion_nivel, count(*) OVER() AS  total ");
		$query=$query->from($this->table);
		$query=$query->join("nivel",'nivel.id_nivel = '.$this->table.'.id_nivel');
		if(sizeof($sort)) {
			$query = $query->order_by($this->sort_map($sort));
		}
		if(isset($search)){
			$query=$query->like("Concat('',curso.descripcion)",$search);
		}
		if($pageSize>-1){

			$query=$query->limit($pageSize);
			$query=$query->offset(($pageSize*($offset-1)));
		}
		$query=$query->get();
		return $query->result();
	}
	function sort_map($array)
	{
		$sort="";
		foreach ($array as $key => $value){
			$sort.=$key." ".$value;
		}
		return $sort;
	}
	public function save($data,$id){
		if (!$id) {
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			return $id;
		} 
		else {
			$this->db->update($this->table, $data, array("id_curso" => $id));
			$afftectedRows = $this->db->affected_rows();
			return $afftectedRows;
		}    
	}
	function remove_empty($array) {
		return array_diff($array, array(''));
	}
	public function get($id){		
		$query=$this->db->select("*");		
		$query=$this->db->from($this->table);		
		$query=$query->where($this->table.'.id_curso',$id)->get();			
		return $query->row();
	}
	public function delete($id){						
		$this->db->where('id_curso', $id);
		$this->db->delete($this->table);
		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function _list(){		
		$query=$this->db->select("*");		
		$query=$query->from($this->table);		
		$query=$query->get();			
		return $query->result();
	}
	public function checkDuplicate($nivel,$parallel,$grade){
		$query=$this->db->select("*");		
		$query=$query->from($this->table);				
		$query=$query->where('paralelo',$parallel);		
		$query=$query->where('grado',$grade);		
		$query=$query->where('id_nivel',$nivel);
		$query=$query->get();
		if ($query->num_rows() > 0)
		{
			return "\"false\"";
		}
		else
		{
			return "\"true\"";
		}
	}	
	public function findByIdNivel($idNivel){               
		$query=$this->db->select("*");          
		$query=$query->from($this->table);           		
		$query=$query->where($this->table.'.id_nivel',$idNivel)->get();                   
		return $query->result();
	}

}

/* End of file curso.php */
/* Location: ./application/models/curso.php */