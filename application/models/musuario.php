<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musuario extends CI_Model {	

        var $table="usuario";
        public function __construct()
        {
                parent::__construct();        
        }
        public function credentials($username,$password){
                $query=$this->db->select("*");          
                $query=$this->db->from($this->table);           
                $query=$query->where($this->table.'.login',$username);
                $query=$query->where($this->table.'.clave',$password);
                return $query->get()->row();
        }
        public function get($id){
                $query=$this->db->select("*");
                $query=$this->db->from($this->table);
                $query=$query->where($this->table.'.id_usuario',$id)->get();
                return $query->row();
        }
        public function load($search,$pageSize,$offset,$sort){

                $query=$this->db->select(" * , unidad_educativa.descripcion as descripcion_unidad_educativa,usuario.activo as activo, count(*) OVER() AS  total ");
                $query=$query->from($this->table);
                $query=$query->join("unidad_educativa",'unidad_educativa.id_unidad_educativa = '.$this->table.'.id_unidad_educativa');
                if(sizeof($sort)) {
                        $query = $query->order_by($this->sort_map($sort));
                }
                if(isset($search)){
                        $query=$query->like("Concat(nombres, '', apellido_paterno, '', apellido_materno,'', ci,'', login)",$search);
                }
                if($pageSize>-1){

                        $query=$query->limit($pageSize);
                        $query=$query->offset(($pageSize*($offset-1)));
                }
                $query=$query->get();
                return $query->result();
        }
        function sort_map($array)
        {
                $sort="";
                foreach ($array as $key => $value){
                        $sort.=$key." ".$value;
                }
                return $sort;
        }
        public function delete($id){
                $this->db->where('id_usuario', $id);
                $this->db->delete($this->table);
                if ($this->db->affected_rows() > 0)
                {
                        return TRUE;
                }
                else
                {
                        return FALSE;
                }
        }
        public function save($data,$id)
        {
                if (!$id) {
                        $this->db->insert($this->table, $data);
                        $id = $this->db->insert_id();
                        return $id;
                }
                else {
                        $this->db->update($this->table, $data, array("id_usuario" => $id));
                        $afftectedRows = $this->db->affected_rows();return $afftectedRows;


                }
        }
        public function findByIdCargo($id_cargo){
                $query=$this->db->select("*");          
                $query=$this->db->from($this->table);
                $query=$query->join("usuario_cargo",'usuario_cargo.id_usuario = '.$this->table.'.id_usuario');        
                $query=$query->where('usuario_cargo.id_cargo',$id_cargo);                
                return $query->get()->result();

        }
        public function findByCodigoCargo($codigo){
                $query=$this->db->select("*");          
                $query=$this->db->from($this->table);
                $query=$query->join("usuario_cargo",'usuario_cargo.id_usuario = '.$this->table.'.id_usuario');        
                $query=$query->join("cargo",'cargo.id_cargo = usuario_cargo.id_cargo');        
                $query=$query->where('cargo.codigo',$codigo);                
                return $query->get()->result();

        }
        function remove_empty($array) {
                return array_diff($array, array(''));
        } 

}

/* End of file usuario.php */
/* Location: ./application/models/usuario.php */