<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MArea extends CI_Model {

	var $table="area";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}	
	public function load($search,$pageSize,$offset,$sort){
		
		$query=$this->db->select(" area.* ,campo.descripcion as descripcion_campo , count(*) OVER() AS  total ");
		$query=$query->from($this->table);
		$query=$query->join("campo",'campo.id_campo = '.$this->table.'.id_campo');
		$query=$query->order_by("area.id_campo");
		if(sizeof($sort)) {
			$query = $query->order_by($this->sort_map($sort));
		}
		if(isset($search)){
			$query=$query->like("Concat(area.descripcion, '')",$search);
		}
		if($pageSize>-1){

			$query=$query->limit($pageSize);
			$query=$query->offset(($pageSize*($offset-1)));
		}
		$query=$query->get();
		return $query->result();
	}
	public function save($data,$id){
		if (!$id) {
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			return $id;
		} 
		else {
			$this->db->update($this->table, $data, array("id_area" => $id));
			$afftectedRows = $this->db->affected_rows();
			return $afftectedRows;
		}    
	}
	function remove_empty($array) {
		return array_diff($array, array(''));
	}
	function sort_map($array)
	{
		$sort="";
		foreach ($array as $key => $value){
			$sort.=$key." ".$value;
		}
		return $sort;
	}
	public function get($id){		
		$query=$this->db->select("*");		
		$query=$this->db->from($this->table);		
		$query=$query->where($this->table.'.id_area',$id)->get();			
		return $query->row();
	}
	public function delete($id){						
		$this->db->where('id_area', $id);
		$this->db->delete($this->table);
		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function _list(){		
		$query=$this->db->select("*");		
		$query=$query->from($this->table);		
		$query=$query->get();			
		return $query->result();
	}
	public function findByIdCampo($idCampo){
		$query=$this->db->select("*");
		$query=$query->from($this->table);		
		$query=$query->where("id_campo",$idCampo);
		return $query->get()->result();
	}

}

/* End of file cargo.php */
/* Location: ./application/models/cargo.php */