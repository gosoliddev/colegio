<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MCursoSubArea extends CI_Model {

	var $table="curso_subarea";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}	
	public function load($data,$pageSize,$offset){

		$query=$this->db->select(" * , count(*) OVER() AS  itemsCount ");
		$query=$query->from($this->table);
		$data=$this->remove_empty($data);
		if($data){
			$query=$query->like($data);
		}
		$query=$query->limit($pageSize);
		$query=$query->offset($offset)->get();
		return $query->result();
	}
	function remove_empty($array) {
		return array_diff($array, array(''));
	}
	public function findByIdCurso($idCurso,$idGestion){
		$query=$this->db->select("curso_subarea.activo as active,*");
		$query=$query->from($this->table);
		$query=$query->join("subarea","subarea.id_subarea=".$this->table.".id_subarea");
		$query=$query->where($this->table.".id_curso",$idCurso);
		$query=$query->where($this->table.".id_gestion",$idGestion);
		$query=$query->where($this->table.".activo",1);
		$query=$query->order_by("subarea.descripcion","asc");
		return $query->get()->result();
	}
	public function encontrarPorIdCursoIdUsuario($idCurso,$idusuario){
		$query=$this->db->select("subarea.*");
		$query=$query->from($this->table);
		$query=$query->join("subarea","subarea.id_subarea=".$this->table.".id_subarea");
		$query=$query->join("curso","curso.id_curso=".$this->table.".id_curso");
		$query=$query->join("usuario_curso","usuario_curso.id_curso=curso.id_curso");
		$query=$query->join("usuario_subarea","usuario_subarea.id_subarea=subarea.id_subarea");		
		$query=$query->where($this->table.".id_curso",$idCurso);
		$query=$query->where("usuario_curso.id_usuario",$idusuario);
		$query=$query->where("usuario_subarea.activSo",1);
		return $query->get()->result();
	}	
	public function obtenerSubAreasUsuario($idUsuario,$idGestion,$idCurso){
		$sql = "SELECT usa.*,sa.* from usuario_subarea usa
		JOIN subarea sa ON sa.id_subarea=usa.id_subarea
		JOIN curso_subarea csa ON csa.id_subarea=sa.id_subarea
		WHERE  usa.id_usuario=".$idUsuario." AND usa.activo=1 AND csa.id_curso=".$idCurso." AND usa.id_gestion=".$idGestion; 
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	public function obtenerSubAreasUsuarioFix($idUsuario,$idGestion,$idCurso){
		$sql = "SELECT DISTINCT usa.*,sa.* from usuario_subarea usa
		JOIN subarea sa ON sa.id_subarea=usa.id_subarea
		JOIN curso_subarea csa ON csa.id_subarea=sa.id_subarea
		JOIN usuario_curso uc ON uc.id_usuario=usa.id_usuario
		WHERE  usa.id_usuario=".$idUsuario." AND usa.activo=1 AND csa.activo=1 AND csa.id_curso=".$idCurso." AND usa.id_gestion=".$idGestion." AND uc.id_usuario=".$idUsuario.""; 
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	public function save($data,$id)
	{			
		if ($id!="") {						
			$this->db->update($this->table, $data, array("id_curso_subarea" => $id));
			$afftectedRows = $this->db->affected_rows();
			return $afftectedRows;
		}
		else {        	
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			return $id;

		}
	}
}

/* End of file cargo.php */
/* Location: ./application/models/cargo.php */