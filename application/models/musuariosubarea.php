<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MUsuarioSubArea extends CI_Model {

	var $table="usuario_subarea";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}		 
	function remove_empty($array) {
		return array_diff($array, array(''));
	}
	public function findbyIdUsuario($idUsuario,$idGestion,$activo){
		$query=$this->db->select($this->table.".activo as active,".$this->table.".id_usuario_subarea,subarea.*");
		$query=$query->from($this->table);		
		$query=$query->join("usuario","usuario.id_usuario=".$this->table.".id_usuario");		
		$query=$query->join("subarea","subarea.id_subarea=".$this->table.".id_subarea");
		if(!empty($idUsuario) && !empty($idGestion)){
			$query=$query->where($this->table.".id_usuario",$idUsuario);		 
			$query=$query->where($this->table.".id_gestion",$idGestion);
		}
		if(!empty($activo)){
			$query=$query->where($this->table.".activo",$activo);
		}
		return $query->get()->result();
	}
	public function save($data,$id)
	{			
		if ($id!="") {						
			$this->db->update($this->table, $data, array("id_usuario_subarea" => $id));
			$afftectedRows = $this->db->affected_rows();
			return $afftectedRows;
		}
		else {        	
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			return $id;

		}
	}
	public function deletePorIdGestionIdUsuario($idGestion,$idUsuario){			 
		$this->db->where("id_usuario",$idUsuario);
		$this->db->where("id_gestion",$idGestion);
		$this->db->delete($this->table);	
		return $this->db->affected_rows();
	}
	public function _list(){		
		$query=$this->db->select("*");		
		$query=$query->from($this->table);		
		$query=$query->get();			
		return $query->result();
	}
}

/* End of file cargo.php */
/* Location: ./application/models/cargo.php */