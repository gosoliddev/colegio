<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MSubArea extends CI_Model {

	var $table="subarea";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}	
	public function get($id){
        $query=$this->db->select("*");
        $query=$this->db->from($this->table);
        $query=$query->where($this->table.'.id_subarea',$id)->get();
        return $query->row();
    }
    public function load($search,$pageSize,$offset,$sort){

        $query=$this->db->select(" * ,subarea.activo, subarea.descripcion as descripcion_subarea,subarea.fecha_actualizacion as fecha_actualizacion,area.descripcion descripcion_area,count(*) OVER() AS  total ");
         $query=$query->from($this->table);
         $query=$query->join("area","area.id_area=".$this->table.".id_area");
         $query=$query->order_by($this->table.".id_area, ".$this->table.'.fecha_actualizacion desc');
         if(sizeof($sort)) {
            $query = $query->order_by($this->sort_map($sort));
        }
        if(isset($search)){
            $query=$query->like("Concat( area.descripcion, '',subarea.descripcion,'', subarea.fecha_actualizacion)",$search);
        }
        if($pageSize>-1){

            $query=$query->limit($pageSize);
            $query=$query->offset(($pageSize*($offset-1)));
        }
        $query=$query->get();
        return $query->result();
    }
    public function delete($id){
        $this->db->where('id_subarea', $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    public function save($data,$id)
    {
        if (!$id) {
            $this->db->insert($this->table, $data);
            $id = $this->db->insert_id();
            return $id;
        }
        else {
            $this->db->update($this->table, $data, array("id_subarea" => $id));
            $afftectedRows = $this->db->affected_rows();return $afftectedRows;
        }
    }

    function remove_empty($array) {
        return array_diff($array, array(''));
    } 
    public function findByIdArea($idArea){
        $query=$this->db->select("*");
        $query=$query->from($this->table);      
        $query=$query->where("id_area",$idArea);
        return $query->get()->result();
    }
    public function _list(){        
        $query=$this->db->select('subarea.id_subarea,area.id_area ,campo.id_campo,subarea.descripcion,area.descripcion as descripcion_area,campo.descripcion as descripcion_campo ');      
        $query=$query->from($this->table);  
        $query=$query->join("area","area.id_area=".$this->table.".id_area");    
        $query=$query->join("campo","campo.id_campo=area.id_campo");    
        $query=$query->order_by("area.descripcion",'asc');    
        $query=$query->order_by("subarea.descripcion",'asc');                    
        $query=$query->order_by("campo.descripcion",'asc');            
        $query=$query->get();           
        return $query->result();
    }

}

/* End of file cargo.php */
/* Location: ./application/models/cargo.php */
