<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MBimestre extends CI_Model {

	var $table="bimestre";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function findByCurrent($date){
		$query=$this->db->select("*");
		$query=$query->from($this->table);
		$query=$query->where('DATE(fecha_inicio) <= \''. date('Y-m-d', strtotime($date)). '\'');
		$query=$query->where('DATE(fecha_fin) >= \''. date('Y-m-d', strtotime($date)). '\'');
		$query=$query->where('activo',1);
		$query=$query->get();
		return $query->row();
	}
	public function findByGestion($idGestion){
		$query=$this->db->select("*");
		$query=$query->from($this->table);		 
		$query=$query->where('id_gestion',$idGestion);
		$query=$query->get();
		return $query->result();
	}
	public function _list(){                
		$query=$this->db->select("*");          
		$query=$query->from($this->table);              
		$query=$query->get();                   
		return $query->result();
	}

}

/* End of file mBimestre.php */
/* Location: ./application/models/mBimestre.php */