<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MKardexCriterio extends CI_Model {

	var $table="kardex_dimension";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}	
	public function load($data,$pageSize,$offset){

		$query=$this->db->select(" * , count(*) OVER() AS  itemsCount ");
		$query=$query->from($this->table);
		$data=$this->remove_empty($data);
		if($data){
			$query=$query->like($data);
		}
		$query=$query->limit($pageSize);
		$query=$query->offset($offset)->get();
		return $query->result();
	}
	public function save($data,$id){
        if (!$id) {
            $this->db->insert($this->table, $data);
            $id = $this->db->insert_id();
            return $id;
        } 
        else {
            $this->db->update($this->table, $data, array("id_kardex_criterio" => $id));
            $afftectedRows = $this->db->affected_rows();
            return $afftectedRows;
        }    
    }
	function remove_empty($array) {
		return array_diff($array, array(''));
	}

}

/* End of file kardexcriterio.php */
/* Location: ./application/models/kardexcriterio.php */