<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MValoracion extends CI_Model {
	var $table="valoracion";
	var $tblObservacion="observacion";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	//Obtiene las observaciones o comentarios par alas valoraciones cualitativas 
	public function obtenerObservaciones($idNivel,$activo,$promedio){
		$sql=$this->db->from($this->tblObservacion." o");
		$sql=$sql->join("valoracion v","v.id_valoracion=o.id_valoracion");
		$sql=$sql->join("nivel n ","n.id_nivel=v.id_nivel");
		$sql=$sql->where("v.min_promedio<=",$promedio);		
		$sql=$sql->where("v.max_promedio>=",$promedio);			
		$sql=$sql->where("o.activo",$activo);		
		$sql=$sql->where("v.id_nivel",$idNivel)->get();		
		return $sql->result();
	}

}

/* End of file valoracion.php */
/* Location: ./application/models/valoracion.php */