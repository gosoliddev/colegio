<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MKardexDetalle extends CI_Model {

	var $table="kardex_detalle";
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}	
	public function load($data,$pageSize,$offset){

		$query=$this->db->select(" * , count(*) OVER() AS  itemsCount ");
		$query=$query->from($this->table);
		$data=$this->remove_empty($data);
		if($data){
			$query=$query->like($data);
		}
		$query=$query->limit($pageSize);
		$query=$query->offset($offset)->get();
		return $query->result();
	}
	function remove_empty($array) {
		return array_diff($array, array(''));
	}
	public function save($data,$id)
	{
		if (!$id) {
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			return $id;
		}
		else {
			$this->db->update($this->table, $data, array("id_estudiante" => $id));
			$afftectedRows = $this->db->affected_rows();
			return $afftectedRows;
		}
	}	 
	public function obtnerByIdCursoIdGestion($rude,$idGestion,$idCurso){
		$sql=$this->db->query("SELECT kd.* FROM kardex_detalle kd
			JOIN kardex k ON k.id_kardex=kd.id_kardex
			JOIN estudiante e ON e.id_estudiante=k.id_estudiante
			WHERE kd.id_gestion=".$idGestion." AND kd.id_curso=".$idCurso." AND e.rude='".$rude."'");		
		return $sql->row();
	}
	public function obtenerEstudiantesPorIdCursoIdGestion($idGestion,$idCurso){
		$sql=$this->db->query("SELECT e.*,kd.* FROM kardex_detalle kd
			JOIN kardex k ON k.id_kardex=kd.id_kardex
			JOIN estudiante e ON e.id_estudiante=k.id_estudiante
			WHERE kd.id_gestion=".$idGestion." AND kd.id_curso=".$idCurso."");		
		return $sql->result();
	}
	public function obtnerCursosByIdGestion($rude,$idGestion){
		$sql=$this->db->query("SELECT c.* FROM kardex_detalle kd
			JOIN kardex k ON k.id_kardex=kd.id_kardex
			JOIN curso c ON c.id_curso=kd.id_curso
			JOIN estudiante e ON e.id_estudiante=k.id_estudiante
			WHERE kd.id_gestion=".$idGestion." AND e.rude='".$rude."'");		
		return $sql->result();
	}
	public function obtenerNotasByRude($rude,$idSubarea,$codigo_dimension,$codigo_criterio,$bimestre){
		$sql=$this->db->query("SELECT e.rude,e.nombres, kd.bimestre,sa.descripcion,d.codigo as codigo_dimension,c.codigo as codigo_criterio,n.nota,kd.promedio,kd.autoevaluacion
			FROM kardex_dimension kd
			JOIN subarea sa ON sa.id_subarea=kd.id_subarea
			JOIN dimension d ON d.id_dimension=kd.id_dimension
			JOIN kardex_detalle kdt ON kdt.id_kardex_detalle=kd.id_kardex_detalle
			JOIN notas n ON n.id_kardex_criterio=kd.id_kardex_criterio
			JOIN criterio c ON c.id_criterio=n.id_criterio
			JOIN kardex k ON k.id_kardex=kdt.id_kardex
			JOIN estudiante e ON e.id_estudiante=k.id_estudiante
			WHERE  kd.id_subarea=".$idSubarea." AND e.rude='".$rude."' AND d.codigo='".$codigo_dimension."' AND c.codigo='".$codigo_criterio."' AND kd.bimestre='".$bimestre."' 
			GROUP BY e.rude,e.nombres,kd.bimestre,sa.descripcion,d.codigo,c.codigo,n.nota,kd.promedio,kd.autoevaluacion
			ORDER BY e.nombres , d.codigo");
		//$sql=$sql->get();
		return $sql->row();
	}
	public function obtenerPromedioAutoEvalucaionByRude($rude,$idSubarea,$codigo_dimension,$bimestre){
		$sql=$this->db->query("SELECT kd.promedio,kd.autoevaluacion
			FROM kardex_dimension kd
			JOIN subarea sa ON sa.id_subarea=kd.id_subarea
			JOIN dimension d ON d.id_dimension=kd.id_dimension
			JOIN kardex_detalle kdt ON kdt.id_kardex_detalle=kd.id_kardex_detalle
			JOIN notas n ON n.id_kardex_criterio=kd.id_kardex_criterio
			JOIN criterio c ON c.id_criterio=n.id_criterio
			JOIN kardex k ON k.id_kardex=kdt.id_kardex
			JOIN estudiante e ON e.id_estudiante=k.id_estudiante
			WHERE  kd.id_subarea=".$idSubarea." AND e.rude='".$rude."' AND d.codigo='".$codigo_dimension."' AND kd.bimestre='".$bimestre."'
			GROUP BY kd.promedio,kd.autoevaluacion");		
		return $sql->row();
	}
	
}

/* End of file cargo.php */
/* Location: ./application/models/cargo.php */