<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MCargo extends CI_Model {

	var $table="cargo";
	public function __construct()
	{
		parent::__construct();		
	}	
    public function get($id){               
        $query=$this->db->select("*");          
        $query=$this->db->from($this->table);           
        $query=$query->where($this->table.'.id_cargo',$id)->get();                   
        return $query->row();
    }
    public function load($search,$pageSize,$offset,$sort){

        $query=$this->db->select(" * , count(*) OVER() AS  total ");
        $query=$query->from($this->table);        
       if(sizeof($sort)) {
            $query = $query->order_by($this->sort_map($sort));
        }
        if(isset($search)){
            $query=$query->like("Concat(cargo.descripcion,'')",$search);
        }
        if($pageSize>-1){

            $query=$query->limit($pageSize);
            $query=$query->offset(($pageSize*($offset-1)));
        }
        $query=$query->get();
        return $query->result();
    }
    public function delete($id){                                            
        $this->db->where('id_cargo', $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    public function deleteByIdUsuario($idUsuario){                                            
        $this->db->where('id_usuario', $idUsuario);
        $this->db->delete("usuario_cargo");
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function save($data,$id)
    {
        if (!$id) {
            $this->db->insert($this->table, $data);
            $id = $this->db->insert_id();
            return $id;
        } 
        else {
            $this->db->update($this->table, $data, array("id_cargo" => $id));
            $afftectedRows = $this->db->affected_rows();return $afftectedRows;


        }
    }
    public function saveCargos($data)
    {        
        return $this->db->insert('usuario_cargo', $data);    
    }
    function sort_map($array)
    {
        $sort="";
        foreach ($array as $key => $value){
            $sort.=$key." ".$value;
        }
        return $sort;
    }
    function remove_empty($array) {
        return array_diff($array, array(''));
    }
    public function _list(){                
        $query=$this->db->select("*");          
        $query=$query->from($this->table);              
        $query=$query->get();                   
        return $query->result();
    }
    public function findByIdUsuario($idUsuario){                
        $query=$this->db->select("*");          
        $query=$query->from("usuario_cargo");              
        $query=$query->join("cargo","cargo.id_cargo=usuario_cargo.id_cargo");              
        $query=$query->where("usuario_cargo.id_usuario",$idUsuario);              
        $query=$query->get();                   
        return $query->row();
    }

}

/* End of file cargo.php */
/* Location: ./application/models/cargo.php */