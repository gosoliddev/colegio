<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();		 
		$this->load->model('musuario');		
		$this->load->model('mcargo');		
		$this->validation();		
	}
	
	public function validation(){		 
		if(!is_null($this->input->post('username'))){	

			$this->form_validation->set_rules($this->validation->config['logon']);
			if ($this->form_validation->run()==TRUE)
			{				
				$musuario=new musuario();
				$mcargo=new mcargo();
				$login=$this->input->post("username");
				$password=md5($this->input->post('password'));
				$usuario=$this->musuario->credentials($login,$password);
				if(isset($usuario)){
					$cargo=$mcargo->findByIdUsuario($usuario->id_usuario);
					$this->session->set_userdata('usr',$usuario);								
					$this->session->set_userdata('cargo',$cargo);								
					$this->session->set_flashdata('success', lang('bienvenido').' '.$usuario->login);				
					$cookie = array(
						'name'   => 'rid',
						'value'  => $cargo->codigo,
						'expire' => '86500'					
						);

					$this->input->set_cookie($cookie);		
				}	
				else
					error(lang('password_user_invalid'));				
			}
		}				
		if(is_null($this->session->userdata('usr'))){
			if($this->input->is_ajax_request()){
				exit("<script>location.reload();</script>");
			}
			else{
				exit($this->load->view('login', NULL, TRUE));
			}
		}
	}
}
