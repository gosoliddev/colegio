<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('curricular_year'))
{
	function curricular_year()
	{
		$ci=& get_instance();
		$ci->load->database(); 
		$ci->load->model('mgestion');
		$mgestion=new mgestion();
		$date=date('Y-m-d h:m');
		$gestion=$mgestion->findByCurrent($date);	
		return $gestion;	 
	}
}
if ( ! function_exists('current_bimestre'))
{
	function current_bimestre()
	{
		$date=date('Y-m-d h:m');
		$ci=& get_instance();
		$ci->load->database(); 
		$ci->load->model('mbimestre');
		$mbimestre=new mbimestre();
		$date=date('Y-m-d h:m');
		$bimestre=$mbimestre->findByCurrent($date);	
		return $bimestre;	 
	}
}
if ( ! function_exists('existe_subarea_curso'))
{
	function existe_subarea_curso($subarea,$gestion,$id_curso)
	{
		$ci=& get_instance();
		$ci->load->database(); 
		$ci->load->model('mcursosubarea');
		$mcursosubarea=new mcursosubarea();
		$date=date('Y-m-d h:m');		
		/*var_dump($subarea)		;
		var_dump($gestion)		;
		var_dump($id_curso)		;		*/
		if(!empty($subarea) && !empty($gestion)){
			$sql = " SELECT  * FROM curso_subarea WHERE id_subarea=".$subarea." AND id_gestion=".$gestion->id_gestion."  AND id_curso=".$id_curso." AND activo=1"; 
			$query = $ci->db->query($sql);
			$row = $query->result();
			if($row)
			{
				return true;
			}
			return false;			
		}
		return false;
	}
}
if ( ! function_exists('obtener_usuario_asesor'))
{
	function obtener_usuario_asesor($id_curso,$id_gestion)
	{
		$ci=& get_instance();
		$ci->load->database(); 				
		if(!empty($id_curso) && !empty($id_gestion)){
			$sql = " select u.* from usuario_curso uc JOIN usuario u ON u.id_usuario=uc.id_usuario 
			WHERE uc.asesor=1 AND uc.id_curso=".$id_curso." AND uc.id_gestion=".$id_gestion; 
			$query = $ci->db->query($sql);			
			return $query->row();			 		
		}
		return null;
	}
}
if ( ! function_exists('obtener_promedi_bimestre'))
{
	function obtener_promedio_bimestre_rpt1($bimestre,$id_curso,$id_subarea,$id_gestion,$id_estudiante)
	{
		$ci=& get_instance();
		$ci->load->database(); 						
		$sql = "SELECT ROUND(SUM(promedio)) promedio
		FROM (SELECT sa.id_area,kdim.id_subarea, SUM(kdim.promedio) as promedio
		FROM kardex_dimension kdim
		JOIN kardex_detalle kd ON kd.id_kardex_detalle=kdim.id_kardex_detalle
		JOIN kardex k ON k.id_kardex=kd.id_kardex
		JOIN estudiante e ON e.id_estudiante=k.id_estudiante
		JOIN subarea sa ON sa.id_subarea=kdim.id_subarea
		JOIN area a On a.id_area=sa.id_area
		JOIN campo c ON c.id_campo=a.id_campo		
		WHERE bimestre=".$bimestre."	
		AND kd.id_gestion=".$id_gestion."	
		AND e.id_estudiante=".$id_estudiante."
		AND kdim.id_subarea=".$id_subarea."
		AND kd.id_curso=".$id_curso."
		GROUP BY sa.id_area,kdim.id_subarea ) _table
		GROUP BY id_area"; 
		$query = $ci->db->query($sql);			
		return $query->unbuffered_row("array");			 		
	}
}
if ( ! function_exists('obtener_promedi_bimestre'))
{
	function obtener_aprobado_rpt1($id_curso,$id_subarea,$id_gestion,$id_estudiante)
	{
		$ci=& get_instance();
		$ci->load->database(); 						
		$sql = "SELECT SUM(promedio) as promedio_bismetral,ROUND(SUM(promedio)/(SELECT COUNT(*) FROM bimestre)) promedio
		FROM (SELECT sa.id_area,kdim.id_subarea,bimestre, (SUM(kdim.promedio)) as promedio
		FROM kardex_dimension kdim
		JOIN kardex_detalle kd ON kd.id_kardex_detalle=kdim.id_kardex_detalle
		JOIN kardex k ON k.id_kardex=kd.id_kardex
		JOIN estudiante e ON e.id_estudiante=k.id_estudiante
		JOIN subarea sa ON sa.id_subarea=kdim.id_subarea
		JOIN area a On a.id_area=sa.id_area
		JOIN campo c ON c.id_campo=a.id_campo			
		WHERE kd.id_gestion=".$id_gestion."	
		AND e.id_estudiante=".$id_estudiante."
		AND kdim.id_subarea=".$id_subarea."
		AND kd.id_curso=".$id_curso."
		GROUP BY sa.id_area,kdim.id_subarea,bimestre ) _table
		GROUP BY id_area"; 
		$query = $ci->db->query($sql);			
		return $query->unbuffered_row("array");			 		
	}
}
if ( ! function_exists('obtener_unidad_educativa'))
{
	function obtener_unidad_educativa()
	{
		$ci=& get_instance();
		$ci->load->database(); 	
		$usr=$ci->session->userdata('usr');					
		$sql = "SELECT * FROM unidad_educativa where id_unidad_educativa=".$usr->id_unidad_educativa; 
		$query = $ci->db->query($sql);			
		return $query->row();			 		
	}
}
/*foreach ($this->input->post("cualitativo") as $key => $value) {
		$tmpEstudiante=$mestudiante->findByRude($key.'');
		$tmpKardex=$mkardexdetalle->obtnerByIdCursoIdGestion($tmpEstudiante->rude,$gestion->id_gestion,$idCurso);
		$observacion=array(
			"id_kardex_detalle"=>$tmpKardex->id_kardex_detalle,
			"bimestre"=>$bimestre,
			"observacion"=>$value,
			"fecha_registro"=>$date,
			"fecha_actualizacion"=>$date,
			"usuario_registro"=>$usr->id_usuario,
			"usuario_actualizacion"=>$usr->id_usuario);
		$mkardexbimestreobservacion->save($observacion,null);
	}*/