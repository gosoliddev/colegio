<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('existe_campo'))
{
	function existe_campo($id_estudiante = 0,$id_campo=0,$idGestion)
	{
		$ci=& get_instance();
		$ci->load->database(); 

		$sql = "SELECT c.descripcion campo,a.descripcion area, sa.descripcion materia FROM kardex_detalle kd
		JOIN kardex k ON k.id_kardex=kd.id_kardex
		JOIN estudiante e ON e.id_estudiante=k.id_estudiante
		JOIN kardex_dimension kdim ON kdim.id_kardex_detalle=kd.id_kardex_detalle
		JOIN dimension dim ON dim.id_dimension=kdim.id_dimension
		JOIN subarea sa ON sa.id_subarea=kdim.id_subarea
		JOIN area a ON a.id_area=sa.id_area
		JOIN campo c ON c.id_campo=a.id_campo
		WHERE e.id_estudiante=".$id_estudiante." and c.id_campo=".$id_campo." AND kd.id_gestion=".$idGestion."
		GROUP BY c.descripcion ,a.descripcion , sa.descripcion"; 
		$query = $ci->db->query($sql);
		$row = $query->result();
		if($row)
		{
			return true;
		}
		return false;		
	}   
}
if ( ! function_exists('obtener_areas'))
{
	function obtener_areas($id_estudiante = 0,$id_campo=0,$idGestion,$id_curso)
	{
		$ci=& get_instance();
		$ci->load->database(); 

		$sql = "SELECT DISTINCT  a.id_area,a.descripcion FROM kardex_detalle kd
		JOIN kardex k ON k.id_kardex=kd.id_kardex
		JOIN estudiante e ON e.id_estudiante=k.id_estudiante
		JOIN kardex_dimension kdim ON kdim.id_kardex_detalle=kd.id_kardex_detalle
		JOIN dimension dim ON dim.id_dimension=kdim.id_dimension
		JOIN subarea sa ON sa.id_subarea=kdim.id_subarea
		JOIN area a ON a.id_area=sa.id_area
		JOIN campo c ON c.id_campo=a.id_campo
		WHERE e.id_estudiante=".$id_estudiante." and c.id_campo=".$id_campo." AND kd.id_gestion=".$idGestion." 
		AND kd.id_curso=".$id_curso; 
		$query = $ci->db->query($sql);		
		return $query->result();		
	}   
}

if ( ! function_exists('obtener_nota'))
{
	function obtener_nota($id_estudiante = 0,$id_campo=0,$bimestre=0,$idGestion,$id_curso)
	{
		$ci=& get_instance();
		$ci->load->database(); 

		$sql = "SELECT sa.id_area,(
		SELECT ROUND(SUM(promedio)/COUNT(promedio)) promedio
		FROM (SELECT sa.id_area,kdim.id_subarea, SUM(kdim.promedio) as promedio
		FROM kardex_dimension kdim
		JOIN kardex_detalle kd ON kd.id_kardex_detalle=kdim.id_kardex_detalle
		JOIN kardex k ON k.id_kardex=kd.id_kardex
		JOIN estudiante e ON e.id_estudiante=k.id_estudiante
		JOIN subarea sa ON sa.id_subarea=kdim.id_subarea
		JOIN area a On a.id_area=sa.id_area
		JOIN campo c ON c.id_campo=a.id_campo
		WHERE bimestre=".$bimestre." AND a.id_area=aa.id_area AND kd.id_gestion=".$idGestion." AND kd.id_curso=".$id_curso." AND e.id_estudiante=".$id_estudiante."
		GROUP BY sa.id_area,kdim.id_subarea ) _table
		GROUP BY id_area) as promedio
		FROM kardex_detalle kd
		JOIN kardex k ON k.id_kardex=kd.id_kardex
		JOIN estudiante e ON e.id_estudiante=k.id_estudiante
		JOIN kardex_dimension kdim ON kdim.id_kardex_detalle=kd.id_kardex_detalle
		JOIN dimension dim ON dim.id_dimension=kdim.id_dimension
		JOIN subarea sa ON sa.id_subarea=kdim.id_subarea
		JOIN area aa ON aa.id_area=sa.id_area
		JOIN campo ca ON ca.id_campo=aa.id_campo
		WHERE e.id_estudiante=".$id_estudiante." AND aa.id_area=".$id_campo." AND kdim.bimestre=1 AND kd.id_gestion=".$idGestion." AND kd.id_curso=".$id_curso."
		GROUP BY sa.id_area,aa.id_area
		"; 
		$query = $ci->db->query($sql);		
		return $query->row();		
	}   
}

if ( ! function_exists('obtener_observacion'))
{
	function obtener_observacion($id_estudiante = 0,$bimestre=0,$idGestion)
	{
		$ci=& get_instance();
		$ci->load->database(); 
		$sql = "SELECT kbo.* from kardex_bimestre_observacion kbo
		JOIN kardex_detalle kd ON kd.id_kardex_detalle=kbo.id_kardex_detalle
		JOIN kardex k ON k.id_kardex=kd.id_kardex
		JOIN estudiante e ON e.id_estudiante=k.id_estudiante
		WHERE e.id_estudiante=".$id_estudiante." AND bimestre=".$bimestre." AND kd.id_gestion=".$idGestion; 
		$query = $ci->db->query($sql);		
		return $query->row();		
	}   
}


if ( ! function_exists('obtener_literal'))
{
	function obtener_literal($numero = 0)
	{
		$ci=& get_instance();
		$ci->load->library('LiteralNumber');
		$V=new LiteralNumber();		
		return $V->ValorEnLetras($numero,""); 
	}   
}
