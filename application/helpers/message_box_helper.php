<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('success'))
{
	function success( $message='')		
	{
		$ci=& get_instance();
		if(!isset($message) || $message=='')
		{
			$message=lang("success_message");
		} 		
		$msg=array('status'=>1,"message"=>$message);		
		$ci->session->set_flashdata('message',"toast(".json_encode($msg).");");		
	}   
}
if ( ! function_exists('error'))
{
	function error( $message='')		
	{
		$ci=& get_instance();
		if(!isset($message) || $message=='')
		{
			$message=lang("error_message");
		} 		
		$msg=array('status'=>0,"message"=>$message);		
		$ci->session->set_flashdata('message',"toast(".json_encode($msg).");");		
	}   
}
if ( ! function_exists('warnig'))
{
	function warnig( $message='')		
	{
		$ci=& get_instance();
		if(!isset($message) || $message=='')
		{
			$message=lang("warning_message");
		} 		
		$msg=array('status'=>0,"message"=>$message);		
		$ci->session->set_flashdata('message',"toast(".json_encode($msg).");");		
	}   
}
if ( ! function_exists('info'))
{
	function info( $message)		
	{
		$ci=& get_instance();		 	
		$msg=array('status'=>0,"message"=>$message);		
		$ci->session->set_flashdata('message',"toast(".json_encode($msg).");");		
	}   
}