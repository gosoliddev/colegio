<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('usuario'))
{
	function user_name($id_estudiante = 0,$id_campo=0)
	{
		$ci=& get_instance();
		$ci->load->database(); 
		$usr=$ci->session->userdata('usr');
		$cargo=$ci->session->userdata('cargo');
		echo $usr->login.'('.$cargo->descripcion.')';
	}
}

if ( ! function_exists('user_unidad_educativa'))
{
	function user_unidad_educativa( $idUser='')		
	{
		$ci=& get_instance();
		$ci->load->database(); 
		$ci->load->model("munidadeducativa");
		$munidadeducativa=new munidadeducativa();
		$usr=$ci->session->userdata('usr');
		return $munidadeducativa->get($usr->id_unidad_educativa);

	}   
}