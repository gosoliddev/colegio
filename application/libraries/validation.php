<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Validation {
    public $config;
    
    function __construct(){

        $this->config=  array(
            'logon'=>array(
                array(
                    'field' => 'username',
                    'label' => lang('username'),
                    'rules' => 'required'
                    ),
                array(
                    'field' => 'password',
                    'label' => lang('password'),
                    'rules' => 'required'           
                    ),
                

                ),
            'munidadeducativa'=>array(
                array(
                    'field' => 'unidad_educativa[nombre]',
                    'label' => lang('name'),
                    'rules' => 'required'
                    ),
                array(
                    'field' => 'unidad_educativa[descripcion]',
                    'label' => lang('description'),
                    'rules' => 'required'           
                    ),
                array(
                    'field' => 'unidad_educativa[codigo_sie]',
                    'label' => lang('number_sie'),
                    'rules' => 'required|trim|min_length[8]|max_length[8]'           
                    ),                         
                array(
                    'field' => 'unidad_educativa[dependencia]',
                    'label' => lang('dependency'),
                    'rules' => 'required'
                    )

                ),
            'musuario'=>array(
                array(
                    'field' => 'usuario[nombres]',
                    'label' => lang('names'),
                    'rules' => 'required'
                    ),
                array(
                    'field' => 'usuario[apellido_paterno]',
                    'label' => lang('last_name'),
                    'rules' => 'required'           
                    ),
                array(
                    'field' => 'usuario[apellido_materno]',
                    'label' => lang('second_last_name'),
                    'rules' => 'required|trim'           
                    ),            
                array(
                    'field' => 'usuario[ci]',
                    'label' => lang('ci'),
                    'rules' => 'required|numeric'
                    ),
                array(
                    'field' => 'usuario[ci_expedido]',
                    'label' => lang('expedido'),
                    'rules' => 'required'
                    ),
                array(
                    'field' => 'usuario[telefono]',
                    'label' => lang('telephone'),
                    'rules' => 'required'
                    ),
                array(
                    'field' => 'usuario[email]',
                    'label' => lang('email'),
                    'rules' => 'required'
                    )
                ),
            'mnivel'=>array(
                array(
                    'field' => 'nivel[id_unidad_educativa]',
                    'label' => lang('unit_education'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'nivel[descripcion]',
                    'label' => lang('descripcion'),
                    'rules' => 'trim|required'
                    )               
                ),
            'mcriterio'=>array(
                array(
                    'field' => 'criterio[id_dimension]',
                    'label' => lang('dimencion'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'criterio[descripcion]',
                    'label' => lang('description'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'criterio[porcentaje_calificacion]',
                    'label' => lang('percentage_rating'),
                    'rules' => 'trim|required'
                    )
                ),
            'mkardexcriterio'=>array(
                array(
                    'field' => 'kardexcriterio[id_criterio]',
                    'label' => lang('criterio'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'kardexcriterio[id_kardex_detalle]',
                    'label' => lang('kardex_detail'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'kardexcriterio[bimestre]',
                    'label' => lang('bi_monthly'),
                    'rules' => 'trim|required'
                    )
                ),
            'mnotas'=>array(
                array(
                    'field' => 'notas[id_kardex_criterio]',
                    'label' => lang('kardex_criterio'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'notas[nota_bimestral]',
                    'label' => lang('bi_monthly_note'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'notas[nota]',
                    'label' => lang('note'),
                    'rules' => 'trim|required'
                    )
                ),
            'mkardex'=>array(
                array(
                    'field' => 'kardex[id_estudiante]',
                    'label' => lang('student'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'kardex[activo]',
                    'label' => lang('active'),
                    'rules' => 'trim|required'
                    )
                ),
            'mkardexdetalle'=>array(
                array(
                    'field' => 'kardexdetalle[id_kardex]',
                    'label' => lang('kardex'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'kardexdetalle[id_gestion]',
                    'label' => lang('management'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'kardexdetalle[id_curso_subarea]',
                    'label' => lang('sub_area_course'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'kardexdetalle[nota_anual]',
                    'label' => lang('anual_note'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'kardexdetalle[activo]',
                    'label' => lang('active'),
                    'rules' => 'trim|required'
                    )
                ),
            'msubarea'=>array(
                array(
                    'field' => 'subarea[id_area]',
                    'label' => lang('area'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'subarea[descripcion]',
                    'label' => lang('description'),
                    'rules' => 'trim|required'
                    )               
                ),
            'marea'=>array(
                array(
                    'field' => 'area[id_campo]',
                    'label' => lang('countryside'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'area[descripcion]',
                    'label' => lang('description'),
                    'rules' => 'trim|required'
                    )                
                ),
            'mdimension'=>array(
             array(
                'field' => 'dimension[descripcion]',
                'label' => lang('description'),
                'rules' => 'trim|required'
                )                
             ),
            'mgestion'=>array(
                array(
                    'field' => 'gestion[id_unidad_educativa]',
                    'label' => lang('educational_unit'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'gestion[inicio_gestion]',
                    'label' => lang('start_management'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'gestion[fin_gestion]',
                    'label' => lang('end_management'),
                    'rules' => 'trim|required'
                    )              
                ),
            'mcurso'=>array(
                array(
                    'field' => 'curso[id_nivel]',
                    'label' => lang('level'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'curso[descripcion]',
                    'label' => lang('description'),
                    'rules' => 'trim|required|max_length[30]'
                    ),
                array(
                    'field' => 'curso[grado]',
                    'label' => lang('grade'),
                    'rules' => 'trim|required'
                    )               
                ),
            'mcursosubarea'=>array(
                array(
                    'field' => 'cursosubarea[id_curso]',
                    'label' => lang('course'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'cursosubarea[id_subarea]',
                    'label' => lang('subarea'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'cursosubarea[activo]',
                    'label' => lang('active'),
                    'rules' => 'trim|required'
                    )
                ),
            'mestudiante'=>array(
                array(
                    'field' => 'estudiante[sie_anterior]',
                    'label' => lang('number_sie'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[nombres]',
                    'label' => lang('names'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[apellido_paterno]',
                    'label' => lang('last_name'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[apellido_materno]',
                    'label' => lang('second_last_name'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[ci]',
                    'label' => lang('ci'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[ci_expedido]',
                    'label' => lang('ci_expedido'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[fecha_nacimiento]',
                    'label' => lang('student_birth'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[celular]',
                    'label' => lang('cell_phone'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[telefono]',
                    'label' => lang('telephone'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[genero]',
                    'label' => lang('sex'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[direccion]',
                    'label' => lang('address'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[email]',
                    'label' => lang('email'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[tutor]',
                    'label' => lang('student_tutor'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[ci_tutor]',
                    'label' => lang('ci_tutor'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[estado]',
                    'label' => lang('status'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'estudiante[activo]',
                    'label' => lang('active'),
                    'rules' => 'trim|required'
                    )

                ),
            'mcargo'=>array(
                array(
                    'field' => 'cargo[descripcion]',
                    'label' => lang('description'),
                    'rules' => 'trim|required'
                    ),                
                ),
            'mcampo'=>array(
                array(
                    'field' => 'campo[descripcion]',
                    'label' => lang('description'),
                    'rules' => 'trim|required'
                    )               
                ),
            'musuariocurso'=>array(
                array(
                    'field' => 'usuario_curso[id_gestion]',
                    'label' => lang('gestion'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'usuario_curso[id_curso]',
                    'label' => lang('curso'),
                    'rules' => 'trim|required|max_length[20]'
                    ),
                array(
                    'field' => 'usuario_curso[id_usuario]',
                    'label' => lang('usuario'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'nivel[id_nivel]',
                    'label' => lang('nivel'),
                    'rules' => 'trim|required'
                    )              
                ),
            'mplanglobal'=>array(
                array(
                    'field' => 'gestion[id_gestion]',
                    'label' => lang('gestion'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'usuario[id_usuario]',
                    'label' => lang('usuario'),
                    'rules' => 'trim|required|max_length[20]'
                    ),
                array(
                    'field' => 'subarea[id_subarea]',
                    'label' => lang('subarea'),
                    'rules' => 'trim|required'
                    )            
                ),
            'rptaprobados'=>array(
                array(
                    'field' => 'gestion[id_gestion]',
                    'label' => lang('curricular_year'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'nivel[id_nivel]',
                    'label' => lang('level'),
                    'rules' => 'trim|required|max_length[20]'
                    ),
                array(
                    'field' => 'curso[id_curso]',
                    'label' => lang('course'),
                    'rules' => 'trim|required'
                    ) ,
                array(
                    'field' => 'subarea[id_subarea]',
                    'label' => lang('subject'),
                    'rules' => 'trim|required'
                    )            
                ),
            'rptmaterias'=>array(
                array(
                    'field' => 'gestion[id_gestion]',
                    'label' => lang('curricular_year'),
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'nivel[id_nivel]',
                    'label' => lang('level'),
                    'rules' => 'trim|required|max_length[20]'
                    ),
                array(
                    'field' => 'curso[id_curso]',
                    'label' => lang('course'),
                    'rules' => 'trim|required'
                    ) ,
                array(
                    'field' => 'bimestre[codigo]',
                    'label' => lang('bi_monthly'),
                    'rules' => 'trim|required'
                    )            
                ),
            'rptaprobadosprofesor'=>array(
                array(
                    'field' => 'gestion[id_gestion]',
                    'label' => lang('curricular_year'),
                    'rules' => 'trim|required'
                    ),                
                array(
                    'field' => 'curso[id_curso]',
                    'label' => lang('course'),
                    'rules' => 'trim|required'
                    ) ,    
                array(
                    'field' => 'subarea[id_subarea]',
                    'label' => lang('subject'),
                    'rules' => 'trim|required'
                    )             
                ),
            'rptmateriasprofesor'=>array(
             array(
                'field' => 'gestion[id_gestion]',
                'label' => lang('curricular_year'),
                'rules' => 'trim|required'
                ),                 
             array(
                'field' => 'curso[id_curso]',
                'label' => lang('course'),
                'rules' => 'trim|required'
                ) ,
             array(
                'field' => 'bimestre[codigo]',
                'label' => lang('bi_monthly'),
                'rules' => 'trim|required'
                )             
             ),
            'rptaprobadosasesor'=>array(
                array(
                    'field' => 'gestion[id_gestion]',
                    'label' => lang('curricular_year'),
                    'rules' => 'trim|required'
                    ),                
                array(
                    'field' => 'curso[id_curso]',
                    'label' => lang('course'),
                    'rules' => 'trim|required'
                    ) ,    
                array(
                    'field' => 'subarea[id_subarea]',
                    'label' => lang('subject'),
                    'rules' => 'trim|required'
                    )             
                ),
            'rptmateriasasesor'=>array(
                array(
                    'field' => 'gestion[id_gestion]',
                    'label' => lang('curricular_year'),
                    'rules' => 'trim|required'
                    ),                 
                array(
                    'field' => 'curso[id_curso]',
                    'label' => lang('course'),
                    'rules' => 'trim|required'
                    ) ,
                array(
                    'field' => 'bimestre[codigo]',
                    'label' => lang('bi_monthly'),
                    'rules' => 'trim|required'
                    )           
                )
            
            );

} 

}
