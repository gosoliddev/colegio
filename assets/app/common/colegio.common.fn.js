var config={
	dateTimeFormat:'DD-MM-YYYY HH:mm',
	dateFormat:'DD-MM-YYYY',
	maskDateTime:"99-99-9999 99:99",
	maskDate:"99-99-9999",
	placeholderDateTime:"__-__-____ __:__",
	placeholderDate:"__-__-____ __:__",

}
jQuery.validator.setDefaults({
	debug: true,
	lang: 'es',
	onclick: false,	
	ignoreTitle :true,    
	success: "valid",
	ignore: ".ignore",  
	errorClass :"has-error",
	validClass:"has-success", 
	errorElement: "em",   
	errorContainer: "#messageBox",     
	submitHandler: function(form) { 
		form.submit();
	},
	highlight: function(element, errorClass, validClass) {
		$(element).closest(".form-group").removeClass(validClass).addClass(errorClass);
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).closest(".form-group").removeClass(errorClass).addClass(validClass);
	},	      
	errorPlacement: function(error, element) {
		$(error).addClass(this.errorClass).addClass("control-label");
		$(element).parent().parent().append(error);
	},
	success: function (label) {
        label.closest('.form-group').removeClass('has-error'); // set success class to the control group
    },
    invalidHandler: function(form, validator) {
    	var errors = validator.numberOfInvalids();
    	if (errors) {
    		var element=$(validator.errorList[0].element);        		
        		console.log(element.closest(".form-group").find(".control-label").text()+": "+validator.errorList[0].message);  //Only show first invalid rule message!!!
        		validator.errorList[0].element.focus(); //Set Focus
        	}
        }
    });
jQuery.validator.addMethod("lettersonly", function(value, element) 
{
	return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "Profavor ingrese solo letras y espacios");
function getCookie(name) {
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
}
$(document).ready(function (){
	$("._maskDateTime").mask(config.maskDateTime, {placeholder: config.placeholderDateTime});
	$("._maxDateTime").datetimepicker({locale: 'es', format:config.dateTimeFormat , sideBySide: true, useCurrent: false});
	$("._maskDate").mask(config.maskDate, {placeholder: config.placeholderDate});	
	$("._fixDate").datetimepicker({locale: 'es', format: config.dateFormat, sideBySide: false, useCurrent: false});
	jQuery('[data-tooltip="true"]').tooltip({ placement: 'auto top', animated: 'fade', container: 'body', trigger: "hover" });
	$('[data-toggle="popover"]').popover(); 		
	$("li.dropdown ,ul.dropdown-menu>li,a.btn-toolbar").each(function (){
		var cookie=getCookie("rid");		
		if(Boolean($(this).attr('data-rol'))){			
			var rol=$(this).attr('data-rol').split(',');
			if(rol.indexOf(cookie)==-1){
				$(this).hide();
			}
			else
				$(this).show();	
		}
	})
	spinner();
});

function spinner(){
	if($(".preloader").is(":hidden")){
		$(".preloader").show();	
	}
	else{
		$(".preloader").delay(500).fadeOut("fast").delay(500);
	}
}
function _confirm(msg,callback){
	bootbox.confirm({
		title: lang['system_name'],
		message: msg,
		buttons: {
			cancel: {
				label: '<i class="glyphicon glyphicon-remove"></i> '+lang['cancel']
			},
			confirm: {
				label: '<i class="glyphicon glyphicon-ok"></i> '+lang['confirm']
			}
		},
		callback: function (result) {
			callback(result);
		}
	});
}
function _alert(msg){
	bootbox.alert({
		message: msg,
		size: 'small',
		className: 'bb-alternate-modal',
		backdrop: true
	});
}
function dialog(msg){
	bootbox.dialog({
		message: msg,
		closeButton: false
	});
}
function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}

function startTime() {
	var today = new Date();
	var y = today.getFullYear();
	var mo = (today.getMonth()+1);
	var d = today.getDate();
	var h = today.getHours();
	var m = today.getMinutes();
	var s = today.getSeconds();
    // add a zero in front of numbers<10
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('time').title =y+"/"+mo+"/"+d+" "+h + ":" + m;
    document.getElementById('time').innerText =y+"/"+mo+"/"+d+" "+h + ":" + m;
    t = setTimeout(function () {
    	startTime()
    }, 500);
}
startTime();
function toast(data){ 
	switch(data.status){
		case 0:{
			$.toaster({ priority : 'danger', title : lang['title_error'], message : data.message});
			break;
		}
		case 1:{
			$.toaster({ priority : 'success', title : lang['title_success'], message : data.message});			
			break;
		}
		case 2:{
			$.toaster({ priority : 'info', title : lang['title_info'], message :data.message});
			break;
		}
		case 3:{
			$.toaster({ priority : 'warning', title : lang['title_warning'], message : data.message});
			break;
		}
		

	}
}