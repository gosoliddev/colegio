 //validation
 (function ($){
    $("#criterio").validate({
        // Specify the validation rules
        rules: {
            "criterio[id_dimencion]": "required",
            "criterio[descripcion]": "required",
            "criterio[porcentaje_calificacion]": "required",
        }

    });

})(jQuery);
//list
$(function() {

    $("#tableCriterio").bootgrid({
        ajax: true,   
        url: "criterio/load",
        selection: true,
        padding:4,
        navigation:3,
        multiSelect: false,      
        rowSelect: true,
        keepSelection: true,
        sorting:true,
        columnSelection:false,
        rowCount:[20,30,60,100,-1],
        searchSettings: {
            delay: 100,
            characters: 3
        },        
        statusMappings: {
            4: "wrong"
        },
        labels: {
            noResults: lang['no_result_data'],
            
        },
        formatters: {
            "active": function(column, row)
            {
                return (row.activo=="1"?lang['yes']:lang['no']);
            }
        }
    }).on("load.rs.jquery.bootgrid", function (e)
    {
       spinner();
    }).on("loaded.rs.jquery.bootgrid", function (e)
    {       
      spinner();
    });
});
 //toolbar
 (function (toolbar,$){
     var list=$("#tableCriterio");
     this.toolbar=toolbar || {}
     this.toolbar.create=function(event){
         event.preventDefault();
         element=$(event.target);
         window.location.href=element.attr("href").replace('#','');
     };
     this.toolbar.edit=function(event){
         event.preventDefault();
         var rows=list.bootgrid("getSelectedRows");
         if(rows.length>0){
             var element=$(event.target);
             window.location.href=element.attr("href").replace('#','')+"/"+rows;
         } else{
             _alert(lang['alert_required_row']);
         }
     };
     this.toolbar.trash=function(event){
         event.preventDefault();
         var rows=list.bootgrid("getSelectedRows");
         if(rows.length>0){
             _confirm(lang['confirm_delete_message'],function (result){
                 if(result){
                     var element=$(event.target);
                     window.location.href=element.attr("href").replace('#','')+"/"+rows;
                 }
             });
         }
         else{
             _alert(lang['alert_required_row']);
         }
     };
     this.toolbar.home=function(event){
         event.preventDefault();
         var element=$(event.target);
         window.location.href=element.attr("href").replace('#','');

     };
 })({},jQuery)