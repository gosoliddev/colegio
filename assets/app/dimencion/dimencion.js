 //validation
 (function ($){
    $("#dimencion").validate({
        // Specify the validation rules
        rules: {
            "dimencion[descripcion]": "required"            
        },       

    });

})(jQuery);
//list
$(function() {

    $("#tableDimencion").bootgrid({
        ajax: true,   
        url: "dimencion/load",
        selection: true,
        padding:4,
        navigation:3,
        multiSelect: false,      
        rowSelect: true,
        keepSelection: true,
        sorting:true,
        columnSelection:false,
        rowCount:[20,30,60,100,-1],
        searchSettings: {
            delay: 100,
            characters: 3
        },        
        statusMappings: {
            4: "wrong"
        },
        labels: {
            noResults: lang['no_result_data'],
            
        },
        formatters: {
            "active": function(column, row)
            {
                return (row.activo=="1"?lang['yes']:lang['no']);
            }
        }
    }).on("load.rs.jquery.bootgrid", function (e)
    {
       spinner();
    }).on("loaded.rs.jquery.bootgrid", function (e)
    {       
      spinner();
    });
});