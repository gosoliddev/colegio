 //validation
 (function ($){
    $("#kardexcriterio").validate({
        // Specify the validation rules
        rules: {
            "kardexcriterio[id_criterio]": "required",
            "kardexcriterio[id_kardex_detalle]": "required",
            "kardexcriterio[bimestre]": "required",     	
        },       

    });

})(jQuery);
//list
$(function() {

    $("#tableKardexcriterio").bootgrid({
        ajax: true,   
        url: "kardexcriterio/load",
        selection: true,
        multiSelect: true,      
        rowSelect: true,
        keepSelection: true,
        sorting:true,
        searchSettings: {
            delay: 100,
            characters: 3
        },
        formatters: {
            "codigo_sie": function(column, row)
            {
                return "<a href=\"#\">" + column.codigo_sie + ": " + row.codigo_sie + "</a>";
            }
        }
    }).on("selected.rs.jquery.bootgrid", function(e, rows)
    {
        console.log($(e.target).bootgrid("getSelectedRows"));
        var rowIds = [];
        for (var i = 0; i < rows.length; i++)
        {
            rowIds.push(rows[i].codigo_sie);
        }
        e.addClass('has-error');
        alert("Select: " + rowIds.join(","));
    }).on("deselected.rs.jquery.bootgrid", function(e, rows)
    {
        var rowIds = [];
        for (var i = 0; i < rows.length; i++)
        {
            rowIds.push(rows[i].codigo_sie);
        }
        alert("Deselect: " + rowIds.join(","));
    });
});