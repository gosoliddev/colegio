 //validation
 (function ($){
    $("#kardex").validate({
        // Specify the validation rules
        rules: {
            "kardex[id_estudiante]": "required",
            "kardex[activo]": "required",      	
        },       

    });

})(jQuery);
//list
$(function() {

    $("#tableKardex").bootgrid({
        ajax: true,   
        url: "kardex/search",
        requestHandler: function (d)
        {
           d.searchPhrase=$('input[name="estudiante[rude]"]').val();
           return d;        
       },
       navigation:2,
       selection: true,
       multiSelect: true,      
       rowSelect: true,
       keepSelection: true,
       sorting:true,
       searchSettings: {
        delay: 100,
        characters: 3
    },
    formatters: {
        "codigo_sie": function(column, row)
        {
            return "<a href=\"#\">" + column.codigo_sie + ": " + row.codigo_sie + "</a>";
        }
    }
}).on("selected.rs.jquery.bootgrid", function(e, rows)
{
    console.log($(e.target).bootgrid("getSelectedRows"));
    var rowIds = [];
    for (var i = 0; i < rows.length; i++)
    {
        rowIds.push(rows[i].codigo_sie);
    }
    e.addClass('has-error');
    alert("Select: " + rowIds.join(","));
}).on("deselected.rs.jquery.bootgrid", function(e, rows)
{
    var rowIds = [];
    for (var i = 0; i < rows.length; i++)
    {
        rowIds.push(rows[i].codigo_sie);
    }
    alert("Deselect: " + rowIds.join(","));
});
});
$(document).ready(function (){
   var form=$("#formSearch");
   form.submit(function(event){
    event.preventDefault();
    $("#tableKardex").bootgrid('reload');
});
});