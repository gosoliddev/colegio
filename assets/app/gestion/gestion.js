 //validation
 (function ($){
    $("#gestion").validate({
        // Specify the validation rules
        rules: {
            "gestion[id_unidad_educativa]": "required",
            "gestion[inicio_gestion]": {
                required:true,
                validaGestion:true
            },
            "gestion[fin_gestion]": {
                required:true,
                validaGestion:true
            }
        },       

    });

})(jQuery);
//list
$(function() {

    $("#tableGestion").bootgrid({
        ajax: true,   
        url: "gestion/load",
        selection: true,
        padding:4,
        navigation:3,
        multiSelect: false,      
        rowSelect: true,
        keepSelection: true,
        sorting:true,
        columnSelection:false,
        rowCount:[20,30,60,100,-1],
        searchSettings: {
            delay: 100,
            characters: 3
        },        
        statusMappings: {
            4: "wrong"
        },
        labels: {
            noResults: lang['no_result_data'],
            
        },
        formatters: {
            "active": function(column, row)
            {
                return (row.activo=="1"?lang['yes']:lang['no']);
            }
        }
    }).on("load.rs.jquery.bootgrid", function(e, rows)
    {
        spinner();
    }).on("loaded.rs.jquery.bootgrid", function(e, rows)
    {
        spinner();
    });
});
//toolbar
(function (toolbar,$){
    var list=$("#tableGestion");
    this.toolbar=toolbar || {}   
    this.toolbar.create=function(event){
        event.preventDefault();
        element=$(event.target);
        window.location.href=element.attr("href").replace('#','');    
    };
    this.toolbar.edit=function(event){
        event.preventDefault();       
        var rows=list.bootgrid("getSelectedRows");          
        if(rows.length>0){       
            var element=$(event.target);
            window.location.href=element.attr("href").replace('#','')+"/"+rows;    
        } else{
           _alert(lang['alert_required_row']);
       }
   };
   this.toolbar.trash=function(event){
    event.preventDefault();      
    var rows=list.bootgrid("getSelectedRows");       
    if(rows.length>0){  
        _confirm(lang['confirm_delete_message'],function (result){
            if(result){
                var element=$(event.target);
                window.location.href=element.attr("href").replace('#','')+"/"+rows;    
            }
        }); 
    }
    else{
       _alert(lang['alert_required_row']);
   }
};
this.toolbar.home=function(event){
    event.preventDefault();     
    var element=$(event.target);
    window.location.href=element.attr("href").replace('#','');    

};
})({},jQuery);

$(document).ready(function (){
    $.validator.addMethod("validaGestion",function (value, element)
    {
        var band = ''; 
        $.ajax(
        {
            url: "gestion/validagestion",
            type: "post",
            dataType: 'json',      
            async:false,  
            data: {                        
                "mgestion[inicio_gestion]": function() {
                    return ($( "input[name='gestion[inicio_gestion]']" ).val()!=""?$( "input[name='gestion[inicio_gestion]']" ).val():"");
                },
                "mgestion[fin_gestion]": function() {
                    return ($( "input[name='gestion[fin_gestion]']" ).val()!=""?$( "input[name='gestion[fin_gestion]']" ).val():"");
                },
                "mgestion[id_unidad_educativa]": function() {
                    return ($( "input[name='gestion[id_unidad_educativa]']" ).val()!=""?$( "input[name='gestion[id_unidad_educativa]']" ).val():"");
                }
            }   , 
            success: function(data)
            {            
                if (JSON.parse(data) != true)
                {

                    band =false;
                }
                else
                {
                    band =true;             
                }
            },
            error: function(xhr, textStatus, errorThrown)
            {            
                band =true;
            }
        });
        return band;
    } , 'La gesti&oacute;n ya se encuentra registrada');
    $('#dateStart').datetimepicker({
        locale: 'es', 
        format: config.dateFormat, 
        sideBySide: false,
        useCurrent:false,    
        daysOfWeekDisabled: [0,6]     
    });
    $('#dateEnd').datetimepicker({
        locale: 'es', 
        format: config.dateFormat, 
        sideBySide: false,
        useCurrent: false, 
        daysOfWeekDisabled: [0,6]  
    });

    $("#dateStart").on("dp.change", function (e) {
        $('#dateEnd').data("DateTimePicker").minDate(e.date);
    });
    $("#dateEnd").on("dp.change", function (e) {
        $('#dateStart').data("DateTimePicker").maxDate(e.date);
    });
});