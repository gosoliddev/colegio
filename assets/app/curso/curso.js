 //validation
 (function ($){
    $("#curso").validate({
        // Specify the validation rules
         onkeyup: false,
        rules: {
            "curso[id_nivel]": {
                required:true,
                validarCurso:true
            },
            "curso[descripcion]": "required",
            "curso[paralelo]":{
                required:true,
                validarCurso:true
            },
            "curso[grado]": {
                required:true,
                validarCurso:true
            },                 
        }            
    });

})(jQuery);
//list
$(function() {

    $("#tableCurso").bootgrid({
        ajax:true,   
        url: "curso/load",
        selection: true,
        padding:4,
        navigation:3,
        multiSelect: false,      
        rowSelect: true,
        keepSelection: true,
        sorting:true,
        columnSelection:false,
        rowCount:[20,30,60,100,-1],
        searchSettings: {
            delay: 100,
            characters: 3
        },
        statusMappings: {
            4: "wrong"
        },
        labels: {
            noResults: lang['no_result_data'],
            
        },
        formatters: {
            "active": function(column, row)
            {
                return (row.activo=="1"?lang['yes']:lang['no']);
            }
        }
    }).on("load.rs.jquery.bootgrid", function (e)
    {
     spinner();
 }).on("loaded.rs.jquery.bootgrid", function (e)
 {       
  spinner();
});;
});
//toolbar
(function (toolbar,$){
    var list=$("#tableCurso");
    this.toolbar=toolbar || {}   
    this.toolbar.create=function(event){
        event.preventDefault();
        element=$(event.target);
        window.location.href=element.attr("href").replace('#','');    
    };
    this.toolbar.edit=function(event){
        event.preventDefault();       
        var rows=list.bootgrid("getSelectedRows");          
        if(rows.length>0){       
            var element=$(event.target);
            window.location.href=element.attr("href").replace('#','')+"/"+rows;    
        } else{
         _alert(lang['alert_required_row']);
     }
 };
 this.toolbar.trash=function(event){
    event.preventDefault();      
    var rows=list.bootgrid("getSelectedRows");       
    if(rows.length>0){  
        _confirm(lang['confirm_delete_message'],function (result){
            if(result){
                var element=$(event.target);
                window.location.href=element.attr("href").replace('#','')+"/"+rows;    
            }
        }); 
    }
    else{
     _alert(lang['alert_required_row']);
 }
};
this.toolbar.home=function(event){
    event.preventDefault();     
    var element=$(event.target);
    window.location.href=element.attr("href").replace('#','');    

};
})({},jQuery)
$(document).ready(function (){
    var form=$("#curso");
    form.find("select[name='curso[grado]'] , select[name='curso[paralelo]'] , select[name='curso[id_nivel]'] ").change(function (){
        var txt_grado=form.find("select[name='curso[grado]'] option:selected");
        var txt_paralelo=form.find("select[name='curso[paralelo]'] option:selected");
        var txt_nivel=form.find("select[name='curso[id_nivel]'] option:selected");
        txt_nivel=txt_nivel.val()!=""?txt_nivel.text():'';
        txt_paralelo=txt_paralelo.val()!=""?txt_paralelo.text():'';
        txt_grado=txt_grado.val()!=""?txt_grado.text():'';

        form.find("input[name='curso[descripcion]']").val(txt_grado+" "+txt_paralelo+" ("+txt_nivel+")")
    });
});
$.validator.addMethod("validarCurso",function (value, element)
{
    var band = ''; 
    $.ajax(
    {
        url: "curso/checkduplicate",
        type: "post",
        dataType: 'json',      
        async:false,  
        data: {                        
            "curso[grado]": function() {
                return ($( "select[name='curso[grado]']" ).val()!=""?$( "select[name='curso[grado]']" ).val():0);
            },
            "curso[paralelo]": function() {
                return ($( "select[name='curso[paralelo]']" ).val()!=""?$( "select[name='curso[paralelo]']" ).val():0);
            },
            "curso[id_nivel]": function() {
                return ($( "select[name='curso[id_nivel]']" ).val()!=""?$( "select[name='curso[id_nivel]']" ).val():0);
            }
        }   , 
        success: function(data)
        {            
            if (JSON.parse(data) != true)
            {
              
                band =false;
            }
            else
            {
                band =true;             
            }
        },
        error: function(xhr, textStatus, errorThrown)
        {            
            band =true;
        }
    });
    return band;
} , 'Este curso ya esta registrado');
