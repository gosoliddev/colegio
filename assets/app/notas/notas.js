 //validation
 (function ($){
    $("#notas").validate({
        // Specify the validation rules
        rules: {
            "notas[id_kardex_criterio]": "required",
            "notas[nota_bimestral]": "required",
            "notas[nota]": "required",      	
        },       

    });

})(jQuery);
//list
$(function() {

    $("#tableNotas").bootgrid({
        ajax: true,   
        url: "notas/load",
        selection: true,
        multiSelect: true,      
        rowSelect: true,
        keepSelection: true,
        sorting:true,
        searchSettings: {
            delay: 100,
            characters: 3
        },
        formatters: {
            "codigo_sie": function(column, row)
            {
                return "<a href=\"#\">" + column.codigo_sie + ": " + row.codigo_sie + "</a>";
            }
        }
    }).on("selected.rs.jquery.bootgrid", function(e, rows)
    {
        console.log($(e.target).bootgrid("getSelectedRows"));
        var rowIds = [];
        for (var i = 0; i < rows.length; i++)
        {
            rowIds.push(rows[i].codigo_sie);
        }
        e.addClass('has-error');
        alert("Select: " + rowIds.join(","));
    }).on("deselected.rs.jquery.bootgrid", function(e, rows)
    {
        var rowIds = [];
        for (var i = 0; i < rows.length; i++)
        {
            rowIds.push(rows[i].codigo_sie);
        }
        alert("Deselect: " + rowIds.join(","));
    });
});