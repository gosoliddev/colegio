$(document).ready(function (){

	var form=$("#form-filter");
	var formCalificacion=$("#form-calificacion");
	formCalificacion.validate({
		errorPlacement: function(error,element) {
			return true;
		}
	});
	form.find("select[name='usuario_curso[id_usuario_curso]']").change(function (){
		form.submit();
	});
	form.find("select[name='subarea[id_subarea]']").change(function (){
		form.submit();
	});
	form.find("select[name='bimestre[codigo]']").change(function (){
		form.submit();
	});
	$.each($(".nota ,.autoevaluacion"),function (index,item){
		$(item).rules('add', {		
			required:true,	
			number:true,
			range: [0, 100]
		});
	});
	$(".nota ,.autoevaluacion").keyup(function(evt) {
		var _self=evt.currentTarget;

		var row=_self.closest("tr");		
		var inputs=$(row).find("input[type='text']").not('.promedio');
		var promedio=$(row).find('.promedio');		
		var sumatoria=0;
		$.each(inputs,function (index,item){
			var nota=parseInt(item.value);
			if(!isNaN(nota))
				sumatoria+=nota;

		});		
		promedio.val(Math.round((sumatoria/inputs.length)*(parseInt($(inputs[0]).attr('data-porcentaje'))/100)));		
		var rowCalificacion=$(row).closest(".rowCalificacion");
		var promedios=$(rowCalificacion).find('.promedio');
		var sumatoriaPromedio=0;
		$.each(promedios,function (index,item){
			var promedioVal=parseInt(item.value);
			if(!isNaN(promedioVal))
				sumatoriaPromedio+=promedioVal;
		})
		$(rowCalificacion).find(".nota-bimestral").val(sumatoriaPromedio);
	});		
	$(".nota ,.autoevaluacion").each(function(){
		$(this).keyup();
	});	
	$("button[name='btnValoracion']").click(function (evt){		
		evt.preventDefault();
		var _self=evt.currentTarget;
		var tableValoraciones=$("#tableValoraciones");
		var formFilter=$("#form-filter");		
		var row=_self.closest("tr");
		var rowCalificacion=$(row).closest(".rowCalificacion");
		$.ajax({    
			type: 'POST',  
			url: 'calificacion/obtenerValoraciones',  
			data: {                        
				"usuario_curso[id_usuario_curso]": formFilter.find("select[name='usuario_curso[id_usuario_curso]']").val(),
				"promedio": $(rowCalificacion).find(".nota-bimestral").val()                
			}, 
			dataType:'json',			
			success:function(data){          
				var body=tableValoraciones.find("tbody");
				if(data.valoracion_list.length>0){		
					var row=[];
					$.each(data.valoracion_list,function (index,item){
						row.push({id:item.id_observacion,text:item.observacion});
					});
					$("#obs").select2({
						width: '100%',
						data:row,
						placeholder: {
							id: '',
							text:""
						},
						allowClear: true,						 						
						language: "es"
					});
				} 				
				$("#modal-cualitativo").find("input[name='nota-bimestral']").val($(rowCalificacion).find(".nota-bimestral").val());		
				$("#modal-cualitativo").find("input[name='rude_estudiante']").val($(rowCalificacion).find(".rude").text());		
				$("#modal-cualitativo").find("input[name='bimestre']").val($("select[name='bimestre[codigo]'] option:selected").text());		
				$("#modal-cualitativo").modal("show")     		
				$("#modal-cualitativo").find("button[name='btnSave']").off("click").click(function (e){
					e.preventDefault();
					var obs=$("#modal-cualitativo").find("#obs").select2('data');
					var observacion="";
					$.each(obs,function (){
						observacion+=this.text+"\n";
					})					
					observacion+=$("#modal-cualitativo").find("textarea[name='adicionalObs']").val();
					$(rowCalificacion).find("textarea[name='cualitativo["+$(rowCalificacion).find(".rude").text()+"]']").html(observacion);
					$("#modal-cualitativo").modal("hide");
				}); 
			},
			error: function(data) {         
				toast(data);
			}
		}); 
	});
});
function loop(){
	var rows=$(".rowCalificacion")
	$.each(rows,function (index , item1){
		var cells=$(item1).find(".table-vertical");
		$.each(cells,function (index , item2){		
			var inputs=$(item2).find("input");	
			$.each(inputs,function (index , item){			
				console.log(item.value);
			});
		});
	});
}
(function (toolbar,$){
	var form=$("#form-calificacion");
	this.toolbar=toolbar || {}   
	this.toolbar.save=function(event){
		event.preventDefault();
		if(form.valid()){
			form.submit();
		}
	};
})({},jQuery);
/*
var row="";			
					$.each(data.valoracion_list,function (index,item){
						row+="<tr>";
						row+="<td>";
						row+=item.id_observacion;
						row+="</td>";
						row+="<td>";
						row+=item.observacion;
						row+="</td>";
						row+="<td>";
						row+=item.min_promedio;
						row+="</td>";
						row+="<td>";
						row+=item.max_promedio;
						row+="</td>";
						row+="</tr>";						
					});
					body.append(row);
					$("#tableValoraciones").bootgrid({
						selection: true,
						padding:4,
						navigation:3,
						multiSelect: true,      
						rowSelect: true,
						keepSelection: true,
						sorting:true,
						columnSelection:false,
						rowCount:[5,10,15,20,-1],				
						labels: {
							noResults: lang['no_result_data'],

						}
					});	
*/