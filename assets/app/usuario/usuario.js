 $(document).ready(function (){
    $("select[name='cargo[id_cargo]']").select2({
        width:'100%'
    });
});
 //validation
 (function ($){
    $("#form-usuario").validate({
        // Specify the validation rules
        rules: {
        	"usuario[nombres]": {
                required:true,
                lettersonly:true
            },
            "usuario[apellido_paterno]": {
                required:true,
                lettersonly:true
            },
            "usuario[apellido_materno]": {
                required:true,
                lettersonly:true
            },
            "usuario[ci]": {
                required:true,
                number:true,
                maxlength:7
            },
            "usuario[ci_expedido]": "required",
            "usuario[genero]": "required",
            "usuario[telefono]": {
                required:true,
                number:true,
                maxlength:7
            }, 
            "usuario[celular]": {                
                number:true,
                maxlength:8
            }, 
            "usuario[login]" :"required",
            "usuario[clave]" :"required",            
            "usuario[email]":{
                required:true,
                email:true
            },
            "usuario[activo]":{
                required:false                
            },
            "cargo[id_cargo]":{
                required:true
            }
        },       

    });

})(jQuery);
//list
$(function() {

    $("#tableUsuario").bootgrid({
        ajax: true,   
        url: "usuario/load",        
        selection: true,
        padding:4,
        navigation:3,
        multiSelect: false,      
        rowSelect: true,
        keepSelection: true,
        sorting:true,
        columnSelection:false,
        rowCount:[20,30,60,100,-1],
        searchSettings: {
            delay: 100,
            characters: 3
        },        
        statusMappings: {
            4: "wrong"
        },
        labels: {
            noResults: lang['no_result_data'],
            
        },
        formatters: {
            "active": function(column, row)
            {
                return (row.activo=='1'?lang['yes']:lang['no']);
            }
        }
    }).on("load.rs.jquery.bootgrid", function (e)
    {
     spinner();
 }).on("loaded.rs.jquery.bootgrid", function (e)
 {       
  spinner();
});
});
//toolbar
(function (toolbar,$){
    var list=$("#tableUsuario");
    this.toolbar=toolbar || {}   
    this.toolbar.create=function(event){
        event.preventDefault();
        element=$(event.target);
        window.location.href=element.attr("href").replace('#','');    
    };
    this.toolbar.edit=function(event){
        event.preventDefault();       
        var rows=list.bootgrid("getSelectedRows");          
        if(rows.length>0){       
            var element=$(event.target);
            window.location.href=element.attr("href").replace('#','')+"/"+rows;    
        } else{
         _alert(lang['alert_required_row']);
     }
 };
 this.toolbar.trash=function(event){
    event.preventDefault();      
    var rows=list.bootgrid("getSelectedRows");       
    if(rows.length>0){  
        _confirm(lang['confirm_delete_message'],function (result){
            if(result){
                var element=$(event.target);
                window.location.href=element.attr("href").replace('#','')+"/"+rows;    
            }
        }); 
    }
    else{
     _alert(lang['alert_required_row']);
 }
};
this.toolbar.home=function(event){
    event.preventDefault();     
    var element=$(event.target);
    window.location.href=element.attr("href").replace('#','');    

};
})({},jQuery)
$(document).ready(function (){
    $('#dateBorn').datetimepicker({
        locale: 'es', 
        format: config.dateFormat, 
        sideBySide: false,
        useCurrent:false,    
        daysOfWeekDisabled: [0,6]     
    });
})