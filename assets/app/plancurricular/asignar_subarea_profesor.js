 //validation
 (function ($){


 })(jQuery);

 (function ($){      
    var table=$("#tableSubareaUsuario");    
    $("select[name='usuario']").change(function (){
        getUsuarioSubare();
    });
})(jQuery);
//toolbar
(function (toolbar,$){
   var table=$("#tableSubareaUsuario");
   this.toolbar=toolbar || {}   
   this.toolbar.save=function(event){
    event.preventDefault();        
    var cbx_list=table.find("input[name='usuario_subarea[][activo]']");
    var values = new Array();
    $.each(cbx_list, function() {
        var tr=$(this).parent().parent();
        var subarea_curso={
            id_subarea:tr.find('input[name="usuario_subarea[][id_subarea]"]').val(),
            id_usuario:$("select[name='usuario']").val(),
            id_gestion:$("#gestion").val(),
            id_usuario_subarea:tr.find('input[name="usuario_subarea[][id_usuario_subarea]"]').val(),
            activo:($(this).prop("checked")?1:0)
        }; 
        values.push(subarea_curso);         
    });     
    $.post("usuariosubarea/setsubareausuario",{
        "subarea_usuario": values           
    },
    function(data, status){     
        toast({status:1,message:lang['success_message']}) ;
        getUsuarioSubare();                   
    },"json");      
};     
})({},jQuery)

function getUsuarioSubare(){
    var table=$("#tableSubareaUsuario");
    $.post("usuariosubarea/findbyIdUsuario",{
        "id_usuario":$("select[name='usuario']").val(),                
        "id_gestion":$("#gestion").val()            
    },
    function(data, status){                 
        var rows=table.find('input[name="usuario_subarea[][id_subarea]"]');
        $.each(rows,function (index,cbx){   
            if(Boolean(data.subareausuario_list) && data.subareausuario_list.length>0){
                $.each(data.subareausuario_list,function (index,item){                
                    if(parseInt(cbx.value)==parseInt(item.id_subarea)){
                        $(cbx).siblings('input[name="usuario_subarea[][activo]"]').prop('checked',(item.active==1?true:false));
                        $(cbx).siblings('input[name="usuario_subarea[][id_usuario_subarea]"]').val(item.id_usuario_subarea);
                    }   
                });             
            }else
            {
                $(cbx).siblings('input[name="usuario_subarea[][activo]"]').prop('checked',false); 
                $(cbx).siblings('input[name="usuario_subarea[][id_usuario_subarea]"]').val('');
            }

        });


    },"json");  
}