 //validation
 (function ($){
     $("#asesorcurso").validate({
        // Specify the validation rules
        rules: {
            "usuario_curso[id_gestion]": "required",
            "usuario_curso": "required",
            "usuario_curso[id_curso]":{
                required:true                
            },
            "nivel[id_nivel]": "required",
            "usuario_curso[id_usuario]":{
                required:true,
                validarAsignacion:true
            }
        },       

    });

 })(jQuery);

//toolbar
(function (toolbar,$){
 var table=$("#tableSubareaUsuario");

})({},jQuery)
$(document).ready(function (){
    $("select[name='usuario_curso[id_gestion]']").change(function (){     
        $("select[name='usuario_curso[id_curso]'],select[name='nivel[id_nivel]']").val('').change();
    });
    $("select[name='nivel[id_nivel]']").change(function (){
        $.post("curso/findByIdNivel",{
            "id_nivel":$("select[name='nivel[id_nivel]']").val() ,                      
        },
        function(data, status){     
            var select=$('select[name="usuario_curso[id_curso]"]');
            var option=$("<option />",{value:'',text:lang['select']});
            select.empty();
            select.append(option);
            $.each(data.curso_list,function (index,item){
                var option=$("<option />",{value:item.id_curso,text:item.descripcion});
                select.append(option);
            });
        },"json");   
    });
    $("select[name='usuario_curso[id_usuario]']").change(function (){
        $.post("plancurricular/obtenerCursoProfesor",{
            "usuario_curso[id_usuario]":$("select[name='usuario_curso[id_usuario]']").val() ,                      
            "usuario_curso[id_curso]":$("select[name='usuario_curso[id_curso]']").val(),
            "usuario_curso[id_gestion]":$("[name='usuario_curso[id_gestion]']").val()
        },
        function(data, status){     
         var table=$("#tableCurso tbody");
         if(Boolean(data) && data.length>0){
            table.html('');
             $.each(data,function (index,item){            
                var tr="<tr>";                                
                tr+="<td>"+item.descripcion+"</td>";
                tr+="<td>"+item.paralelo+"</td>";                
                tr+="<td>"+item.grado+"</td>";
                tr+="<td>"+(item.asesor!=null && item.asesor==1?lang['yes']:lang['no'])+"</td>";                
                tr+="<td>"+item.fecha_actualizacion+"</td>";             
                tr+="</tr>";
                table.append(tr);
            });
         }
         else{
            table.html('');
         }
     },"json");           
    });    
})
$.validator.addMethod("validarAsignacion",function (value, element)
{
    var band = ''; 
    $.ajax(
    {
        url: "plancurricular/validarAsignacionCursoProfesor",
        type: "post",
        dataType: 'json',      
        async:false,  
        data: {                        
         "usuario_curso[id_usuario]":$("select[name='usuario_curso[id_usuario]']").val() ,                      
         "usuario_curso[id_curso]":$("select[name='usuario_curso[id_curso]']").val(),
         "usuario_curso[id_gestion]":$("[name='usuario_curso[id_gestion]']").val()
     }, 
     success: function(data)
     {            
        if (Boolean(data) && data.length>0)
        {

            band =false;
        }
        else
        {
            band =true;             
        }
    },
    error: function(xhr, textStatus, errorThrown)
    {            
        band =true;
    }
});
    return band;
} , lang['msg_validate_assign_teacher_to_course']);