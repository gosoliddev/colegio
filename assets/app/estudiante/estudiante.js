 //validation
 (function ($){


 })(jQuery);

 //list
 $(function() {
   var form=$("#formSearch");
   $("#tableKardex").bootgrid({
    ajax: true,   
    url: "kardex/search",
    requestHandler: function (d)
    {
     d.searchPhrase=form.find('input[name="estudiante[rude]"]').val();
     return d;        
   },  
   selection: true,
   padding:4,
   navigation:3,
   multiSelect: false,      
   rowSelect: true,
   keepSelection: true,
   sorting:true,
   columnSelection:false,
   rowCount:[20,30,60,100,-1],
   searchSettings: {
    delay: 100,
    characters: 3
  },
  statusMappings: {
    4: "wrong"
  },
  labels: {
    noResults: lang['no_result_data'],

  },
  formatters: {
    "active": function(column, row)
    {
      return (row.activo=="1"?lang['yes']:lang['no']);
    }
  }
}).on("load.rs.jquery.bootgrid", function (e)
{
 spinner();
}).on("loaded.rs.jquery.bootgrid", function (e)
{       
  spinner();
});
});

 $(document).ready(function (){
  $("button[name='btnNewStudent']").css("display","none");
  $("button[name='btnEditStudent']").css("display","none");
  $("button[name='btnNewInscription']").css("display","none");
  var form=$("#formSearch");
  form.submit(function(event){    
    $("#formNewEstudiante").find("input:hidden").val('');    
    $("#tableKardex").bootgrid('reload');
    $.post("estudiante/findByRude",{
      "rude":form.find("input[name='estudiante[rude]']").val() ,                      
    },
    function(data, status){     
      var estudiante=data.estudiante;
      var panelInfo=$("#panel-info");
      if(Boolean(estudiante)){
       $("button[name='btnNewStudent']").css("display","none");
       $("button[name='btnEditStudent']").css("display","block");     
       $("button[name='btnNewInscription']").css("display","block");    
       panelInfo.find("input[name='estudiante[id_estudiante]']").val(estudiante.id_estudiante);
       panelInfo.find("#ci").html(estudiante.ci);
       panelInfo.find("#ci_expedido").html(estudiante.ci_expedido);
       panelInfo.find("#complete_name").html(estudiante.apellido_paterno+" "+estudiante.apellido_materno+" "+estudiante.nombres);
       panelInfo.find("#student_rude").html(estudiante.rude);
       panelInfo.find("#previous_sie").html(estudiante.sie_anterior);
       panelInfo.find("#telephone").html(estudiante.telefono);
       panelInfo.find("#cell_phone").html(estudiante.celular);
       panelInfo.find("#address").html(estudiante.direccion);
       panelInfo.find("#gender").html(estudiante.genero);
       panelInfo.find("#status").html(estudiante.estado);      
       panelInfo.find("#status").html(estudiante.estado==1?lang["active"]:lang["inactive"]);
     }
     else{      
      $("button[name='btnNewStudent']").css("display","block");
      $("button[name='btnEditStudent']").css("display","none");
      $("button[name='btnNewInscription']").css("display","none");
      panelInfo.find("input[name='estudiante[id_estudiante]']").val('');
      panelInfo.find("#ci").html('');
      panelInfo.find("#ci_expedido").html('');
      panelInfo.find("#complete_name").html('');
      panelInfo.find("#student_rude").html('');
      panelInfo.find("#previous_sie").html('');
      panelInfo.find("#telephone").html('');
      panelInfo.find("#cell_phone").html('');
      panelInfo.find("#address").html('');
      panelInfo.find("#gender").html('');
      panelInfo.find("#status").html('');
    }
  },"json"); 
    return false;     
  });
  $("button[name='btnNewStudent']").click(function (event){
    event.preventDefault();    
    $("#modal-new_estudent").find("input[name='estudiante[rude]']").val($("input[name='estudiante[rude]']").val());
    $("#modal-new_estudent").modal("show");
  });
  $("#formNewEstudiante").validate({
    rules:{
      "estudiante[ci]":{
        required:true,
        number:true,
        maxlength:7
      },
      
      "estudiante[sie_anterior]":{
        required:true,
        number:true,
        maxlength:8 
      },
      "estudiante[apellido_paterno]":{
        required:true,
        lettersonly:true
      },
      "estudiante[apellido_materno]":{
        required:true,
        lettersonly:true
      },
      "estudiante[nombres]":{
        required:true,
        lettersonly:true
      },
      "estudiante[telefono]":{
        required:true,
        number:true,
        maxlength:7
      },
      "estudiante[celular]":{
        required:true,
        number:true,
        maxlength:8
      },
      "estudiante[genero]":{
        required:true
      },
      "estudiante[ci_expedido]":{
        required:true
      },
      "estudiante[fecha_nacimiento]":{
        required:true,        
      },
      "estudiante[direccion]":{
        required:true,        
      },
      "estudiante[rude]":{
        required:true,        
        maxlength:16,
        letternumerber:true
      },
      "estudiante[email]":{
        required:true,
        email:true        
      },

    },
    messages:{
      "estudiante[rude]":{
        letternumerber:"Porfavor solo letras y numeros"
      }
    }
    ,submitHandler: function(form) { 
      //form.submit();
    }
  });
  $("#formNewEstudiante").submit(function (e){          
    $.ajax({    
      type: 'POST',  
      url: 'estudiante/new_estudiante',  
      data: $('#formNewEstudiante').serialize(), 
      dataType:'json',
      beforeSend:function(xhr, settings){     
      },
      success:function(data){              
        if(data.status==1){
          $("#modal-new_estudent").modal("hide");
          $("#formNewEstudiante").trigger("reset");
          $("#formNewEstudiante").find("input:hidden").val('');
          $("#formSearch").submit();
        }        
        toast(data);
      },
      error: function(data) {         
       console.log(data);
     }
   });
    return false; 
  });
  //inscripcion
  $("button[name='btnNewInscription']").click(function (e){
    e.preventDefault();
    $("#modal-inscripcion").modal("show");
    var rude=$("#formSearch").find("input[name='estudiante[rude]']").val();
    $("#modal-inscripcion").find("input[name='estudiante[rude]']").val(rude);
  });
  $("#form-add-inscripcion").submit(function (e){
    e.preventDefault();        
    $.ajax({    
      type: 'POST',  
      url: 'kardexdetalle/agregarInscripcion',  
      data: $('#form-add-inscripcion').serialize(), 
      dataType:'json',
      beforeSend:function(xhr, settings){     
      },
      success:function(data){                      
        toast(data);
        $("#formSearch").submit();
        $("#modal-inscripcion").modal("hide");
        $("#formSearch").trigger('reset');
      },
      error: function(data) {         
        toast(data);
      }
    }); 
  });
  $("button[name='btnEditStudent']").click(function (e){
    e.preventDefault();
    $.post("estudiante/encontrarPorid",{
      "id_estudiante": $("#panel-info").find("input[name='estudiante[id_estudiante]']").val(),                      
    },
    function(data, status){     
      var estudiante=data.estudiante;
      if(Boolean(estudiante)){
        form=$("#formNewEstudiante");        
        form.find("input[name='estudiante[id_estudiante]']").val(estudiante.id_estudiante);
        form.find("input[name='estudiante[rude]']").val(estudiante.rude);
        form.find("input[name='estudiante[sie_anterior]']").val(estudiante.sie_anterior);
        form.find("input[name='estudiante[ci]']").val(estudiante.ci);
        form.find("select[name='estudiante[ci_expedido]']").val(estudiante.ci_expedido);
        form.find("input[name='estudiante[apellido_paterno]']").val(estudiante.apellido_paterno);      
        form.find("input[name='estudiante[apellido_materno]']").val(estudiante.apellido_materno);
        form.find("input[name='estudiante[nombres]']").val(estudiante.nombres);
        form.find("input[name='estudiante[fecha_nacimiento]']").val(estudiante.fecha_nacimiento);
        form.find("select[name='estudiante[genero]']").val(estudiante.genero);
        form.find("input[name='estudiante[email]']").val(estudiante.email);
        form.find("input[name='estudiante[celular]']").val(estudiante.celular);
        form.find("input[name='estudiante[telefono]']").val(estudiante.telefono);
        form.find("input[name='estudiante[direccion]']").val(estudiante.direccion);        
        $("#modal-new_estudent").modal("show");
      }else{
        $("#modal-new_estudent").modal("hide");
      }

    },"json");      
  });
});
 
 $.validator.addMethod("letternumerber", function(value, element, param) 
 {
  return value.match(new RegExp("^[A-za-z0-9_\-\s]+$"));
}); 