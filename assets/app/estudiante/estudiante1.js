 //validation
 (function ($){
  $("#estudiante").validate({
        // Specify the validation rules
        rules: {
        	"estudiante[sie_anterior]": "required",
          "estudiante[nombres]": "required",
          "estudiante[apellido_paterno]": "required",
          "estudiante[apellido_materno]": "required",
            "estudiante[ci]": "required",
          "estudiante[ci_expedido]": "required",  
          "estudiante[fecha_nacimiento]": "required",
          "estudiante[celular]": "required",
          "estudiante[telefono]": "required",
          "estudiante[genero]": "required",
          "estudiante[direccion]": "required",
          "estudiante[email]": "required",
          "estudiante[tutor]": "required",
          "estudiante[ci_tutor]": "required",
          "estudiante[estado]": "required",
          "estudiante[activo]": "required",      	
        },       

      });

})(jQuery);

 //list
 $(function() {

  $("#tableKardex").bootgrid({
    ajax: true,   
    url: "kardex/search",
    requestHandler: function (d)
    {
     d.searchPhrase=$('input[name="estudiante[rude]"]').val();
     return d;        
   },  
   selection: true,
   padding:4,
   navigation:3,
   multiSelect: false,      
   rowSelect: true,
   keepSelection: true,
   sorting:true,
   columnSelection:false,
   rowCount:[20,30,60,100,-1],
   searchSettings: {
    delay: 100,
    characters: 3
  },
  statusMappings: {
    4: "wrong"
  },
  labels: {
    noResults: lang['no_result_data'],

  },
  formatters: {
    "active": function(column, row)
    {
      return (row.activo=="1"?lang['yes']:lang['no']);
    }
  }
}).on("load.rs.jquery.bootgrid", function (e)
{
 spinner();
}).on("loaded.rs.jquery.bootgrid", function (e)
{       
  spinner();
});
});

 $(document).ready(function (){
   var form=$("#formSearch");
   form.submit(function(event){
    event.preventDefault();
    $("#tableKardex").bootgrid('reload');
    $.post("estudiante/findByRude",{
      "rude":$("input[name='estudiante[rude]']").val() ,                      
    },
    function(data, status){     
      var estudiante=data.estudiante;
      if(Boolean(estudiante)){
        $("#ci").html(estudiante.ci);
        $("#ci_expedido").html(estudiante.ci_expedido);
        $("#complete_name").html(estudiante.apellido_paterno+" "+estudiante.apellido_materno+" "+estudiante.nombres);
        $("#student_rude").html(estudiante.rude);
        $("#previous_sie").html(estudiante.sie_anterior);
        $("#telephone").html(estudiante.telefono);
        $("#cell_phone").html(estudiante.celular);
        $("#address").html(estudiante.direccion);
        $("#gender").html(estudiante.genero);
        $("#status").html(estudiante.estado);      
      }
      else{
        $("#ci").html('');
        $("#ci_expedido").html('');
        $("#complete_name").html('');
        $("#student_rude").html('');
        $("#previous_sie").html('');
        $("#telephone").html('');
        $("#cell_phone").html('');
        $("#address").html('');
        $("#gender").html('');
        $("#status").html('');
      }
    },"json");      
  });
   $("button[name='btnNewEstudent']").click(function (event){
    event.preventDefault();
    $("#modal-new_estudent").modal("show");
  });
   $("#formNewEstudiante").validate({
    rules:{
      "estudiante[ci]":{
        required:true
      },
      "estudiante[rude]":{
        required:true
      },
      "estudiante[sie_anterior]":{
        required:true
      },
      "estudiante[apellido_paterno]":{
        required:true
      },
      "estudiante[apellido_materno]":{
        required:true
      },
      "estudiante[nombres]":{
        required:true
      },
      "estudiante[telefono]":{
        required:true
      },
      "estudiante[celular]":{
        required:true
      },
      "estudiante[genero]":{
        required:true
      },
      "estudiante[ci_expedido]":{
        required:true
      },
      "estudiante[fecha_nacimiento]":{
        required:true,        
      },
      "estudiante[direccion]":{
        required:true,        
      },
      "estudiante[rude]":{
        required:true
      },
      "estudiante[email]":{
        required:true,
        email:true        
      },
    }
  });
   $("#formNewEstudiante").submit(function (event){
    event.preventDefault();       
    $.ajax({    
    type: 'POST',  
    url: 'estudiante/new_estudiante',  
    data: $('#formNewEstudiante').serialize(), 
    dataType:'json',
     beforeSend:function(xhr, settings){     
     },
    success:function(data){
            //  json response  
            console.log(data);
    },
    error: function(data) { 
        // if error occured
        console.log(data);
    }
    });      
  });
 });
