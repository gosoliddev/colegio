 //validation
 (function ($){
    $("#kardexdetalle").validate({
        // Specify the validation rules
        rules: {
            "kardexdetalle[id_kardex]": "required",
            "kardexdetalle[id_gestion]": "required",
            "kardexdetalle[id_curso_subarea]": "required",
            "kardexdetalle[nota_anual]": "required",   
            "kardexdetalle[activo]": "required",   	
        },       

    });

})(jQuery);
//list
$(function() {

    $("#tableKardexdetalle").bootgrid({
        ajax: true,   
        url: "kardexdetalle/load",
        selection: true,
        multiSelect: true,      
        rowSelect: true,
        keepSelection: true,
        sorting:true,
        searchSettings: {
            delay: 100,
            characters: 3
        },
        formatters: {
            "codigo_sie": function(column, row)
            {
                return "<a href=\"#\">" + column.codigo_sie + ": " + row.codigo_sie + "</a>";
            }
        }
    }).on("selected.rs.jquery.bootgrid", function(e, rows)
    {
        console.log($(e.target).bootgrid("getSelectedRows"));
        var rowIds = [];
        for (var i = 0; i < rows.length; i++)
        {
            rowIds.push(rows[i].codigo_sie);
        }
        e.addClass('has-error');
        alert("Select: " + rowIds.join(","));
    }).on("deselected.rs.jquery.bootgrid", function(e, rows)
    {
        var rowIds = [];
        for (var i = 0; i < rows.length; i++)
        {
            rowIds.push(rows[i].codigo_sie);
        }
        alert("Deselect: " + rowIds.join(","));
    });
});